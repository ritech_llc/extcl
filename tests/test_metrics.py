#!/usr/bin/env python3
# coding: utf-8


from extcllib.metrics_and_scores import calc_confusion_matrix, merge_metrics


def test_calc_confusion_matrix():
    gold_labels_res = [
        ('1', ('A',)),
        ('2', ('B',)),
        (
            '3',
            (
                'A',
                'B',
            ),
        ),
    ]
    predicted_labels_res = [
        ('1', ('A',)),
        ('2', ('B',)),
        (
            '3',
            (
                'A',
                'B',
            ),
        ),
    ]

    classes, confusion_matrix = calc_confusion_matrix(gold_labels_res, predicted_labels_res)

    metrics = merge_metrics(classes, confusion_matrix)

    assert metrics['micro_accuracy'] == 1.0

    assert metrics['micro_precision'] == 1.0
    assert metrics['micro_recall'] == 1.0
    assert metrics['micro_f1'] == 1.0

    assert metrics['macro_precision'] == 1.0
    assert metrics['macro_recall'] == 1.0
    assert metrics['macro_f1'] == 1.0

    for l in ['A', 'B']:
        assert metrics['labels'][l]['precision'] == 1.0
        assert metrics['labels'][l]['recall'] == 1.0
        assert metrics['labels'][l]['f1'] == 1.0
