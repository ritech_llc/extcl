#!/usr/bin/env python3
# coding: utf-8

import json
from pyfakefs.fake_filesystem_unittest import TestCase, TestCaseMixin

from extcllib.dataset_handling import DatasetOpts
from extcllib.dataset_handling import TrainDevTestSplitStrategyOpts

from extcllib.dataset_readers.textapp_dump_dataset_reader import FromCsvDataSetReader


class CsvDatasetMixin(TestCaseMixin):
    def setUp(self):
        self.setUpPyfakefs()
        self.fake_csv_filename = 'dataset_filename.csv'
        self.fs.create_file(self.fake_csv_filename)
        self.sample_size = 20
        self.classes_cnt = 3
        with open(self.fake_csv_filename, 'w') as csv_f:
            for i in range(self.sample_size):
                csv_f.write(f"{i};{i}.txt;{i}.json;{i%self.classes_cnt+1}class\n")

                with open(f"{i}.txt", 'w') as text_f:
                    text_f.write(f"Text content of file #{i}.")

                with open(f"{i}.json", 'w') as json_f:
                    m = {
                        "meta": {"key": f"value {i}"},
                        "labels": {"class": f"{i%self.classes_cnt+1} clsss"},
                    }
                    json_f.write(json.dumps(m))


class CsvDatasetWithSplittersTestCase(CsvDatasetMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.dataset1 = FromCsvDataSetReader(
            opts=DatasetOpts(input=self.fake_csv_filename),
            split_strategy_opts=TrainDevTestSplitStrategyOpts(
                split=(0.8, 0.1, 0.1),
            ),
        )
        self.dataset2 = FromCsvDataSetReader(
            opts=DatasetOpts(input=self.fake_csv_filename),
            split_strategy_opts=TrainDevTestSplitStrategyOpts((0.9, 0.05, 0.05)),
        )

    def test_from_csv_dataset(self):
        assert self.dataset1.x[1] == ('1', '1.txt', '1.json')
        assert self.dataset1.y[1] == ('2',)
        assert self.dataset1.x[3] == ('3', '3.txt', '3.json')
        assert self.dataset1.y[3] == ('1',)
        assert self.dataset1.x[11] == ('11', '11.txt', '11.json')
        assert self.dataset1.y[11] == ('3',)

        assert len(self.dataset1.x) == self.sample_size
        assert len(self.dataset1.y) == self.sample_size
        assert len(self.dataset2.x) == self.sample_size
        assert len(self.dataset2.y) == self.sample_size

    def test_splitter_train_dev_test(self):
        train_items = [(x, y) for x, y in self.dataset1.train_items()]
        dev_items = [(x, y) for x, y in self.dataset1.dev_items()]
        test_items = [(x, y) for x, y in self.dataset1.test_items()]

        assert len(train_items) == self.sample_size * 0.8
        assert len(dev_items) == self.sample_size * 0.1
        assert len(test_items) == self.sample_size * 0.1

        assert sorted(train_items + dev_items + test_items) == sorted(
            [(x, y) for x, y in zip(*self.dataset1.reader.get_items_list())]
        )

        train_items = [(x, y) for x, y in self.dataset2.train_items()]
        dev_items = [(x, y) for x, y in self.dataset2.dev_items()]
        test_items = [(x, y) for x, y in self.dataset2.test_items()]

        assert len(train_items) == self.sample_size * 0.9
        assert len(dev_items) == self.sample_size * 0.05
        assert len(test_items) == self.sample_size * 0.05

        assert sorted(train_items + dev_items + test_items) == sorted(
            [(x, y) for x, y in zip(*self.dataset2.reader.get_items_list())]
        )
