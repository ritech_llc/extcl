#!/usr/bin/env python3
# coding: utf-8

import re
import os.path as fs
from pyfakefs.fake_filesystem_unittest import TestCase

from extcllib.dataset_handling import (
    DatasetOpts,
    ExTclStep,
    get_dataset_handler_instance,
)

from .test_common_utils import CsvDatasetMixin


class DatasetHandlerTestCase(CsvDatasetMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.d_h = get_dataset_handler_instance(
            DatasetOpts(
                dataset_reader='TextAppDumpFromCsvDataSetReader',
                input=self.fake_csv_filename,
                work_dir='test_workdir',
            )
        )

    def test_dataset_handler(self):
        assert self.d_h.step_workdir(ExTclStep.RAW_DOCS) == fs.join('test_workdir', 'raw')
        assert self.d_h.step_workdir(ExTclStep.TEXT_EXTRACT) == fs.join('test_workdir', 'txt')
        assert self.d_h.step_workdir(ExTclStep.LING_ANALYSIS) == fs.join('test_workdir', 'ling')
        assert self.d_h.step_workdir(ExTclStep.VECTORIZE) == fs.join('test_workdir', 'vector')
        assert self.d_h.step_workdir(ExTclStep.CALC_STAT) == fs.join('test_workdir', 'stat')
        assert self.d_h.step_workdir(ExTclStep.CLASSIFY) == fs.join('test_workdir', 'classify')

        raw_items = [i for i in self.d_h.items()]

        for raw_path, y in self.d_h.path_items(step=ExTclStep.RAW_DOCS):
            assert fs.isfile(raw_path)

        for step, subdir in [
            (ExTclStep.TEXT_EXTRACT, 'txt'),
            (ExTclStep.LING_ANALYSIS, 'ling'),
            (ExTclStep.VECTORIZE, 'vector'),
            (ExTclStep.CALC_STAT, 'stat'),
            (ExTclStep.CLASSIFY, 'classify'),
        ]:
            path_items = [i for i in self.d_h.path_items(step=step)]
            assert len(raw_items) == len(path_items)

            for path, label in path_items:
                assert fs.dirname(path) == 'test_workdir/' + subdir

        raw_contents = [i for i in self.d_h.read_items(step=ExTclStep.RAW_DOCS)]
        ids_labels = list()
        for i, (raw_content, label) in enumerate(raw_contents):
            assert isinstance(raw_content, (str,))
            m = re.match(r"Text content of file \#(\d+).", raw_content)
            assert m is not None
            assert isinstance(label, (tuple,))
            ids_labels.append((int(m.group(1)), label))

        for i, l in ids_labels:
            assert i % self.classes_cnt + 1 == int(l[0])

        assert sorted(ids_labels) == [
            (i, (str(i % self.classes_cnt + 1),)) for i in range(self.sample_size)
        ]
