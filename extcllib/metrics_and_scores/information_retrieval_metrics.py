#!/usr/bin/env python3
# coding: utf-8

import math


def tf_idf_bm25(
    n: int, nd: int, N: int, nq: int, k1: float = 2.0, b: float = 0.75, avgdl: int = 3000
) -> float:
    """
    Calculates BM25 based word(entity) score.
    https://ru.wikipedia.org/wiki/Okapi_BM25

    :param n: число вхождений
    :param nd: общее число слов в документе
    :param N: число  документов в коллекции
    :param nq: число документов в коллекции, в которых встречается данное слово
    :param k1: своб. коэфф. (обычно 2.0)
    :param b: своб. коэфф. (обычно 0.75)
    :param avgdl: средняя длина документа в коллекции
    :return: мера tf-idf(bm25)
    """
    tf = n / nd
    idf = math.log((N - nq + 0.5) / (nq + 0.5))
    tf_idf_bm25_ = idf * tf * (k1 + 1) / (tf + k1 * (1 - b + b * nd / avgdl))
    return tf_idf_bm25_


def tf_idf(n: int, nd: int, N: int, nq: int) -> float:
    """
    Calculates TF-IDF score.
    https://ru.wikipedia.org/wiki/TF-IDF

    :param n: число вхождений
    :param nd: общее число слов в документе
    :param N: число  документов в коллекции
    :param nq: число документов в коллекции, в которых встречается данное слово
    :return: мера tf-idf
    """
    tf = n / nd
    idf = math.log(N / nq)
    return tf * idf
