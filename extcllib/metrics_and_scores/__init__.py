#!/usr/bin/env python3
# coding: utf-8

from .extcl_metrics import tf_idf_x
from .information_retrieval_metrics import tf_idf_bm25, tf_idf
from .quality_metrics import (
    round_item,
    ConfusionMatrix,
    MetricsCalculator,
    calc_confusion_matrix,
    merge_metrics,
)

import numpy as np

from typing import List

__all__ = [
    'tf_idf_bm25',
    'tf_idf',
    'tf_idf_x',
    'aggregate_scores',
    'round_item',
    'ConfusionMatrix',
    'MetricsCalculator',
    'calc_confusion_matrix',
    'merge_metrics',
]


def aggregate_scores(scores: List[float | int], predict_score_aggregator: str = 'sum') -> float:
    match predict_score_aggregator:
        case "sum":
            return float(np.sum(scores))
        case "mult":
            return float(np.prod(scores))
        case "avg":
            return float(np.average(scores))
        case _:
            raise RuntimeError(f"Unsupported predict_score_aggregator: {predict_score_aggregator}")
