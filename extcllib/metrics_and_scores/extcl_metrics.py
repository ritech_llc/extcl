#!/usr/bin/env python3
# coding: utf-8

import math


def tf_idf_x(n: int, nd: int, N: int, nq: int, elq: int) -> float:
    """
    https://ru.wikipedia.org/wiki/TF-IDF,
    https://www.mathnet.ru/php/archive.phtml?wshow=paper&jrnid=itvs&paperid=92&option_lang=rus

    Метод автоматической классификации коротких текстовых сообщений
    Мбайкоджи, Драль, Соченков

    ХТЗ -- характеристика тематической значимости

    :param n: число вхождений (сколько раз встречается сущность в этом документе)
    :param nd: общее число слов в документе
    :param N: число  документов в коллекции
    :param nq: число документов в коллекции, в которых встречается данное слово
    :param elq: число документов в коллекции с этим классом, в которых встречается данное слово
    :return: мера tf-idf для класса (ХТЗ)
    """
    tf = n / nd
    idf = math.log(N / nq)
    idf_l = math.log(N / elq)
    delta_I = idf - idf_l
    delta_I = 0.0 if delta_I < 0.0 else delta_I
    return tf * delta_I
