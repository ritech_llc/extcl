#!/usr/bin/env python3
# coding: utf-8

import logging

from typing import Dict, Union
from pydantic.types import PositiveInt

from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.metrics import multilabel_confusion_matrix

from extcllib.dataset_handling import DatasetOneLabelType


def round_item(
    num: Union[int, float], den: Union[int, float], round_precision: PositiveInt = 4
) -> float:
    if num == 0.0:
        return 0.0
    elif den == 0.0:
        logging.error("Denominator is null!")
        return 0.0
    else:
        return round(num / den, round_precision)


class ConfusionMatrix(object):
    """
    https://en.wikipedia.org/wiki/Confusion_matrix
    """

    round_precision: PositiveInt

    def __init__(self, item, round_precision: PositiveInt = 3):
        self.round_precision = round_precision

        tn, fp, fn, tp = item
        self.tn = tn
        self.fn = fn
        self.tp = tp
        self.fp = fp

    def update(self, cfm):
        self.tn += cfm.tn
        self.fn += cfm.fn
        self.tp += cfm.tp
        self.fp += cfm.fp

    def f1(self):
        return round_item(2 * self.tp, 2 * self.tp + self.fp + self.fn, self.round_precision)

    def accuracy(self):
        return round_item(
            self.tp + self.tn, self.tn + self.fn + self.tp + self.fp, self.round_precision
        )

    def precision(self):
        return round_item(self.tp, self.tp + self.fp, self.round_precision)

    def recall(self):
        return round_item(self.tp, self.tp + self.fn, self.round_precision)


class MetricsCalculator(object):
    """
    Metrics Calculator
    """

    def __init__(self, labels, confusion_matrix):
        self.labels = labels
        self.confusion_matrix = [ConfusionMatrix(item) for item in confusion_matrix]
        self.common_confusion_matrix = self.confusion_matrix[0]
        for l_c_m in self.confusion_matrix[1:]:
            self.common_confusion_matrix.update(l_c_m)

    def calc(self):
        labels_f1 = {label: l_c_m.f1() for label, l_c_m in zip(self.labels, self.confusion_matrix)}
        common_micro_f1 = self.common_confusion_matrix.f1()

        return labels_f1, common_micro_f1


def calc_confusion_matrix(gold_labels_res, predicted_labels_res):
    if len(gold_labels_res) != len(predicted_labels_res):
        logging.error("Gold items cnt != predicted items cnt. Skip calc metrics step!")
        return None, None

    if len(gold_labels_res) == 0:
        logging.error("Unable to calculate confusion matrix for empty data!")
        return None, None

    for (gold_doc_id, gold_label), (predicted_doc_id, _) in zip(
        gold_labels_res, predicted_labels_res
    ):
        assert gold_doc_id == predicted_doc_id
        if not gold_label:
            logging.info("Gold label is empty! Skip calc metrics step!")
            return None, None

    mlb = MultiLabelBinarizer()

    y_true = mlb.fit_transform([l for i, l in gold_labels_res])
    y_pred = mlb.transform([l for i, l in predicted_labels_res])

    classes = list(mlb.classes_)
    logging.debug(f"mlb classes: {classes}")
    sk_confusion_matrix = multilabel_confusion_matrix(y_true, y_pred)
    logging.debug(f"sk cfm shape: {sk_confusion_matrix.shape}")

    return classes, [c.ravel().tolist() for c in sk_confusion_matrix]


def merge_metrics(labels, cnf_mtrx, round_precision=3) -> Dict:
    if labels is None or cnf_mtrx is None or len(labels) == 0 or len(cnf_mtrx) == 0:
        logging.error("Unable to merge empty metrics!")
        return dict()

    labels_confusion_matrices: Dict[DatasetOneLabelType, ConfusionMatrix]
    labels_confusion_matrices = dict()
    common_confusion_matrix = ConfusionMatrix([0, 0, 0, 0])
    for label, l_c_m in zip(labels, cnf_mtrx):
        l_c_m = ConfusionMatrix(l_c_m)
        common_confusion_matrix.update(l_c_m)

        if label in labels_confusion_matrices:
            labels_confusion_matrices[label].update(l_c_m)
        else:
            labels_confusion_matrices[label] = l_c_m

    metrics = {
        'micro_accuracy': common_confusion_matrix.accuracy(),
        'micro_precision': common_confusion_matrix.precision(),
        'micro_recall': common_confusion_matrix.recall(),
        'micro_f1': common_confusion_matrix.f1(),
        'labels': {
            label: {
                'accuracy': labels_confusion_matrices[label].accuracy(),
                'precision': labels_confusion_matrices[label].precision(),
                'recall': labels_confusion_matrices[label].recall(),
                'f1': labels_confusion_matrices[label].f1(),
            }
            for label in labels_confusion_matrices.keys()
        },
    }

    for m in ['precision', 'recall', 'f1']:
        metrics['macro_' + m] = round(
            sum([v[m] for l, v in metrics['labels'].items()]) / len(metrics['labels']),
            round_precision,
        )

    return metrics
