#!/usr/bin/env python3
# coding: utf-8

from .multiprocessor import MultiProcessor

__all__ = [
    'MultiProcessor',
]
