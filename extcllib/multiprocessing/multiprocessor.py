#!/usr/bin/env python3
# coding: utf-8

import queue
import logging
import multiprocessing as mp
from typing import Any, Dict, Tuple, Generator, List

from extcllib.dataset_handling import DatasetLabelType


class MultiProcessor(object):
    @staticmethod
    def process_step(opts, func, input_queue_items_generator, step_name) -> Dict:
        input_queue: mp.Queue
        output_queue: mp.Queue
        input_queue = mp.Queue()
        output_queue = mp.Queue()

        input_elements_count = 0
        for item in input_queue_items_generator:
            input_elements_count += 1
            input_queue.put(item)

        procs_cnt = min(input_elements_count, opts.proc_cnt)

        for _ in range(procs_cnt):
            input_queue.put(None)

        processes = [
            mp.Process(target=func, args=(opts, i, input_queue, output_queue))
            for i in range(procs_cnt)
        ]

        for p in processes:
            p.start()

        logging.debug(f"Starting worker-processes")
        for p in processes:
            p.join()

        logging.debug(f"Worker-processes joined")

        res: Dict
        res = dict()
        for _ in range(len(processes)):
            try:
                out_item = output_queue.get()
            except queue.Empty as oq_empty:
                logging.error(f"Failed to get out res: {oq_empty}")
                continue

            for k, v in out_item.items():
                if isinstance(v, (int,)):
                    if k in res:
                        res[k] += v
                    else:
                        res[k] = v
                elif isinstance(v, (list,)):
                    if k in res:
                        res[k].extend(v)
                    else:
                        res[k] = v

        log_res = {k: v for k, v in res.items() if isinstance(v, (int,))}
        if not (hasattr(opts, 'document_path') and opts.document_path):
            logging.info(f"{step_name} whole result(ints): {log_res}")

        return res

    @staticmethod
    def iter_queue(
        input_queue: mp.Queue,
    ) -> Generator[Tuple[Tuple[Any, ...], DatasetLabelType], None, None]:
        item: Tuple[Tuple[Any, ...], DatasetLabelType]
        for item in iter(input_queue.get, None):
            yield item

    @staticmethod
    def iter_hyperparams(
        input_queue: mp.Queue,
    ) -> Generator[Dict, None, None]:
        item: Dict
        for item in iter(input_queue.get, None):
            yield item

    @staticmethod
    def put_to_output_queue(output_queue: mp.Queue, item):
        try:
            if output_queue.full():
                logging.error(f"Failed to put item (queue Full): {item}")
                return

            output_queue.put(item)
        except queue.Full as e:
            logging.exception(f"Failed to put item (queue Full): {item}")
        except Exception as e:
            logging.exception(f"Failed to put item: {e}")
