#!/usr/bin/env python3
# coding: utf-8

from .stat import (
    calc_stat_func,
    upload_stat_func,
    get_stat_cli,
    StatBaseOpts,
    GetStatOpts,
    StatGetter,
    StatBuilder,
    StatBuilderOpts,
    get_stat_builder_instance,
)
from .wordsb_client import (
    StatBaseOpts,
    GetStatOpts,
    StatUploader,
    WdbEntityLabelStat,
    LabelStat,
    StatGetter,
    WdbEntity,
    WordsDbCommonStat,
)

__all__ = [
    'calc_stat_func',
    'add_stat_uploader_opts',
    'upload_stat_func',
    'add_get_stat_opts',
    'get_stat_cli',
    'StatBaseOpts',
    'GetStatOpts',
    'StatGetter',
    'WdbEntity',
    'StatBuilder',
    'StatBuilderOpts',
    'get_stat_builder_instance',
    'WordsDbCommonStat',
    'WdbEntityLabelStat',
    'LabelStat',
]


def add_stat_builder_opts(parser):
    o = StatBuilderOpts()
    parser_group = parser.add_argument_group('stat_builder')
    parser_group.add_argument(
        "--allowed_word_pos_tags",
        "-ap",
        type=str,
        nargs="+",
        default=o.allowed_word_pos_tags,
        help="allowed pos tags",
    )
    parser_group.add_argument(
        "--use_pylp_lemmatization",
        type=bool,
        default=o.use_pylp_lemmatization,
        help="",
    )
    parser_group.add_argument(
        "--phrases_max_len",
        type=int,
        default=o.phrases_max_len,
        help="",
    )
    parser_group.add_argument(
        "--phrases_with_mwe",
        type=bool,
        default=o.phrases_with_mwe,
        help="",
    )
    parser_group.add_argument(
        "--synonym_model",
        type=str,
        default=o.synonym_model,
        help="",
    )
    parser_group.add_argument(
        "--min_sim_threshold",
        type=float,
        default=o.min_sim_threshold,
        help="",
    )


def add_stat_uploader_opts(parser):
    o = StatBaseOpts()
    parser_group = parser.add_argument_group('stat_uploader')
    parser_group.add_argument(
        "--endpoint", "-e", type=str, default=o.endpoint, help="words database endpoint"
    )


def add_get_stat_opts(parser):
    o = GetStatOpts()
    parser_group = parser.add_argument_group('get_stat')
    parser_group.add_argument(
        "--endpoint", "-e", type=str, default=o.endpoint, help="words database endpoint"
    )
    parser_group.add_argument(
        "--get_documents_stat",
        action="store_true",
        default=o.get_documents_stat,
        help="get documents stat",
    )
