#!/usr/bin/env python3
# coding: utf-8

import argparse
import collections
import csv
from dataclasses import dataclass
import gensim
import gensim.downloader
import logging
import multiprocessing as mp
import os
import os.path as fs
from typing import Counter, Dict, List, Tuple, Set

import libpyexbase

from extcllib.dataset_handling import (
    DatasetLabelType,
    UndefinedLabelName,
    ExTclStep,
    get_dataset_handler_instance,
)
from extcllib.linguistics import (
    init_doc,
    calculate_stat_from_doc,
    EntityStat,
    EntityStatChunks,
)
from extcllib.multiprocessing import MultiProcessor

from .wordsb_client import (
    StatBaseOpts,
    GetStatOpts,
    StatUploader,
    LabelStat,
    StatGetter,
)


@dataclass
class StatBuilderOpts:
    allowed_word_pos_tags: List[str] | None = None
    use_pylp_lemmatization: bool | None = None
    phrases_max_len: int = 2
    phrases_with_mwe: bool = True
    synonym_model: str | None = None
    min_sim_threshold: float = 0.75


def calculate_stat(
    doc_id: str | int,
    text: str,
    conll_raw_text: str,
    label: DatasetLabelType,
    opts: StatBuilderOpts = StatBuilderOpts(),
) -> Tuple[List[EntityStatChunks], Dict[int, Set[str]]]:
    doc = init_doc(doc_id, text, conll_raw_text)
    return calculate_stat_from_doc(opts, doc, label)


class NoSynonymModelException(Exception):
    pass


class StatBuilder(object):
    def __init__(self, opts: StatBuilderOpts = StatBuilderOpts()):
        self.opts = opts
        self._load_synonym_model(opts)

    def _load_synonym_model(self, opts):
        if hasattr(opts, 'synonym_model') and opts.synonym_model:
            try:
                synonym_model = gensim.downloader.load(opts.synonym_model)
            except ValueError:
                if fs.exists(opts.synonym_model):
                    try:
                        os.environ['GENSIM_DATA_DIR'] = fs.dirname(opts.synonym_model)
                        synonym_model = gensim.downloader.load(opts.synonym_model)
                    except Exception as e:
                        logging.exception(e)
                        raise NoSynonymModelException(f"There is no {opts.synonym_model}")
                else:
                    raise NoSynonymModelException(f"There is no {opts.synonym_model}")
            logging.info(f"Loaded {opts.synonym_model}")
        else:
            synonym_model = None
        self.synonym_model = synonym_model

    def calculate_stat(
        self, doc_id: str | int, text: str, conll_raw_text: str, label: DatasetLabelType
    ) -> Tuple[List[EntityStatChunks], Dict[int, Set[str]]]:
        entities: List[EntityStatChunks]
        sent_chunks: Dict[int, Set[str]]
        entities, sent_chunks = calculate_stat(doc_id, text, conll_raw_text, label, self.opts)

        if self.synonym_model:
            synonyms: Dict[str, Set[EntityStatChunks]]
            synonyms = collections.defaultdict(set)
            doc_lemmas = set()

            for entity in entities:
                doc_lemmas.add(entity.lemma)
                lemma_variants = []
                if entity.pos:
                    lemma_variants.append((entity.lemma, None))
                    lemma_variants.append((str(entity.lemma + '_' + entity.pos), '_'))
                else:
                    # if not pos, then consider as phrase
                    lemma = "::".join(entity.lemma.split())
                    lemma_variants.append((lemma, None))
                for l in lemma_variants:
                    lemma, delim = l
                    try:
                        for ss in self.synonym_model.most_similar(lemma):
                            synonym, similarity = ss
                            if similarity < self.opts.min_sim_threshold:
                                continue
                            if delim:
                                synonym = synonym.split(delim)[0]

                            synonyms[synonym].add(entity)
                        break
                    except KeyError:
                        continue

            extra_synonyms = set([str(lemma) for lemma in synonyms.keys()])  # - doc_lemmas
            extra_entities = list()
            orig_entity_id_to_extra_entity_id = collections.defaultdict(set)
            for lemma in extra_synonyms:
                word_id = libpyexbase.detect_lang_calc_word_id(lemma, True)
                for entity in synonyms[lemma]:
                    extra_entities.append(
                        EntityStatChunks(
                            word_id, lemma, entity.label, entity.count, '', entity.chunks
                        )
                    )
                    orig_entity_id_to_extra_entity_id[entity.get_id()].add(word_id)

            entities.extend(extra_entities)

            extra_sent_chunks = collections.defaultdict(set)
            for s_id in sent_chunks:
                for entity_id in sent_chunks[s_id]:
                    extra_entity_ids = orig_entity_id_to_extra_entity_id.get(entity_id, None)
                    if extra_entity_ids:
                        for extra_entity_id in extra_entity_ids:
                            extra_sent_chunks[s_id].add(extra_entity_id)

            for s_id, extra_entities_ in extra_sent_chunks.items():
                sent_chunks[s_id].update(extra_entities_)

        return entities, sent_chunks


def calc_stat(
    stat_builder: StatBuilder,
    txt_path: str,
    ling_path: str,
    stat_path: str,
    label: DatasetLabelType,
    counters=None,
):
    if counters is None:
        counters = collections.Counter()

    if fs.exists(stat_path):
        logging.info(f"Skip. {stat_path} already exists.")
        counters["skip"] += 1
        return

    assert fs.exists(txt_path)
    assert fs.exists(ling_path)

    with open(txt_path, 'r') as f:
        text = f.read()

    with open(ling_path, 'r') as f:
        conll_raw_text = f.read()

    try:
        stat_result, _ = stat_builder.calculate_stat(
            doc_id=stat_path, text=text, conll_raw_text=conll_raw_text, label=label
        )
        counters["built_stat"] += 1
    except RuntimeError as r_err:
        logging.error(f"Failed to build stat: {ling_path}: {str(r_err)}")
        counters["failed_to_build_stat"] += 1
        return

    os.makedirs(fs.dirname(stat_path), exist_ok=True)
    with open(stat_path, 'w') as f:
        csv_writer = csv.writer(f, delimiter=',')
        for entity_stat in stat_result:
            csv_writer.writerow(entity_stat.to_row())

    counters["stat_success"] += 1
    logging.info(f"Stat built {ling_path} -> {stat_path}")
    return stat_result


def get_stat_builder_instance(opts) -> StatBuilder:
    stat_builder = StatBuilder(
        StatBuilderOpts(
            allowed_word_pos_tags=opts.allowed_word_pos_tags,
            use_pylp_lemmatization=opts.use_pylp_lemmatization,
            phrases_max_len=opts.phrases_max_len,
            phrases_with_mwe=opts.phrases_with_mwe,
            synonym_model=opts.synonym_model,
            min_sim_threshold=opts.min_sim_threshold,
        )
    )
    return stat_builder


def calc_stat_func(
    opts: argparse.Namespace, proc_name: str, input_queue: mp.Queue, output_queue: mp.Queue
):
    stat_builder = get_stat_builder_instance(opts)

    counters: Counter
    counters = collections.Counter()

    for item in MultiProcessor.iter_queue(input_queue):
        try:
            label: DatasetLabelType
            (txt_path, ling_path, stat_path), label = item
            calc_stat(stat_builder, txt_path, ling_path, stat_path, label, counters)
        except Exception as e:
            counters["fail_to_build_stat_for_item"] += 1
            logging.exception(f"Failed to build stat for item {item}: {e}")

    res = {
        'proc_name': str(proc_name),
    }
    res.update(counters)

    MultiProcessor.put_to_output_queue(output_queue, res)


def read_stat(stat_path: str) -> List[EntityStat]:
    if not fs.exists(stat_path):
        raise RuntimeError(f"Stat path does not exists: {stat_path}")

    entities: List[EntityStat] = list()
    with open(stat_path, 'r', newline='\n') as f:
        csv_reader = csv.reader(f, delimiter=',')

        try:
            for row in csv_reader:
                try:
                    entity = EntityStat.from_row(row)
                except ValueError:
                    logging.error(f"Failed to split row: {row}")
                    continue

                entities.append(entity)

        except Exception as r_err:
            logging.error(f"Failed to read stat: {stat_path}: {str(r_err)}")

    return entities


class BadUploadEntities(Exception):
    pass


def upload_stat(stat_uploader: StatUploader, stat_path: str, counters=None):
    if counters is None:
        counters = collections.Counter()

    entities = read_stat(stat_path)

    if entities:
        upload_entities = []
        for entity in entities:
            if (
                entity.label is not None
                and len(entity.label) > 0
                and set(entity.label) != {UndefinedLabelName}
            ):
                upload_entities.append(entity.to_dict())
            else:
                logging.debug(f"Skip entity {entity} with bad label: {entity.label}")

        if len(upload_entities) == 0:
            raise BadUploadEntities("")

        upload_result = stat_uploader.add_document_entities(upload_entities)
        if upload_result is True:
            logging.info(f"Stat uploaded {stat_path} -> {stat_uploader.opts.endpoint}")
            counters["uploaded"] += 1
        else:
            counters["upload_request_false"] += 1
            logging.error(
                f"Stat upload request returned false: {stat_path}. Endpoint: {stat_uploader.opts.endpoint}"
            )
    else:
        logging.error(f"Failed to upload stat: {stat_path}: got empty list of entities")
        counters["failed_to_upload_stat"] += 1


def upload_stat_func(
    opts: argparse.Namespace, proc_name: str, input_queue: mp.Queue, output_queue: mp.Queue
):
    stat_uploader = StatUploader(StatBaseOpts(endpoint=opts.endpoint))

    counters: Counter
    counters = collections.Counter()

    for item in MultiProcessor.iter_queue(input_queue):
        try:
            (stat_path,), _ = item
            upload_stat(stat_uploader, stat_path, counters)
        except Exception as e:
            counters["failed_to_upload_stat_for_item"] += 1
            logging.exception(f"Failed to upload stat for item {item}: {e}")

    res = {
        'proc_name': str(proc_name),
    }
    res.update(counters)

    MultiProcessor.put_to_output_queue(output_queue, res)


def get_stat_cli(opts):
    d_h = get_dataset_handler_instance(opts)
    stat_getter = StatGetter(GetStatOpts(endpoint=opts.endpoint))

    got_stat_cnt = 0
    docs_cnt = 0

    common_stat = StatGetter.get_common_stat(stat_getter.opts.endpoint)
    logging.info(common_stat)

    if common_stat is None:
        logging.error("Failed to get common_stat!")
        return

    if not opts.get_documents_stat:
        logging.info("Parameter 'get_documents_stat' is not set.")
        return

    for (stat_path,), _ in d_h.steps_paths(
        step=ExTclStep.UPLOAD_STAT,
        part='train',
    ):
        try:
            docs_cnt += 1

            entities = read_stat(stat_path)

            flag = False
            for entity in entities:
                try:
                    wdb_entities = StatGetter.get_entity(stat_getter.opts.endpoint, entity.word_id)
                    if wdb_entities is None:
                        logging.error(f"Failed to get entity from wdb: {entity.word_id}.")
                        continue
                    elif len(wdb_entities) == 0:
                        logging.warning(
                            f"Entity {entity.word_id} '{entity.lemma}' was not found in wdb."
                        )
                        continue

                    assert len(wdb_entities) == 1
                    wdb_entity = wdb_entities[0]

                    candidate_to_reduce = False
                    flag = True

                    wdb_entity_docs_cnt = 0
                    wdb_entity_occurrences_cnt = 0
                    for wdb_entity_stat in wdb_entity.stat:
                        if wdb_entity_stat.label == "#TOTAL#":
                            wdb_entity_docs_cnt = wdb_entity_stat.total_docs_count
                            wdb_entity_occurrences_cnt = wdb_entity_stat.total_count
                            break
                        else:
                            wdb_entity_docs_cnt += wdb_entity_stat.total_docs_count
                            wdb_entity_occurrences_cnt += wdb_entity_stat.total_count

                    reduce_labels_cnt = 0
                    entity_labels_cnt = len(wdb_entity.stat)
                    for wdb_entity_stat in wdb_entity.stat:
                        if wdb_entity_stat.label == "#TOTAL#":
                            continue

                        label = wdb_entity_stat.label
                        N = common_stat.total_docs_count
                        nq = wdb_entity_docs_cnt
                        try:
                            wdb_label_stat = common_stat.labels_stat[label]
                        except KeyError as kerr:
                            wdb_label_stat = LabelStat(1, 1)

                        elq = wdb_label_stat.total_docs_count

                        import math

                        idf = math.log(N / nq, N)
                        idf_l = math.log(N / elq, N)
                        delta_I = idf - idf_l

                        if delta_I <= 0.0:
                            reduce_labels_cnt += 1
                            candidate_to_reduce = True
                            logging.debug(
                                f"entity.lemma {entity.lemma} label {label}, delta_I: {delta_I}, N: {N}, nq: {nq}, elq: {elq}"
                            )

                    logging.debug(
                        f"{entity} , {wdb_entity}, candidate_to_reduce: {candidate_to_reduce}, {reduce_labels_cnt}/{entity_labels_cnt}"
                    )

                except Exception as e:
                    logging.exception(f"Failed to get stat for entity: {entity}. Err: {str(e)}")

            if flag:
                got_stat_cnt += 1

        except Exception as e:
            logging.exception(f"Failed to get stat: {e}")

    logging.info(f"Got stat {got_stat_cnt}/{docs_cnt}")
