#!/usr/bin/env python3
# coding: utf-8

import collections
from dataclasses import dataclass
from functools import lru_cache
import jsonrpcclient
import logging
import requests
from typing import Dict, List, OrderedDict, Tuple

from extcllib.dataset_handling import DatasetOneLabelType


@dataclass
class StatBaseOpts:
    endpoint: str = 'http://localhost:8550/jsonrpc'


@dataclass
class GetStatOpts(StatBaseOpts):
    get_documents_stat: bool = False


class StatUploader(object):
    def __init__(self, opts: StatBaseOpts):
        self.opts = opts

    def add_document_entities(self, entities: List[Dict]) -> bool:
        req = jsonrpcclient.request(
            'AddDocumentEntities',
            params={
                "entities": entities,
            },
        )
        res = requests.post(self.opts.endpoint, json=req)
        try:
            res_json = res.json()
            if 'result' in res_json and res_json['result'] is True:
                return True
            else:
                logging.error(f"Failed to upload. Req:  {req}")
                logging.error(f"Failed to upload. Res:  {res_json}")
                return False
        except Exception as e:
            raise RuntimeError(e)


@dataclass
class WdbEntityLabelStat:
    label: DatasetOneLabelType
    total_count: int
    total_docs_count: int


@dataclass
class WdbEntity:
    word_id: str
    lemma: str
    docs_cnt: int
    occurrences_cnt: int
    stat: List[WdbEntityLabelStat]

    def __init__(
        self, word_id: str, lemma: str, docs_cnt: int, occurrences_cnt: int, raw_stat: List
    ):
        self.word_id = word_id
        self.lemma = lemma
        self.docs_cnt = docs_cnt
        self.occurrences_cnt = occurrences_cnt
        self.stat = WdbEntity.get_stat(raw_stat)
        self.labels = set([entity_stat.label for entity_stat in self.stat])

    @staticmethod
    def get_stat(
        raw_stat: List[Tuple[DatasetOneLabelType, Tuple[int, int]]]
    ) -> List[WdbEntityLabelStat]:
        return [
            WdbEntityLabelStat(label, total_count, total_docs_count)
            for label, (total_count, total_docs_count) in raw_stat
        ]

    @property
    def label_count(self) -> int:
        return len(self.stat)

    def has_label(self, label) -> bool:
        return label in self.labels

    def get_id(self) -> str:
        return self.word_id


@dataclass
class LabelStat:
    total_entities_count: int
    total_docs_count: int


@dataclass
class WordsDbCommonStat:
    last_added_time: int
    total_entities_count: int
    total_docs_count: int
    vocabulary_size: int
    labels_size: int
    labels_stat: OrderedDict[DatasetOneLabelType, LabelStat]


class StatGetter(object):
    def __init__(self, opts: GetStatOpts):
        self.opts = opts

    @staticmethod
    def check_wdb_entity(word_id, lemma, stat) -> bool:
        return word_id != ''

    @staticmethod
    def process_entities(endpoint, req) -> List[WdbEntity] | None:
        entity_request_ids = (
            req['params']['ids'] if 'ids' in req['params'] else [req['params']['id']]
        )
        res = requests.post(endpoint, json=req)
        try:
            res_json = res.json()
            if (
                'result' in res_json
                and res_json['result']
                and isinstance(res_json['result'], (list,))
            ):
                found = 0
                not_found = 0
                wdb_entities = list()
                for word_id_req, word_stat in zip(entity_request_ids, res_json['result']):
                    word_id = word_stat['id']
                    lemma = word_stat['lemma']
                    docs_cnt = word_stat['total_docs_count']
                    occurrences_cnt = word_stat['total_entities_count']
                    stat = word_stat['stat']
                    if StatGetter.check_wdb_entity(word_id, lemma, stat):
                        assert word_id_req == word_id
                        wdb_entity = WdbEntity(word_id, lemma, docs_cnt, occurrences_cnt, stat)
                        wdb_entities.append(wdb_entity)
                        found += 1
                    else:
                        not_found += 1

                if len(entity_request_ids) > 1:
                    found_rate_percent = int(100 * found / (found + not_found))
                    if found_rate_percent < 55:
                        logging.warning(
                            f"found ONLY {found} of {found+not_found} ({found_rate_percent}%) entities in WordsDB"
                        )
                    else:
                        logging.debug(
                            f"found {found} of {found+not_found} ({found_rate_percent}%) entities in WordsDB"
                        )
                else:
                    if found == 0:
                        logging.error(f"NOT FOUND entity in WordsDB: {entity_request_ids[0]}")

                return wdb_entities
            else:
                return None
        except Exception as e:
            raise RuntimeError(e)

    @lru_cache(maxsize=10000)
    @staticmethod
    def get_entity(endpoint, entity_id: str) -> List[WdbEntity] | None:
        req = jsonrpcclient.request(
            'GetEntity',
            params={
                "id": entity_id,
            },
        )
        return StatGetter.process_entities(endpoint, req)

    @lru_cache(maxsize=10000)
    @staticmethod
    def get_entities(endpoint, entity_ids: Tuple[str]) -> List[WdbEntity] | None:
        req = jsonrpcclient.request(
            'GetEntities',
            params={
                "ids": list(entity_ids),
            },
        )
        return StatGetter.process_entities(endpoint, req)

    @lru_cache(maxsize=100)
    @staticmethod
    def get_common_stat(endpoint) -> WordsDbCommonStat | None:
        req = jsonrpcclient.request(
            'GetStat',
            params={
                "all": True,
            },
        )
        res = requests.post(endpoint, json=req)
        try:
            res_json = res.json()
            if (
                'result' in res_json
                and res_json['result']
                and isinstance(res_json['result'], (dict,))
            ):
                labels_stat = collections.OrderedDict()
                for label in sorted(res_json['result']['labels'].keys()):
                    labels_stat[label] = LabelStat(*res_json['result']['labels'][label])

                last_added_time = res_json['result']['last_added_time']
                total_entities_count = res_json['result']['total_entities_count']
                total_docs_count = res_json['result']['total_docs_count']
                vocabulary_size = res_json['result']['vocabulary_size']
                labels_size = res_json['result']['labels_size']

                return WordsDbCommonStat(
                    last_added_time,
                    total_entities_count,
                    total_docs_count,
                    vocabulary_size,
                    labels_size,
                    labels_stat,
                )
            else:
                return None
        except Exception as e:
            raise RuntimeError(e)
