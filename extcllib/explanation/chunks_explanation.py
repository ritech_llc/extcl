#!/usr/bin/env python3
# coding: utf-8

import collections
from dataclasses import dataclass
import logging
from typing import Dict, List, OrderedDict, TypeAlias

from extcllib.dataset_handling import DatasetOneLabelType
from extcllib.linguistics import (
    EntityChunksType,
    Chunk,
)


@dataclass
class ExplanationChunk:
    """
    id -- chunk identifier (word_id/phrase_id or sentence_id/sentence_num). Must be unique in frame of one document.
    chunks -- contains 'chunks' attribute -- list of 'Chunk'-s (contains offset and length).
    score -- chunk weight in document, bigger means more important.
    """

    id: str
    chunks: List[List[Chunk]]
    score: float | None = None


ExplanationChunksType: TypeAlias = OrderedDict[DatasetOneLabelType, List[ExplanationChunk]]


class Explanation(object):
    explanation: ExplanationChunksType

    def __init__(self, explanation: ExplanationChunksType):
        self.explanation = explanation

    @staticmethod
    def dump(explanation: ExplanationChunksType):
        return {
            label: [
                {
                    'id': explanation_chunk.id,
                    'score': explanation_chunk.score,
                    'offsets': [
                        [(chunk.offset, chunk.length) for chunk in entity_chunks]
                        for entity_chunks in explanation_chunk.chunks
                    ],
                }
                for explanation_chunk in explanation_chunks
            ]
            for label, explanation_chunks in explanation.items()
        }


def get_chunks_explanation(
    entities_cnt,
    entity_id_to_chunks: Dict[str, List[List[Chunk]]],
    label_to_entities,
    predicted_labels,
    entity_scores,
    wdb_entities_dict,
    explanation_strategy,
    auto_explanation_strategy_entities_threshold,
) -> ExplanationChunksType | None:
    try:
        default_explanation: ExplanationChunksType
        default_explanation = collections.OrderedDict(
            [
                (
                    pred_label.label,
                    [
                        ExplanationChunk(
                            id=entity_id,
                            chunks=entity_id_to_chunks[entity_id],
                            score=entity_score,
                        )
                        for entity_id, entity_score in label_to_entities[pred_label.label]
                        if entity_id in entity_id_to_chunks
                    ],
                )
                for pred_label in predicted_labels
            ]
        )

        extended_explanation: ExplanationChunksType
        extended_explanation = collections.OrderedDict(
            [
                (
                    pred_label.label,
                    [
                        ExplanationChunk(
                            id=entity_id,
                            chunks=entity_id_to_chunks[entity_id],
                            score=entity_score,
                        )
                        for entity_id, entity_score in entity_scores
                        if entity_id in entity_id_to_chunks
                        and (
                            wdb_entities_dict is None
                            or (
                                wdb_entities_dict[entity_id].has_label(pred_label.label)
                                and entity_id in entity_id_to_chunks
                            )
                        )
                    ],
                )
                for pred_label in predicted_labels
            ]
        )
        s_explanation_strategy = explanation_strategy
        if s_explanation_strategy == 'auto':
            if len(default_explanation) == 0:
                s_explanation_strategy = 'extended'
            else:
                threshold = auto_explanation_strategy_entities_threshold
                assert 0.0 < threshold <= 1
                threshold_explanation_entities_cnt = entities_cnt * threshold
                min_explanation_default = len(
                    min(
                        [
                            (label, expl_chunks)
                            for label, expl_chunks in default_explanation.items()
                        ],
                        key=lambda x: len(x[1]),
                    )[1]
                )

                if min_explanation_default < threshold_explanation_entities_cnt:
                    s_explanation_strategy = 'extended'
                else:
                    s_explanation_strategy = 'default'

                logging.debug(f"Selected {s_explanation_strategy} strategy.")

        if s_explanation_strategy == 'default':
            explanation = default_explanation
        elif s_explanation_strategy == 'extended':
            explanation = extended_explanation
        else:
            raise RuntimeError(f"Unsupported explanation strategy: {explanation_strategy}")

    except IndexError as i_err:
        logging.exception(i_err)
        explanation = None

    return explanation
