#!/usr/bin/env python3
# coding: utf-8

from dataclasses import dataclass
import collections
import logging
import functools
import html
import json
import seaborn as sns
from typing import Dict, List, Tuple

from extcllib.dataset_handling import DatasetOneLabelType


def html_escape(t: str) -> str:
    return html.escape(t)


def chunks_comparator(item_1, item_2) -> int:
    offset_1, length_1 = item_1[0]
    offset_2, length_2 = item_2[0]
    if offset_1 > offset_2:
        return 1
    elif offset_1 < offset_2:
        return -1
    else:
        if length_1 > length_2:
            return -1
        else:
            return 1


@dataclass
class BaselineHighlighterOpts:
    max_labels_count: int = 255
    round_precision: int = 4


class BaselineHighlighter(object):
    def __init__(self, opts: BaselineHighlighterOpts = BaselineHighlighterOpts()):
        self.opts = opts

        self.base_colors = [
            f'{int(x[0]*255)},{int(x[1]*255)},{int(x[2]*255)}'
            for x in sns.color_palette(None, opts.max_labels_count)
        ]

        self.chunk_type_to_tag = {
            'sentence': 'p',
            'token': 'span',
        }

    def prepare_chunks_data(
        self,
        text: str,
        extcl_classify_res: Dict,
    ) -> Tuple[
        Dict[DatasetOneLabelType, float],
        List[Tuple[int, int, List[Tuple[str, str, DatasetOneLabelType, float]]]],
    ]:
        label_to_max_token_score: Dict[DatasetOneLabelType, float]
        label_to_max_token_score = dict()
        offset_to_chunks = collections.defaultdict(list)
        for label, label_explanation in extcl_classify_res['explanation'].items():
            for chunk in label_explanation:
                chunk_id: str
                score: float
                offsets: List[List[Tuple[int, int]]]
                chunk_id, score, offsets = chunk['id'], chunk['score'], chunk['offsets']
                for chunk_offset in offsets:
                    for offset, length in chunk_offset:
                        chunks_info = ('token', chunk_id, label, score)
                        offset_to_chunks[(offset, length)].append(chunks_info)
                        if label in label_to_max_token_score:
                            label_to_max_token_score[label] = max(
                                label_to_max_token_score[label], score
                            )
                        else:
                            label_to_max_token_score[label] = score

        offset_to_chunks_l: List[Tuple[int, int, List[Tuple[str, str, DatasetOneLabelType, float]]]]
        offset_to_chunks_l = [
            (offset, length, chunks_info)
            for (offset, length), chunks_info in sorted(
                [
                    (offset_and_length, chunks_info)
                    for offset_and_length, chunks_info in offset_to_chunks.items()
                ],
                key=functools.cmp_to_key(chunks_comparator),
            )
        ]
        return label_to_max_token_score, offset_to_chunks_l

    def prepare_highlighted_html(
        self, text, extcl_classify_res, label_to_max_token_score, offset_to_chunks
    ):
        label_to_base_color = {
            label: (base_color, score)
            for (label, score), base_color in zip(
                extcl_classify_res['predicted_labels'],
                self.base_colors,
            )
        }

        legend = "<form class='legend'>\n"
        legend += "<h3>Legend</h3>\n"
        legend += "<label><input class='ExtclLabelsCheckBoxAll' type='checkbox' onClick='extcl_checkbox_handler_all(this)' checked>all labels</input></label>\n"
        label_to_label_id = dict()
        for i, (label, (base_color, score)) in enumerate(label_to_base_color.items()):
            label_id = f'label_{i}'
            label_to_label_id[label] = label_id
            legend += f"<label style='background-color:rgb({base_color});'><input class='ExtclLabelsCheckBox' type='checkbox' onClick='extcl_checkbox_handler(this.form)' value='{label_id}' checked>label {html_escape(label)}, score: {score:.3f}</input></label>\n"
        legend += "</form>\n"

        document_html = '<div>'
        document_html += "<h3>Document</h3>\n"

        document_html += text[: offset_to_chunks[0][0]]
        self_check_text = text[: offset_to_chunks[0][0]]
        prev_offset = -1
        prev_length = -1

        chunks_scores: Dict[str, Dict[DatasetOneLabelType, float]]
        chunks_scores = collections.defaultdict(dict)
        label_to_chunks: Dict[DatasetOneLabelType, List[str]]
        label_to_chunks = collections.defaultdict(list)
        chunks_types = dict()

        chunk_ids_separator = ';'

        for offset, length, chunks_info in offset_to_chunks:
            if prev_offset == offset:
                logging.warning(f"This offset again: {offset}. Just skip!:(")
                continue

            chunk_ids = list()
            for chunk_type, chunk_id, label, score in chunks_info:
                label_id = label_to_label_id[label]
                label_to_chunks[label_id].append(chunk_id)
                chunks_types[chunk_id] = chunk_type
                if chunk_id in chunks_scores and label_id in chunks_scores[chunk_id]:
                    assert chunks_scores[chunk_id][label_id] == score
                else:
                    chunks_scores[chunk_id][label_id] = score

                chunk_ids.append(chunk_id)
                if prev_offset == offset and prev_length == length:
                    logging.warning(
                        f"This offset and length again: {(offset,length)}: {text[offset:offset+length]}. Just skip!:("
                    )
                    continue

            if prev_offset >= 0 and prev_offset + prev_length < offset:
                document_html += text[prev_offset + prev_length : offset]
                self_check_text += text[prev_offset + prev_length : offset]

            chunk = text[offset : offset + length]
            self_check_text += chunk
            document_html += (
                '<span chunk_ids="'
                + chunk_ids_separator.join([i for i in set(chunk_ids)])
                + '"'
                + ' style'
                + ' >'
                + html_escape(chunk)
                + '</span>'
            )
            prev_offset = offset
            prev_length = length

        if prev_offset + prev_length < len(text):
            document_html += text[prev_offset + prev_length :]
            self_check_text += text[prev_offset + prev_length :]

        document_html += '</div>\n'

        if self_check_text != text:
            raise RuntimeError("Failed to create highlighted text [self-check error]!")

        chunks_scores_n: Dict[str, List[Tuple[DatasetOneLabelType, float]]]
        chunks_scores_n = {
            chunk_id: sorted(
                [
                    (label, round(score, self.opts.round_precision))
                    for label, score in label_to_score.items()
                ],
                key=lambda x: x[1],
            )
            for chunk_id, label_to_score in chunks_scores.items()
        }

        js = f"""
        <script type="text/javascript">
        
        var ChunkIdsSep = '{chunk_ids_separator}';
        
        var ChunksScoresJson = '{json.dumps(chunks_scores_n)}';
        var LabelToChunksJson = '{json.dumps(label_to_chunks)}';
        var LabelToBaseColorJson = '{json.dumps({label_to_label_id[label]: base_color for label, (base_color, _) in label_to_base_color.items()})}';
        var LabelToMaxTokenScoreJson = '{json.dumps({label_to_label_id[label]: round(score, self.opts.round_precision) for label, score in label_to_max_token_score.items()})}';
        
        var ChunksScores = JSON.parse(ChunksScoresJson);
        var LabelToChunks = JSON.parse(LabelToChunksJson);
        var LabelToBaseColor = JSON.parse(LabelToBaseColorJson);
        var LabelToMaxTokenScore = JSON.parse(LabelToMaxTokenScoreJson);
        
        </script>
        """

        with open('etc/js/extcl_highlighter.js', 'r') as f:
            js2 = f.read()

        js2 = """<script type="text/javascript">\n""" + js2 + "\n</script>\n"

        highlighted_html = """<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">"""
        highlighted_html += """<html>\n<head>\n\t<meta http-equiv="Content-Type" content="text/html; charset=utf-8">"""
        highlighted_html += "<title>Extcl - Document View</title>\n</head>"
        highlighted_html += "\n\n" + js + "\n\n"
        highlighted_html += "\n\n" + js2 + "\n\n"
        highlighted_html += "\n<body>\n<div>\n"
        highlighted_html += "\n\n" + legend + "\n\n"
        highlighted_html += "\n\n" + document_html + "\n\n"

        highlighted_html += "\n</div>\n</body>\n"
        highlighted_html += "</html>\n"
        return highlighted_html

    def highlight(self, text, extcl_classify_res):
        label_to_max_token_score, offset_to_chunks = self.prepare_chunks_data(
            text, extcl_classify_res
        )

        if label_to_max_token_score and offset_to_chunks:
            highlighted_html = self.prepare_highlighted_html(
                text, extcl_classify_res, label_to_max_token_score, offset_to_chunks
            )
            return highlighted_html
        else:
            logging.error("Nothing to highlight!")
            return None
