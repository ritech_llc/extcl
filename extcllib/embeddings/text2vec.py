from gensim.models import Word2Vec
from sentence_transformers import SentenceTransformer
from laserembeddings import Laser

class W2VModel:
    def __init__(self):
        self.model = Word2Vec.load('model_W2V.model')

    def calc(self, document):
        res = []
        for i in document['ngrams']:
            res.append({
                'word': i,
                "embedding": self.model.wv[i]
            })

        return res

class SB_embeddings:
    def __init__(self):
        self.model = SentenceTransformer('all-MiniLM-L6-v2')
    def calc(self, document):
        return zip(document['sentences'], self.model.encode(document.sentences))
    
class Laser_embeddings:
    def __init__(self):
        self.laser = Laser()
    def calc(self, document):
        return zip(document.sentences, 
                   self.laser.embed_sentences(document['sentences'],lang=['ru','en']))