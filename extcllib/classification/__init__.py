#!/usr/bin/env python3
# coding: utf-8

from .baseline_classifier import BaselineClassifierOpts
from .vec_classifier import VectorClassifierOpts
from .classifier import ClassifierOpts, classificator_part_func
from .tuner import TuneOpts, tuner_cli

__all__ = [
    'add_classificator_opts',
    'add_baseline_classificator_opts',
    'add_vec_classificator_opts',
    'classificator_part_func',
    'add_tuner_opts',
    'tuner_cli',
]


def add_classificator_opts(parser):
    o = ClassifierOpts()
    parser_group = parser.add_argument_group('classificator')
    parser_group.add_argument(
        "--parts",
        type=str,
        nargs="+",
        default=o.parts,
        help="dataset part to classify (train, dev, test)",
    )
    parser_group.add_argument(
        "--top_l",
        type=int,
        default=o.top_l,
        help="top labels",
    )
    parser_group.add_argument(
        "--top_k",
        type=int,
        default=o.top_k,
        help="top k entities",
    )
    parser_group.add_argument(
        "--label_score_threshold",
        type=float,
        default=o.label_score_threshold,
        help="skip label less than threshold",
    )
    parser_group.add_argument(
        "--entity_score_min_threshold",
        type=float,
        default=o.entity_score_min_threshold,
        help="entity score min threshold",
    )
    parser_group.add_argument(
        "--entity_score_max_threshold",
        type=float,
        default=o.entity_score_max_threshold,
        help="entity score max threshold",
    )
    parser_group.add_argument(
        "--predict_score_aggregator",
        type=str,
        default=o.predict_score_aggregator,
        help="predict score aggregator",
    )
    parser_group.add_argument(
        "--entity_weight_method",
        type=str,
        default=o.entity_weight_method,
        help="entity weight method",
    )
    parser_group.add_argument(
        "--explanation_strategy",
        type=str,
        default=o.explanation_strategy,
        help="explanation strategy",
    )
    parser_group.add_argument(
        "--auto_explanation_strategy_entities_threshold",
        type=float,
        default=o.auto_explanation_strategy_entities_threshold,
        help="auto_explanation_strategy_entities_threshold",
    )
    parser_group.add_argument(
        "--save_highlighted_html",
        type=bool,
        default=o.save_highlighted_html,
        help="save_highlighted_html",
    )


def add_baseline_classificator_opts(parser):
    o = BaselineClassifierOpts()
    parser_group = parser.add_argument_group('baseline classificator')
    parser_group.add_argument(
        "--entity_total_docs_count_min_threshold",
        type=int,
        default=o.entity_total_docs_count_min_threshold,
        help="entity total documents count min threshold",
    )
    parser_group.add_argument(
        "--entity_total_count_min_threshold",
        type=int,
        default=o.entity_total_count_min_threshold,
        help="entity total count min threshold",
    )
    parser_group.add_argument(
        "--entity_label_max_count_threshold",
        type=int,
        default=o.entity_label_max_count_threshold,
        help="entity label max count threshold",
    )
    parser_group.add_argument(
        "--entity_labels_topk",
        type=int,
        default=o.entity_labels_topk,
        help="entity labels topk",
    )
    parser_group.add_argument(
        "--predict_score_type",
        type=str,
        default=o.predict_score_type,
        help="predict score type",
    )


def add_vec_classificator_opts(parser):
    o = VectorClassifierOpts()
    parser_group = parser.add_argument_group('vec classificator')
    parser_group.add_argument(
        "--top_v",
        type=int,
        default=o.top_v,
        help="",
    )
    parser_group.add_argument(
        "--sentence_sim_score_up_threshold",
        type=float,
        default=o.sentence_sim_score_up_threshold,
        help="",
    )
    parser_group.add_argument(
        "--sentence_sim_score_low_threshold",
        type=float,
        default=o.sentence_sim_score_low_threshold,
        help="",
    )
    parser_group.add_argument(
        "--sentence_min_weight",
        type=float,
        default=o.sentence_min_weight,
        help="",
    )
    parser_group.add_argument(
        "--use_weight_of_sentences",
        type=bool,
        default=o.use_weight_of_sentences,
        help="",
    )


def add_tuner_opts(parser):
    o = TuneOpts()
    parser_group = parser.add_argument_group('tune')
    parser_group.add_argument(
        "--max_evals",
        type=int,
        default=o.max_evals,
        help="max evals",
    )
    parser_group.add_argument(
        "--target_metrics",
        type=str,
        default=o.target_metrics,
        help="target metrics",
    )
    parser_group.add_argument(
        "--tune_part",
        type=str,
        default=o.tune_part,
        help="tune on dataset part",
    )
    parser_group.add_argument(
        "--top_l_vars",
        type=int,
        nargs="+",
        default=o.top_l_vars,
        help="",
    )
    parser_group.add_argument(
        "--top_k_vars",
        type=int,
        nargs="+",
        default=o.top_k_vars,
        help="",
    )
    parser_group.add_argument(
        "--label_score_threshold_vars",
        type=float,
        nargs="+",
        default=o.label_score_threshold_vars,
        help="",
    )
    parser_group.add_argument(
        "--entity_labels_topk_vars",
        type=int,
        nargs="+",
        default=o.entity_labels_topk_vars,
        help="",
    )
    parser_group.add_argument(
        "--entity_total_docs_count_min_threshold_vars",
        type=int,
        nargs="+",
        default=o.entity_total_docs_count_min_threshold_vars,
        help="",
    )
    parser_group.add_argument(
        "--entity_total_count_min_threshold_vars",
        type=int,
        nargs="+",
        default=o.entity_total_count_min_threshold_vars,
        help="",
    )
    parser_group.add_argument(
        "--entity_label_max_count_threshold_vars",
        type=int,
        nargs="+",
        default=o.entity_label_max_count_threshold_vars,
        help="",
    )
    parser_group.add_argument(
        "--entity_score_min_threshold_vars",
        type=float,
        nargs="+",
        default=o.entity_score_min_threshold_vars,
        help="",
    )
    parser_group.add_argument(
        "--entity_score_max_threshold_vars",
        type=float,
        nargs="+",
        default=o.entity_score_max_threshold_vars,
        help="",
    )
    parser_group.add_argument(
        "--top_v_vars",
        type=int,
        nargs="+",
        default=o.top_v_vars,
        help="",
    )
    parser_group.add_argument(
        "--sentence_sim_score_low_threshold_vars",
        type=float,
        nargs="+",
        default=o.sentence_sim_score_low_threshold_vars,
        help="",
    )
    parser_group.add_argument(
        "--sentence_sim_score_up_threshold_vars",
        type=float,
        nargs="+",
        default=o.sentence_sim_score_up_threshold_vars,
        help="",
    )
    parser_group.add_argument(
        "--sentence_min_weight_vars",
        type=float,
        nargs="+",
        default=o.sentence_min_weight_vars,
        help="",
    )
    parser_group.add_argument(
        "--use_weight_of_sentences_vars",
        type=bool,
        nargs="+",
        default=o.use_weight_of_sentences_vars,
        help="",
    )
