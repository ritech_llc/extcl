#!/usr/bin/env python3
# coding: utf-8
import argparse
import collections
import copy
import hashlib
import itertools
import json
import logging
import multiprocessing as mp
import os
import os.path as fs
import pprint
from dataclasses import dataclass, field
from functools import cmp_to_key

from typing import Counter, Dict, Generator, List, Tuple
import numpy as np

from extcllib.dataset_handling import get_dataset_handler_instance, ExTclStep, DatasetLabelType
from extcllib.multiprocessing import MultiProcessor
from extcllib.metrics_and_scores import (
    calc_confusion_matrix,
    merge_metrics,
)

from .classifier import Classifier, make_classification


@dataclass
class TuneOpts:
    max_evals: int = 30
    target_metrics: List[str] | str = field(
        default_factory=lambda: ["micro_f1", "macro_f1", "accuracy"]
    )
    tune_part: str = 'dev'
    top_l_vars: List[int] = field(default_factory=lambda: [5])
    top_k_vars: List[int] = field(default_factory=lambda: [50])
    label_score_threshold_vars: List[float] = field(default_factory=lambda: [0.01, 0.05])
    entity_labels_topk_vars: List[int] = field(default_factory=lambda: [2])
    entity_total_docs_count_min_threshold_vars: List[int] = field(default_factory=lambda: [2])
    entity_total_count_min_threshold_vars: List[int] = field(default_factory=lambda: [2])
    entity_label_max_count_threshold_vars: List[int] = field(default_factory=lambda: [2])
    entity_score_min_threshold_vars: List[float] = field(default_factory=lambda: [0.005, 0.01])
    entity_score_max_threshold_vars: List[float] = field(default_factory=lambda: [1.0])
    top_v_vars: List[int] = field(default_factory=lambda: [10])
    sentence_sim_score_up_threshold_vars: List[float] = field(default_factory=lambda: [1.01])
    sentence_sim_score_low_threshold_vars: List[float] = field(default_factory=lambda: [0.4])
    sentence_min_weight_vars: List[float] = field(default_factory=lambda: [-0.01])
    use_weight_of_sentences_vars: List[bool] = field(default_factory=lambda: [False])


def param_grid_generator(opts: argparse.Namespace) -> Generator[Dict, None, None]:
    param_grid = {
        'top_l': opts.top_l_vars,
        'top_k': opts.top_k_vars,
        'label_score_threshold': opts.label_score_threshold_vars,
        'entity_labels_topk': opts.entity_labels_topk_vars,
        'entity_total_docs_count_min_threshold': opts.entity_total_docs_count_min_threshold_vars,
        'entity_total_count_min_threshold': opts.entity_total_count_min_threshold_vars,
        'entity_label_max_count_threshold': opts.entity_label_max_count_threshold_vars,
        'entity_score_min_threshold': opts.entity_score_min_threshold_vars,
        'entity_score_max_threshold': opts.entity_score_max_threshold_vars,
        'top_v': opts.top_v_vars,
        'sentence_sim_score_low_threshold': opts.sentence_sim_score_low_threshold_vars,
        'sentence_sim_score_up_threshold': opts.sentence_sim_score_up_threshold_vars,
        'sentence_min_weight': opts.sentence_min_weight_vars,
        'use_weight_of_sentences': opts.use_weight_of_sentences_vars,
    }
    keys, values = zip(*param_grid.items())
    values = list(values)

    all_values = [v for v in itertools.product(*values)]
    np.random.seed(opts.random_state)

    if opts.max_evals < len(all_values):
        logging.info("shuffle and select max_evals grid variants")
        np.random.shuffle(all_values)
        all_values = all_values[: opts.max_evals]

    for i, v in enumerate(all_values):
        hyperparameters = dict(zip(keys, v))
        yield hyperparameters


def make_tune(d_h, classifier, opts, hyperparameters_str_hash):
    opts.skip_saving = True

    document_items_generator = d_h.steps_paths(
        step=ExTclStep.CLASSIFY,
        part=opts.tune_part,
    )

    counters: Counter
    counters = collections.Counter()
    gold_labels_res: List[Tuple] = list()
    predicted_labels_res: List[Tuple] = list()

    for item in document_items_generator:
        label: DatasetLabelType
        (
            raw_path,
            txt_path,
            ling_path,
            vect_path,
            classify_path,
            highlighted_path,
        ), label = item

        make_classification(
            classifier,
            raw_path,
            txt_path,
            ling_path,
            vect_path,
            classify_path,
            highlighted_path,
            label,
            gold_labels_res,
            predicted_labels_res,
            opts,
            counters,
        )

    labels, cnf_mtrx = calc_confusion_matrix(gold_labels_res, predicted_labels_res)
    logging.info(f"Hash: {hyperparameters_str_hash}. Tune counters: {counters}")
    logging.info(f"Hash: {hyperparameters_str_hash}. labels: {labels}")
    logging.info(f"Hash: {hyperparameters_str_hash}. cnf_mtrx: {cnf_mtrx}")
    metrics = merge_metrics(labels, cnf_mtrx)
    logging.info(f"Hash: {hyperparameters_str_hash}. metrics: {metrics}")
    return counters, metrics


def tuner_func(
    opts: argparse.Namespace, proc_name: str, input_queue: mp.Queue, output_queue: mp.Queue
):
    d_h = get_dataset_handler_instance(opts)
    classifier = Classifier(opts)

    counters: Counter
    counters = collections.Counter()
    for hyperparameters in MultiProcessor.iter_hyperparams(input_queue):
        hyperparameters_str = ''
        for k in sorted(hyperparameters.keys()):
            v = hyperparameters[k]
            hyperparameters_str += '_' + '_'.join([k, str(v)])

        hyperparameters_str_hash = hashlib.md5()
        hyperparameters_str_hash.update(hyperparameters_str.encode('utf-8'))
        hyperparameters_str = hyperparameters_str_hash.hexdigest()

        current_params_config_filename = hyperparameters_str + '.json'

        hyperparameters_work_file = d_h.get_filepath_in_workdir(
            current_params_config_filename, ExTclStep.TUNE
        )
        if fs.exists(hyperparameters_work_file):
            logging.info(
                f"File {hyperparameters_work_file} already exists. Loading result from file!"
            )
            with open(hyperparameters_work_file, 'r') as f:
                hyperparameters_res = json.load(f)
                metrics = hyperparameters_res['metrics']
        else:
            current_opts = copy.deepcopy(opts)
            current_opts.__dict__.update(hyperparameters)

            counters_, metrics = make_tune(d_h, classifier, current_opts, hyperparameters_str)

            for k, v in counters_.items():
                counters[k] += v

            with open(hyperparameters_work_file, 'w') as f:
                hyperparameters_res = hyperparameters
                hyperparameters_res['metrics'] = metrics
                f.write(json.dumps(hyperparameters_res, indent=4))

        if metrics:
            logging.info(
                f"Hash: {hyperparameters_str}. Metrics: {metrics}. Params: {hyperparameters}"
            )
        else:
            logging.error(
                f"Hash: {hyperparameters_str}. Bad metrics (skip adding to results): {metrics}. Params: {hyperparameters}"
            )

    MultiProcessor.put_to_output_queue(
        output_queue,
        {
            'proc_name': str(proc_name),
            'counters': {k: v for k, v in counters.items()},
        },
    )


def tuner_cli(opts):
    d_h = get_dataset_handler_instance(opts)

    if hasattr(opts, 'index_db_path'):
        opts.index_db_path = d_h.get_filepath_in_workdir(opts.index_db_path, ExTclStep.MAKE_INDEX)
    if hasattr(opts, 'index_path'):
        opts.index_path = d_h.get_filepath_in_workdir(opts.index_path, ExTclStep.MAKE_INDEX)

    best_params_config_filename = d_h.get_best_params_config_filename()

    hyperparams_generator = param_grid_generator(opts)

    res = MultiProcessor.process_step(
        opts=opts,
        func=tuner_func,
        input_queue_items_generator=hyperparams_generator,
        step_name='tune',
    )

    results = list()
    hyperparameters_work_file = d_h.get_filepath_in_workdir('best.json', ExTclStep.TUNE)
    for hyperparameters_work_file in os.listdir(fs.dirname(hyperparameters_work_file)):
        if hyperparameters_work_file == 'best.json':
            continue

        hyperparameters_work_file = d_h.get_filepath_in_workdir(
            hyperparameters_work_file, ExTclStep.TUNE
        )
        with open(hyperparameters_work_file, 'r') as f:
            hyperparameters_res = json.load(f)
            metrics = hyperparameters_res.pop('metrics')
            results.append((metrics, hyperparameters_res, hyperparameters_work_file))

    if isinstance(opts.target_metrics, (list,)):
        target_metrics = opts.target_metrics
    elif isinstance(opts.target_metrics, (str,)):
        target_metrics = [opts.target_metrics]

    def _sort_by_metrics(results1, results2):
        metrics1 = results1[0]
        metrics2 = results2[0]
        if metrics1 is None and metrics2 is None:
            return 1
        elif metrics1 is None and metrics2 is not None:
            return -1
        elif metrics2 is None and metrics1 is not None:
            return 1

        res = None
        for metric in target_metrics:
            if res:
                break
            if metric in metrics1 and metric in metrics2:
                if metrics1[metric] > metrics2[metric]:
                    res = 1
                    break
                if metrics1[metric] < metrics2[metric]:
                    res = -1
                    break
                else:
                    continue
            else:
                if metric in metrics1:
                    res = 1
                if metric in metrics2:
                    res = -1

        if res:
            return res

        return 1

    if results:
        results.sort(reverse=True, key=cmp_to_key(_sort_by_metrics))
    else:
        logging.error("There is empty results!")
        return

    if fs.exists(best_params_config_filename):
        os.remove(best_params_config_filename)

    os.symlink(os.path.basename(results[0][2]), best_params_config_filename)

    for i in range(min(6, len(results))):
        print("Top {} score: {}".format(i, results[i][0]))
        print("Top {} score: ".format(i))
        pprint.pprint(results[i][1])

    print("Best score: {}".format(results[0][0]))
    print("Best params: ")
    pprint.pprint(results[0][1])
