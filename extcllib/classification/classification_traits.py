#!/usr/bin/env python3
# coding: utf-8

from abc import ABC, abstractmethod
import collections
from dataclasses import dataclass, field
import logging
from typing import DefaultDict, Dict, List, Tuple

from extcllib.dataset_handling import DatasetOneLabelType
from extcllib.linguistics import (
    EntityStatChunks,
)
from extcllib.statistics import (
    WdbEntity,
)
from extcllib.metrics_and_scores import (
    tf_idf_bm25,
    tf_idf,
    aggregate_scores,
    round_item,
)
from extcllib.explanation import ExplanationChunksType


@dataclass
class ClassifierOpts:
    """
    Common classifier options.

    parts -- dataset parts, which need to be classified
    explanation_strategy -- auto/default/extended
    auto_explanation_strategy_entities_threshold --
    save_highlighted_html --

    entity_weight_method -- method to order entities inside document (tfidf/pseudo_bm25)
    entity_score_min_threshold -- do not consider entity with weight LESS than
    entity_score_max_threshold -- do not consider entity with weight BIGGER than
    top_k -- select top K entities for document
    predict_score_aggregator -- aggregating function for labels (sum/mult/avg)
    label_score_threshold -- if aggregated score of label LESS this, do not include to final result

    top_l -- # return top L labels count from final result

    """

    classify_method: str = 'baseline'
    parts: List[str] = field(default_factory=lambda: ['dev', 'test'])
    explanation_strategy: str = 'auto'
    auto_explanation_strategy_entities_threshold: float = 0.01
    save_highlighted_html: bool = False

    entity_weight_method: str = 'tfidf'
    entity_score_min_threshold: float = 0.00
    entity_score_max_threshold: float = 1.00
    top_k: int = 50
    predict_score_aggregator: str = 'sum'
    label_score_threshold: float = 0.05

    top_l: int = 1


@dataclass
class PredictedLabel:
    label: str
    score: float


def key_function_by_second_elem(item: Tuple[str, float]):
    return item[1]


def calc_entity_score(
    count: int,
    doc_entities_cnt: int,
    nq: int,
    total_docs_count: int,
    entity_weight_method: str,
    k1: float = 2.0,
    b: float = 0.75,
    avgdl: int = 3000,
) -> float:
    """
    Calculate entity (word or phrase) weight in the document
    :param count: entity occurrences count
    :param doc_entities_cnt: sum of entity occurrences in the document
    :param nq: число документов в коллекции, в которых встречается данное слово
    :param total_docs_count: collection/dataset cardinality
    :param entity_weight_method: method to measure entity weight (tfidf or pseudo_bm25)
    :param k1:
    :param b:
    :param avgdl:
    :return:
    """
    n = count
    nd = doc_entities_cnt
    N = total_docs_count
    match entity_weight_method:
        case 'tfidf':
            return tf_idf(n, nd, N, nq)
        case 'pseudo_bm25':
            return tf_idf_bm25(n, nd, N, nq, k1, b, avgdl)
        case _:
            raise RuntimeError(f"Unsupported entity_weight_method: {entity_weight_method}")


def calc_entity_scores(
    entities: List[Tuple[EntityStatChunks, WdbEntity]],
    doc_entities_cnt: int,
    total_docs_count: int,
    entity_weight_method: str,
) -> List[Tuple[str, float]]:
    return sorted(
        [
            (
                doc_entity.get_id(),
                calc_entity_score(
                    doc_entity.count,
                    doc_entities_cnt,
                    entity_wdb_stat.docs_cnt,
                    total_docs_count,
                    entity_weight_method,
                ),
            )
            for doc_entity, entity_wdb_stat in entities
        ],
        key=key_function_by_second_elem,
        reverse=True,
    )


def select_doc_entities(
    entity_scores: List[Tuple[str, float]],
    entity_score_min_threshold: int | float,
    entity_score_max_threshold: int | float,
    top_k: int,
) -> List[Tuple[str, float]]:
    selected_entities: List[Tuple[str, float]]
    selected_entities = [
        (entity_id, score)
        for entity_id, score in entity_scores
        if entity_score_min_threshold <= score <= entity_score_max_threshold
    ][:top_k]
    return selected_entities


def select_wdb_entities(
    wdb_entities: List[WdbEntity],
    entity_label_max_count_threshold: int | float,
    entity_total_docs_count_min_threshold: int | float,
    entity_total_count_min_threshold: int | float,
) -> Tuple[List[WdbEntity], Dict[str, WdbEntity]]:
    wdb_entities_ = list()
    wdb_entities_dict_ = dict()

    for wdb_entity in wdb_entities:
        if wdb_entity.label_count > entity_label_max_count_threshold:
            continue

        wdb_entity_stat_ = list()
        for wdb_entity_stat in wdb_entity.stat:
            if (
                wdb_entity_stat.total_docs_count >= entity_total_docs_count_min_threshold
                and wdb_entity_stat.total_count >= entity_total_count_min_threshold
            ):
                wdb_entity_stat_.append(wdb_entity_stat)

        if len(wdb_entity_stat_) > 0:
            wdb_entity_ = WdbEntity(
                word_id=wdb_entity.word_id,
                lemma=wdb_entity.lemma,
                docs_cnt=wdb_entity.docs_cnt,
                occurrences_cnt=wdb_entity.occurrences_cnt,
                raw_stat=[
                    (we_s_.label, (we_s_.total_count, we_s_.total_docs_count))
                    for we_s_ in wdb_entity_stat_
                ],
            )
            wdb_entities_.append(wdb_entity_)
            wdb_entities_dict_[wdb_entity.word_id] = wdb_entity_

    if len(wdb_entities) > 0 and len(wdb_entities_) == 0:
        logging.warning(
            f"Skipped all wdb entities by thresholds: docs_count({entity_total_docs_count_min_threshold}) occ_count({entity_total_count_min_threshold})"
        )

    return wdb_entities_, wdb_entities_dict_


def calculate_predictions(
    label_scores_lists: DefaultDict[DatasetOneLabelType, List],
    label_entities_scores_lists: DefaultDict[DatasetOneLabelType, Dict[str, List]],
    max_possible_score: float,
    predict_score_aggregator: str,
    top_l: int,
    label_score_threshold: float,
) -> Tuple[List[PredictedLabel], Dict[DatasetOneLabelType, List[Tuple[str, float]]]]:
    label_scores: Dict[DatasetOneLabelType, float] = dict()
    label_entities_scores: Dict[DatasetOneLabelType, Dict[str, float]] = dict()
    for label, scores in label_scores_lists.items():
        label_scores[label] = round_item(
            aggregate_scores(scores, predict_score_aggregator), max_possible_score
        )

        if label not in label_entities_scores:
            label_entities_scores[label] = dict()

        for entity_id, scores_ in label_entities_scores_lists[label].items():
            label_entities_scores[label][entity_id] = round_item(
                aggregate_scores(scores_, predict_score_aggregator), max_possible_score
            )

    predicted_labels_ = [
        PredictedLabel(label, score)
        for (label, score) in sorted(
            [(str(label), score) for label, score in label_scores.items()],
            key=key_function_by_second_elem,
            reverse=True,
        )
    ]

    if len(predicted_labels_) == 0:
        logging.error(f"Nothing predicted!")

    predicted_labels = [
        predicted_label
        for predicted_label in predicted_labels_[:top_l]
        if predicted_label.score >= label_score_threshold
    ]

    if len(predicted_labels_) > 0 and len(predicted_labels) == 0:
        logging.warning(
            f"Skipped all predicted labels by threshold {label_score_threshold} and top_l {top_l}: {predicted_labels_}"
        )

    label_to_entities: Dict[DatasetOneLabelType, List[Tuple[str, float]]]
    label_to_entities = collections.defaultdict(list)

    for label, entity_scores in label_entities_scores.items():
        label_to_entities[label] = sorted(
            [(entity_id, score) for entity_id, score in entity_scores.items()],
            key=key_function_by_second_elem,
            reverse=True,
        )

    return predicted_labels, label_to_entities


class ConcreteClassifier(ABC):
    def __init__(self, opts):
        self.opts = opts

    @abstractmethod
    def predict(
        self, *args, **kwargs
    ) -> (
        Tuple[List[PredictedLabel], ExplanationChunksType]
        | Tuple[None, None]
        | Tuple[List[PredictedLabel], None]
    ):
        pass
