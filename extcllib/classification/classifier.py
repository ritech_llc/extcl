#!/usr/bin/env python3
# coding: utf-8

import argparse
import collections
import json
import logging
import multiprocessing as mp
import numpy as np
import os
import os.path as fs
from typing import Counter, Dict, List, OrderedDict, Tuple, Set

import pylp.lp_doc

from extcllib.dataset_handling import DatasetLabelType, UndefinedLabelName, ExTclStep
from extcllib.text_extraction import TextExtractorOpts, TextExtractor, extract_text
from extcllib.linguistics import (
    UDPipeLingAnalyzerOpts,
    UDPipeLingAnalyzer,
    EntityStatChunks,
    EntityChunksType,
    ling_analyze,
    init_doc,
    calculate_stat_from_doc,
)
from extcllib.statistics import (
    GetStatOpts,
    StatGetter,
    StatBuilder,
    get_stat_builder_instance,
    WdbEntity,
)
from extcllib.vectorization import (
    VectorMaker,
    VectorMakerOpts,
    vectorize,
    get_sentence_chunks_from_doc,
    get_vector_maker_instance,
)
from extcllib.metrics_and_scores import (
    round_item,
    calc_confusion_matrix,
    merge_metrics,
)
from extcllib.explanation import (
    BaselineHighlighterOpts,
    BaselineHighlighter,
    ExplanationChunk,
    ExplanationChunksType,
    Explanation,
)

from .classification_traits import ClassifierOpts, ConcreteClassifier, PredictedLabel
from .baseline_classifier import get_baseline_classifier_instance
from .vec_classifier import get_vector_classifier_instance

from extcllib.multiprocessing import MultiProcessor


class Classifier(object):
    text_extractor: TextExtractor
    ling_analyzer: UDPipeLingAnalyzer | None
    ling_analyzer_path: str
    stat_builder: StatBuilder
    stat_getter: StatGetter
    classifier: ConcreteClassifier
    vec_maker: VectorMaker | None
    vec_maker_model_name: str

    classify_method: str

    entities: List[EntityStatChunks] | None
    sentence_id_to_chunks: Dict[int, Set[str]] | None
    doc: pylp.lp_doc.Doc | None
    entity_ids: Tuple[str, ...] | None
    wdb_entities: List[WdbEntity] | None

    def __init__(self, opts):
        self.opts = opts
        self.text_extractor = TextExtractor(TextExtractorOpts())
        self.stat_builder = get_stat_builder_instance(opts)
        self.stat_getter = StatGetter(GetStatOpts(endpoint=opts.endpoint))
        opts.common_stat = StatGetter.get_common_stat(self.stat_getter.opts.endpoint)

        self.classify_method = ''
        self.vec_maker_model_name = ''
        self.ling_analyzer = None
        self.ling_analyzer_path = opts.model_file
        self.entities = None
        self.sentence_id_to_chunks = None
        self.doc = None
        self.entity_ids = None
        self.wdb_entities = None

        self.init_classifier(opts)

    def init_classifier(self, opts=None):
        if opts is None:
            opts = self.opts

        if self.classify_method != opts.classify_method:
            if opts.classify_method == 'vectorize':
                if self.vec_maker_model_name != opts.model_name:
                    self.vec_maker = get_vector_maker_instance(opts)
                    self.vec_maker_model_name = opts.model_name
                    self.classifier = get_vector_classifier_instance(opts)
            elif opts.classify_method == 'baseline':
                self.vec_maker = None
                self.classifier = get_baseline_classifier_instance(
                    opts, stat_builder=self.stat_builder, stat_getter=self.stat_getter
                )
            else:
                raise RuntimeError(
                    f"unexpected classify_method {opts.classify_method}. Need 'baseline' or 'vectorize'"
                )

    def extract_text(self, raw_path, txt_path, opts=None):
        if not fs.exists(txt_path):
            extract_text(self.text_extractor, raw_path, txt_path)

    def ling_analyze(self, txt_path, ling_path, opts=None):
        if opts is None:
            opts = self.opts

        if not fs.exists(ling_path) or self.ling_analyzer_path != opts.model_file:
            if self.ling_analyzer is None or self.ling_analyzer_path != opts.model_file:
                self.ling_analyzer = UDPipeLingAnalyzer(
                    UDPipeLingAnalyzerOpts(
                        model_file=opts.model_file,
                        input_format=opts.input_format,
                        output_format=opts.output_format,
                    )
                )
                self.ling_analyzer_path = opts.model_file
                if self.ling_analyzer:
                    logging.info(f"Loaded model: {opts.model_file}")
                else:
                    raise RuntimeError(f"Failed to load model: {opts.model_file}")

            ling_analyze(self.ling_analyzer, txt_path, ling_path)

    def init_doc(self, raw_path, text, conll_raw_text):
        self.doc = init_doc(raw_path, text, conll_raw_text)

    def init_entities(self, raw_path, text, conll_raw_text, label, opts=None):
        if opts is None:
            opts = self.opts

        self.init_doc(raw_path, text, conll_raw_text)

        self.entities, self.sentence_id_to_chunks = calculate_stat_from_doc(opts, self.doc, label)

        self.entity_ids = tuple([entity.get_id() for entity in self.entities])
        self.wdb_entities = StatGetter.get_entities(self.stat_getter.opts.endpoint, self.entity_ids)

    def vectorize(
        self, txt_path, ling_path, vect_path, label
    ) -> Tuple[OrderedDict[int, EntityChunksType], List, np.ndarray] | None:
        if not fs.exists(vect_path):
            if self.vec_maker is not None:
                return vectorize(
                    self.vec_maker,
                    txt_path=txt_path,
                    ling_path=ling_path,
                    vec_path=vect_path,
                    label=label,
                )
            else:
                raise RuntimeError("Cannot vectorize with None vec_maker!")

        return None

    def prepare_document(self, raw_path, txt_path, ling_path, label, opts=None) -> Tuple[str, str]:
        if opts is None:
            opts = self.opts

        self.extract_text(raw_path, txt_path, opts)
        self.ling_analyze(txt_path, ling_path, opts)

        with open(txt_path, 'r') as f:
            text = f.read()

        with open(ling_path, 'r') as f:
            conll_raw_text = f.read()

        self.init_entities(raw_path, text, conll_raw_text, label, opts)
        return text, conll_raw_text

    def predict(
        self, txt_path, ling_path, vect_path, label, text=None, opts=None
    ) -> Tuple[List[PredictedLabel] | None, ExplanationChunksType | None]:
        if opts is None:
            opts = self.opts

        if opts.classify_method == 'baseline':
            predicted_labels, explanation = self.classifier.predict(
                self.classifier,
                entities=self.entities,
                wdb_entities=self.wdb_entities,
                opts=opts,
            )
        elif opts.classify_method == 'vectorize':
            vect_res = self.vectorize(txt_path, ling_path, vect_path, label)
            sentence_chunks: OrderedDict[int, EntityChunksType]
            if vect_res:
                sentence_chunks, _, embeddings = vect_res
            else:
                if text is None:
                    with open(txt_path, 'r') as f:
                        text = f.read()

                sentence_chunks = get_sentence_chunks_from_doc(self.doc, text)
                try:
                    embeddings = np.load(vect_path, allow_pickle=True)
                    assert len(sentence_chunks) == embeddings.shape[0]
                except Exception as e:
                    raise RuntimeError(f"Failed to vectorize {ling_path}. Err: {e}")

            predicted_labels, explanation = self.classifier.predict(
                self.classifier,
                entities=self.entities,
                wdb_entities=self.wdb_entities,
                sentence_id_to_chunks=self.sentence_id_to_chunks,
                sentence_chunks=sentence_chunks,
                vectors=embeddings,
                opts=opts,
            )
        else:
            raise RuntimeError(
                f"unexpected classify_method {opts.classify_method}. Need 'baseline' or 'vectorize'"
            )

        return predicted_labels, explanation

    def finalize(
        self,
        raw_path,
        txt_path,
        classify_path,
        highlighted_path,
        predicted_labels,
        explanation,
        gold_labels_set,
        gold_label_available,
        text,
        opts=None,
        counters=None,
    ):
        if opts is None:
            opts = self.opts
        if counters is None:
            counters = collections.Counter()

        skip_saving = opts.skip_saving if hasattr(opts, 'skip_saving') else False

        if gold_label_available:
            logging.info(f"Classified {raw_path} -> {predicted_labels} (gold: {gold_labels_set}). ")
        else:
            logging.info(f"Classified {raw_path} -> {predicted_labels}. ")

        counters['classified'] += 1

        if skip_saving or fs.exists(classify_path):
            logging.debug(
                f"Skip saving. {classify_path} already exists or param skip_saving is True"
            )
            counters['skip_saving'] += 1
        else:
            extcl_classify_res = {
                'predicted_labels': [
                    (pred_label.label, pred_label.score) for pred_label in predicted_labels
                ],
            }
            if explanation:
                extcl_classify_res['explanation'] = Explanation.dump(explanation)
                counters['explanation_added'] += 1
            else:
                logging.error(
                    "There is no explanation! Skip adding 'explanation' to extcl classify res."
                )
                counters['there_is_no_explanation'] += 1

            os.makedirs(fs.dirname(classify_path), exist_ok=True)
            with open(classify_path, 'w') as f:
                f.write(json.dumps(extcl_classify_res))

            logging.info(f"Saved {classify_path}")
            counters['saved_extcl'] += 1

            if (
                explanation
                and hasattr(opts, 'save_highlighted_html')
                and opts.save_highlighted_html
            ):
                highlighted_text = BaselineHighlighter(
                    BaselineHighlighterOpts(max_labels_count=opts.top_l),
                ).highlight(text, extcl_classify_res)
                if highlighted_text is not None:
                    os.makedirs(fs.dirname(highlighted_path), exist_ok=True)
                    with open(highlighted_path, 'w') as f:
                        f.write(highlighted_text)
                    logging.info(f"Saved {highlighted_path}")
                    counters['saved_highlighted'] += 1
                else:
                    logging.error(f"Nothing to save. highlighted_text is None: {txt_path}")
                    counters['failed_to_save_highlighted_text'] += 1


def make_classification(
    classifier,
    raw_path,
    txt_path,
    ling_path,
    vect_path,
    classify_path,
    highlighted_path,
    label,
    gold_labels_res,
    predicted_labels_res,
    opts=None,
    counters=None,
):
    if opts is None:
        opts = classifier.opts
    if counters is None:
        counters = collections.Counter()

    text, conll_raw_text = classifier.prepare_document(raw_path, txt_path, ling_path, label, opts)

    if classifier.wdb_entities is None or len(classifier.wdb_entities) == 0:
        logging.error(f"Failed to get wdb_entities for {raw_path}. Skip!")
        counters['docs_cnt'] += 1
        return

    predicted_labels: List[PredictedLabel] | None
    explanation: ExplanationChunksType | None
    predicted_labels, explanation = classifier.predict(
        txt_path, ling_path, vect_path, label, text, opts
    )

    if label:
        gold_labels = tuple(label)
        gold_labels_set = set(label)
    else:
        gold_labels = tuple()
        gold_labels_set = set()

    gold_label_available = gold_labels_set and gold_labels_set != {UndefinedLabelName}

    if predicted_labels is None or len(predicted_labels) == 0:
        if gold_label_available:
            logging.error(f"Failed to predict for {raw_path}. Gold: {gold_labels_set}. Skip!")
        else:
            logging.error(f"Failed to predict for {raw_path}. Skip!")

        gold_labels_res.append((txt_path, gold_labels))
        predicted_labels_res.append((txt_path, tuple([UndefinedLabelName])))
        return

    predicted_labels_list = [pred_label.label for pred_label in predicted_labels]

    counters['docs_cnt'] += 1
    gold_labels_res.append((txt_path, gold_labels))
    predicted_labels_res.append((txt_path, tuple(predicted_labels_list)))

    predicted_labels_set = set(predicted_labels_list)
    if predicted_labels_set == gold_labels_set:
        counters['correct_cnt'] += 1

    classifier.finalize(
        raw_path,
        txt_path,
        classify_path,
        highlighted_path,
        predicted_labels,
        explanation,
        gold_labels_set,
        gold_label_available,
        text,
        opts,
        counters,
    )
    return predicted_labels


def classificator_func(
    opts: argparse.Namespace, proc_name: str, input_queue: mp.Queue, output_queue: mp.Queue
):
    classifier = Classifier(opts)
    counters: Counter
    counters = collections.Counter()
    gold_labels_res: List[Tuple] = list()
    predicted_labels_res: List[Tuple] = list()

    predicted_labels_list: List = list()
    for item in MultiProcessor.iter_queue(input_queue):
        counters['items_cnt'] += 1
        try:
            label: DatasetLabelType
            (
                raw_path,
                txt_path,
                ling_path,
                vect_path,
                classify_path,
                highlighted_path,
            ), label = item

            predicted_labels: List[PredictedLabel] | None
            predicted_labels = make_classification(
                classifier,
                raw_path,
                txt_path,
                ling_path,
                vect_path,
                classify_path,
                highlighted_path,
                label,
                gold_labels_res,
                predicted_labels_res,
                opts,
                counters,
            )
            predicted_labels_list.append(predicted_labels)

        except Exception as e:
            logging.exception(f"Failed to process item {item}: {e}")

    labels, cnf_mtrx = calc_confusion_matrix(gold_labels_res, predicted_labels_res)
    logging.debug(f"labels: {labels}")
    logging.debug(f"cnf_mtrx: {cnf_mtrx}")
    out_elem = {
        'proc_name': str(proc_name),
        'labels': labels,
        'cnf_mtrx': cnf_mtrx,
    }
    if hasattr(opts, 'document_path') and opts.document_path is not None:
        assert len(predicted_labels_list) == 1
        out_elem.update(
            {'predicted_labels_one_doc': [(p.label, p.score) for p in predicted_labels_list[0]]}
        )

    out_elem.update(counters)
    MultiProcessor.put_to_output_queue(
        output_queue,
        out_elem,
    )


def classificator_part_func(opts, d_h, part='all'):
    logging.debug(f"[{part}] start processing")

    input_queue_items_generator = d_h.steps_paths(
        step=ExTclStep.CLASSIFY,
        part=part,
    )

    res = MultiProcessor.process_step(
        opts=opts,
        func=classificator_func,
        input_queue_items_generator=input_queue_items_generator,
        step_name='classify',
    )

    correct_cnt = res.get('correct_cnt', 0)
    docs_cnt = res.get('docs_cnt', 0)
    labels = res.get('labels', None)
    cnf_mtrx = res.get('cnf_mtrx', None)
    predicted_labels_one_doc = res.get('predicted_labels_one_doc', None)

    metrics = merge_metrics(labels, cnf_mtrx)
    if docs_cnt > 0:
        metrics['accuracy'] = round_item(correct_cnt, docs_cnt)
    else:
        metrics['accuracy'] = 0.0

    if not hasattr(opts, 'document_path') or opts.document_path is None or not opts.document_path:
        logging.info(f"[{part}] Correct {correct_cnt} / {docs_cnt}. Metrics {metrics}")
    return correct_cnt, docs_cnt, metrics, predicted_labels_one_doc
