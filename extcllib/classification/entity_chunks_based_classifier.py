#!/usr/bin/env python3
# coding: utf-8

from abc import abstractmethod
import copy
from dataclasses import dataclass
import logging
from typing import DefaultDict, Dict, List, Tuple

from extcllib.dataset_handling import DatasetOneLabelType
from extcllib.linguistics import Chunk, EntityStatChunks, EntityChunksType
from extcllib.statistics import (
    WdbEntity,
    WordsDbCommonStat,
    StatBaseOpts,
)

from extcllib.explanation import ExplanationChunksType

from .classification_traits import (
    ClassifierOpts,
    PredictedLabel,
    calculate_predictions,
    select_doc_entities,
    select_wdb_entities,
    calc_entity_scores,
    ConcreteClassifier,
)


@dataclass
class EntityBasedClassifierOpts(StatBaseOpts, ClassifierOpts):
    common_stat: WordsDbCommonStat | None = None
    entity_total_docs_count_min_threshold: int = 1
    entity_total_count_min_threshold: int = 1
    entity_label_max_count_threshold: int = 5
    entity_labels_topk: int = 5
    predict_score_type: str = 'x-tfidf'  # ir, docs_count, occurrences_count, x-tfidf


def process_entities(
    entities: List[EntityStatChunks], wdb_entities: List[WdbEntity], opts
) -> Tuple[int, List[WdbEntity], Dict[str, WdbEntity], List[Tuple[str, float]]] | None:
    doc_entities_cnt = sum([entity.count for entity in entities])
    if doc_entities_cnt == 0:
        logging.error("doc_entities_cnt == 0!")
        return None

    # Select entities
    wdb_entities_dict: Dict[str, WdbEntity]
    wdb_entities, wdb_entities_dict = select_wdb_entities(
        wdb_entities,
        opts.entity_label_max_count_threshold,
        opts.entity_total_docs_count_min_threshold,
        opts.entity_total_count_min_threshold,
    )

    # Calculate information retrieval scores
    entity_scores: List[Tuple[str, float]]
    entity_scores = calc_entity_scores(
        [
            (entity, wdb_entities_dict[entity.get_id()])
            for entity in entities
            if entity.get_id() in wdb_entities_dict
        ],
        doc_entities_cnt,
        opts.common_stat.total_docs_count,
        opts.entity_weight_method,
    )

    entity_scores = select_doc_entities(
        entity_scores,
        opts.entity_score_min_threshold,
        opts.entity_score_max_threshold,
        opts.top_k,
    )
    return doc_entities_cnt, wdb_entities, wdb_entities_dict, entity_scores


class EntityChunksBasedClassifier(ConcreteClassifier):
    def __init__(self, opts):
        super().__init__(opts)

    @abstractmethod
    def get_scores_lists(
        self,
        opts,
        entities: List[EntityStatChunks],
        wdb_entities: List[WdbEntity],
        entity_scores: List[Tuple[str, float]],
        wdb_entities_dict: Dict[str, WdbEntity],
        doc_entities_cnt: int,
        entity_scores_d: Dict[str | int, float],
        *args,
        **kwargs,
    ) -> Tuple[
        DefaultDict[DatasetOneLabelType, List],
        DefaultDict[DatasetOneLabelType, Dict[str, List]],
        float,
    ]:
        raise NotImplementedError("")

    def get_entity_chunks_scores(
        self,
        opts,
        entities: List[EntityStatChunks],
        wdb_entities: List[WdbEntity],
        entity_scores: List[Tuple[str, float]],
        entity_scores_d: Dict[str, float],
        *args,
        **kwargs,
    ) -> Tuple[Dict[str, EntityChunksType] | None, List[Tuple[str, float]]]:
        raise NotImplementedError("")

    @abstractmethod
    def get_entity_scores_dict(
        self,
        opts,
        entities: List[EntityStatChunks],
        wdb_entities: List[WdbEntity],
        entity_scores: List[Tuple[str, float]],
        *args,
        **kwargs,
    ) -> Dict[str, float]:
        raise NotImplementedError("")

    @abstractmethod
    def get_explanation(
        self,
        opts,
        entities: List[EntityStatChunks],
        wdb_entities: List[WdbEntity],
        entity_scores: List[Tuple[str, float]],
        entity_id_to_chunks: Dict[str, List[List[Chunk]]],
        label_to_entities: Dict[DatasetOneLabelType, List[Tuple[str, float]]],
        predicted_labels: List[PredictedLabel],
        wdb_entities_dict: Dict[str, WdbEntity],
    ) -> ExplanationChunksType | None:
        raise NotImplementedError("")

    @staticmethod
    def predict(
        cls,
        entities: List[EntityStatChunks],
        wdb_entities: List[WdbEntity],
        try_count: int = 0,
        opts: EntityBasedClassifierOpts | None = None,
        *args,
        **kwargs,
    ) -> (
        Tuple[List[PredictedLabel], ExplanationChunksType]
        | Tuple[None, None]
        | Tuple[List[PredictedLabel], None]
    ):
        if opts is None:
            opts = copy.deepcopy(cls.opts)

        doc_entities_cnt: int
        wdb_entities_dict: Dict[str, WdbEntity]
        entity_scores: List[Tuple[str, float]]

        r = process_entities(entities, wdb_entities, opts)
        if r is None:
            return None, None

        doc_entities_cnt, wdb_entities, wdb_entities_dict, entity_scores = r

        if opts.top_k > 0 and len(entity_scores) == 0 and try_count < 5:
            opts_ = copy.deepcopy(opts)
            if opts_.entity_total_docs_count_min_threshold > 0:
                opts_.entity_total_docs_count_min_threshold -= 1
            if opts_.entity_total_count_min_threshold > 0:
                opts_.entity_total_count_min_threshold -= 1
            opts_.entity_label_max_count_threshold += 1
            if try_count > 2:
                opts_.entity_labels_topk += 1
            return cls.predict(
                cls,
                entities=entities,
                wdb_entities=wdb_entities,
                try_count=try_count + 1,
                opts=opts_,
                *args,
                **kwargs,
            )

        entity_scores_d: Dict[str | int, float]
        entity_scores_d = cls.get_entity_scores_dict(
            opts, entities, wdb_entities, entity_scores, *args, **kwargs
        )

        label_scores_lists: DefaultDict[DatasetOneLabelType, List]
        label_entities_scores_lists: DefaultDict[DatasetOneLabelType, Dict[str, List]]
        label_scores_lists, label_entities_scores_lists, max_possible_score = cls.get_scores_lists(
            opts,
            entities,
            wdb_entities,
            entity_scores,
            wdb_entities_dict,
            doc_entities_cnt,
            entity_scores_d,
            *args,
            **kwargs,
        )

        predicted_labels: List[PredictedLabel]
        label_to_entities: Dict[DatasetOneLabelType, List[Tuple[str, float]]]
        predicted_labels, label_to_entities = calculate_predictions(
            label_scores_lists,
            label_entities_scores_lists,
            max_possible_score,
            opts.predict_score_aggregator,
            opts.top_l,
            opts.label_score_threshold,
        )

        entity_id_to_chunks: Dict[str, EntityChunksType] | None
        entity_id_to_chunks, entity_scores = cls.get_entity_chunks_scores(
            opts,
            entities,
            wdb_entities,
            entity_scores,
            entity_scores_d,
            *args,
            **kwargs,
        )

        if entity_id_to_chunks is None:
            logging.error("entity_id_to_chunks is None")

        explanation: ExplanationChunksType
        explanation = cls.get_explanation(
            opts,
            entities,
            wdb_entities,
            entity_scores,
            entity_id_to_chunks,
            label_to_entities,
            predicted_labels,
            wdb_entities_dict,
        )

        if predicted_labels is not None and explanation is None:
            logging.error("predicted labels: {predicted_labels}, but explanation is None")

        return predicted_labels, explanation
