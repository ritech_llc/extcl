#!/usr/bin/env python3
# coding: utf-8

import collections
from dataclasses import dataclass
from typing import DefaultDict, Dict, List, OrderedDict, Tuple

from extcllib.dataset_handling import DatasetOneLabelType
from extcllib.linguistics import Chunk, EntityStatChunks, EntityChunksType
from extcllib.statistics import (
    LabelStat,
    WdbEntity,
    WdbEntityLabelStat,
    GetStatOpts,
    StatGetter,
    StatBuilder,
    get_stat_builder_instance,
)
from extcllib.metrics_and_scores import (
    tf_idf_x,
)
from extcllib.explanation import get_chunks_explanation, ExplanationChunksType

from .classification_traits import PredictedLabel
from .entity_chunks_based_classifier import EntityBasedClassifierOpts, EntityChunksBasedClassifier


@dataclass
class BaselineClassifierOpts(EntityBasedClassifierOpts):
    entity_total_docs_count_min_threshold: int = 2
    entity_total_count_min_threshold: int = 2
    entity_label_max_count_threshold: int = 2
    entity_labels_topk: int = 2
    predict_score_type: str = 'x-tfidf'  # ir, docs_count, occurrences_count, x-tfidf


def baseline_calculate_scores_lists(
    entity_scores: List[Tuple[str, float]],
    wdb_entities_dict: Dict[str, WdbEntity],
    doc_entities_cnt: int,
    total_docs_count: int,
    labels_stat: OrderedDict[DatasetOneLabelType, LabelStat],
    entity_labels_topk: int,
    predict_score_type: str,
) -> Tuple[
    DefaultDict[DatasetOneLabelType, List], DefaultDict[DatasetOneLabelType, Dict[str, List]], float
]:
    label_scores_lists: DefaultDict[DatasetOneLabelType, List] = collections.defaultdict(list)
    label_entities_scores_lists: DefaultDict[
        DatasetOneLabelType, Dict[str, List]
    ] = collections.defaultdict()

    max_score = 0.0
    for entity_id, entity_score in entity_scores:
        wdb_entity: WdbEntity
        wdb_entity = wdb_entities_dict[entity_id]
        wdb_entity_stat: WdbEntityLabelStat
        for wdb_entity_stat in wdb_entity.stat[:entity_labels_topk]:
            label = wdb_entity_stat.label
            match predict_score_type:
                case "ir":
                    score = entity_score
                case "docs_count":
                    score = wdb_entity_stat.total_docs_count
                case "occurrences_count":
                    score = wdb_entity_stat.total_count
                case "x-tfidf":
                    n = wdb_entity_stat.total_count
                    nd = doc_entities_cnt
                    N = total_docs_count
                    nq = wdb_entity.docs_cnt
                    try:
                        wdb_label_stat = labels_stat[label]
                    except KeyError as kerr:
                        wdb_label_stat = LabelStat(1, 1)

                    elq = wdb_label_stat.total_docs_count

                    score = tf_idf_x(n, nd, N, nq, elq)
                case _:
                    raise RuntimeError(f"Unsupported predict_score_type: {predict_score_type}")

            if score > max_score:
                max_score = score

            label_scores_lists[label].append(score)
            if label not in label_entities_scores_lists:
                label_entities_scores_lists[label] = collections.defaultdict(list)

            label_entities_scores_lists[label][wdb_entity.get_id()].append(score)

    max_possible_score = len(entity_scores) * entity_labels_topk * max_score
    return label_scores_lists, label_entities_scores_lists, max_possible_score


class BaselineClassifier(EntityChunksBasedClassifier):
    """
    Classifier based on entities and traditional IR weights (TF-IDF and etc.).
    """

    def __init__(self, opts: BaselineClassifierOpts, stat_builder, stat_getter):
        super().__init__(opts)
        if self.opts.common_stat is None:
            raise RuntimeError("'common_stat' is expected in options.")

        self.stat_builder = stat_builder
        self.stat_getter = stat_getter

    def get_scores_lists(
        self,
        opts,
        entities: List[EntityStatChunks],
        wdb_entities: List[WdbEntity],
        entity_scores: List[Tuple[str, float]],
        wdb_entities_dict: Dict[str, WdbEntity],
        doc_entities_cnt: int,
        entity_scores_d: Dict[str | int, float],
        *args,
        **kwargs,
    ) -> Tuple[
        DefaultDict[DatasetOneLabelType, List],
        DefaultDict[DatasetOneLabelType, Dict[str, List]],
        float,
    ]:
        return baseline_calculate_scores_lists(
            entity_scores,
            wdb_entities_dict,
            doc_entities_cnt,
            opts.common_stat.total_docs_count,
            opts.common_stat.labels_stat,
            opts.entity_labels_topk,
            opts.predict_score_type,
        )

    def get_entity_chunks_scores(
        self,
        opts,
        entities: List[EntityStatChunks],
        wdb_entities: List[WdbEntity],
        entity_scores: List[Tuple[str, float]],
        entity_scores_d: Dict[str, float],
        *args,
        **kwargs,
    ) -> Tuple[Dict[str, EntityChunksType], List[Tuple[str, float]]]:
        entity_id_to_chunks = {entity.get_id(): entity.chunks for entity in entities}
        return entity_id_to_chunks, entity_scores

    def get_entity_scores_dict(
        self,
        opts,
        entities: List[EntityStatChunks],
        wdb_entities: List[WdbEntity],
        entity_scores: List[Tuple[str, float]],
        *args,
        **kwargs,
    ) -> Dict[str, float]:
        return {ent_id: score for ent_id, score in entity_scores}

    def get_explanation(
        self,
        opts,
        entities: List[EntityStatChunks],
        wdb_entities: List[WdbEntity],
        entity_scores: List[Tuple[str, float]],
        entity_id_to_chunks: Dict[str, List[List[Chunk]]],
        label_to_entities: Dict[DatasetOneLabelType, List[Tuple[str, float]]],
        predicted_labels: List[PredictedLabel],
        wdb_entities_dict: Dict[str, WdbEntity],
    ) -> ExplanationChunksType | None:
        return get_chunks_explanation(
            len(entity_id_to_chunks),
            entity_id_to_chunks,
            label_to_entities,
            predicted_labels,
            entity_scores,
            wdb_entities_dict,
            opts.explanation_strategy,
            opts.auto_explanation_strategy_entities_threshold,
        )


def get_baseline_classifier_instance(
    opts, stat_builder: StatBuilder | None = None, stat_getter: StatGetter | None = None
) -> BaselineClassifier:
    if stat_builder is None:
        stat_builder = get_stat_builder_instance(opts)
    if stat_getter is None:
        stat_getter = StatGetter(GetStatOpts(endpoint=opts.endpoint))

    return BaselineClassifier(
        BaselineClassifierOpts(
            endpoint=opts.endpoint,
            common_stat=opts.common_stat,
            top_l=opts.top_l,
            top_k=opts.top_k,
            label_score_threshold=opts.label_score_threshold,
            entity_total_docs_count_min_threshold=opts.entity_total_docs_count_min_threshold,
            entity_total_count_min_threshold=opts.entity_total_count_min_threshold,
            entity_label_max_count_threshold=opts.entity_label_max_count_threshold,
            entity_labels_topk=opts.entity_labels_topk,
            entity_score_min_threshold=opts.entity_score_min_threshold,
            entity_score_max_threshold=opts.entity_score_max_threshold,
            predict_score_type=opts.predict_score_type,
            predict_score_aggregator=opts.predict_score_aggregator,
            entity_weight_method=opts.entity_weight_method,
            explanation_strategy=opts.explanation_strategy,
            save_highlighted_html=opts.save_highlighted_html,
        ),
        stat_builder,
        stat_getter,
    )
