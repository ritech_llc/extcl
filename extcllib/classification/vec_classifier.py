#!/usr/bin/env python3
# coding: utf-8

import collections
from dataclasses import dataclass
import logging
import numpy as np
import faiss
import sqlite3
from typing import DefaultDict, Dict, OrderedDict, List, Tuple

from extcllib.dataset_handling import DatasetOneLabelType
from extcllib.linguistics import (
    Chunk,
    EntityStatChunks,
    EntityChunksType,
)
from extcllib.statistics import (
    WdbEntity,
)
from extcllib.vector_indexing import IndexBaseOpts
from extcllib.explanation import (
    ExplanationChunksType,
    get_chunks_explanation,
)

from .classification_traits import (
    PredictedLabel,
    key_function_by_second_elem,
)
from .baseline_classifier import BaselineClassifierOpts
from .entity_chunks_based_classifier import EntityChunksBasedClassifier


@dataclass
class VectorClassifierOpts(IndexBaseOpts, BaselineClassifierOpts):
    """
    top_v -- top V similar vectors for one sentence
    sentence_sim_score_up_threshold -- do not consider similar sentence with similarity score BIGGER than this value
    sentence_sim_score_low_threshold -- do not consider similar sentence with similarity score LESS than this value
    sentence_min_weight -- do not consider sentence that LESS that this weight
    use_weight_of_sentences -- mult IR-sentence weight by similarity score
    """

    top_v: int = 10
    sentence_sim_score_up_threshold: float = 1.01
    sentence_sim_score_low_threshold: float = 0.4
    sentence_min_weight: float = -0.01  # by default DO NOT reduce sentences, use all
    use_weight_of_sentences: bool = (
        False  # by default DO NOT multiply with sentence_similarity by sentence_weight
    )


class VectorClassifier(EntityChunksBasedClassifier):
    def __init__(self, opts: VectorClassifierOpts):
        super().__init__(opts)
        if self.opts.common_stat is None:
            raise RuntimeError("'common_stat' is expected in options.")

        logging.info(f"Reading index from {self.opts.index_path}")
        self.index = faiss.read_index(self.opts.index_path)
        logging.info(f"Done with reading index from {self.opts.index_path}")

        logging.info(f"Connecting to db {self.opts.index_db_path}")
        self.conn = sqlite3.connect(self.opts.index_db_path)
        logging.info(f"Connected to db {self.opts.index_db_path}")

    def _get_ids_of_similar_vectors(self, vectors: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """
        consider here, that index uses IP(inner-product) metric
        therefore, "distances" are cosine similarities
        :param vectors:
        :return: sentence ids, sentence similarities
        """
        faiss.normalize_L2(vectors)
        dst, ids = self.index.search(vectors, self.opts.top_v)
        return ids, dst

    def _get_sentence_ir_weights(
        self, entity_scores_d, sentence_id_to_chunks, sentence_min_weight
    ) -> Dict[str, float]:
        return {
            str(s_ent_id): score
            for s_ent_id, score in sorted(
                [
                    (
                        s_ent_id,
                        sum(
                            [
                                entity_scores_d[ent_id]
                                for ent_id in s_chunk_ids
                                if ent_id in entity_scores_d
                            ]
                        ),
                    )
                    for s_ent_id, s_chunk_ids in sentence_id_to_chunks.items()
                ],
                key=key_function_by_second_elem,
                reverse=True,
            )
            if score >= sentence_min_weight
        }

    def _get_labels_scores(
        self,
        vec_ids,
        similarities,
        sentence_chunks: OrderedDict[int, EntityChunksType],
        s_entity_scores_d,
    ) -> Tuple[
        DefaultDict[DatasetOneLabelType, List],
        DefaultDict[DatasetOneLabelType, Dict[str, List]],
        float,
    ]:
        label_scores_lists: DefaultDict[DatasetOneLabelType, List] = collections.defaultdict(list)
        label_entities_scores_lists: DefaultDict[
            DatasetOneLabelType, Dict[str, List]
        ] = collections.defaultdict()

        max_possible_score = len(sentence_chunks) * self.opts.top_v * 1.0

        for sent_num, sentence_id in zip(range(len(vec_ids)), sentence_chunks.keys()):
            entity_id = str(sentence_id)
            if entity_id not in s_entity_scores_d:
                continue

            for vec_num in range(self.opts.top_v):
                vec_id = vec_ids[sent_num][vec_num]
                score = similarities[sent_num][vec_num]

                low_threshold = self.opts.sentence_sim_score_low_threshold
                up_threshold = self.opts.sentence_sim_score_up_threshold
                assert low_threshold <= up_threshold

                if low_threshold <= score <= up_threshold:
                    labels = self.conn.execute(
                        "SELECT labels FROM vectors where vec_id=%s" % vec_id
                    ).fetchone()

                    if self.opts.use_weight_of_sentences:
                        score *= s_entity_scores_d[entity_id]

                    label: str | int
                    for label in labels:
                        label = str(label)
                        label_scores_lists[label].append(score)

                        if label not in label_entities_scores_lists:
                            label_entities_scores_lists[label] = collections.defaultdict(list)

                        label_entities_scores_lists[label][entity_id].append(score)
                else:
                    logging.debug(
                        f"For sentence {sentence_id} (chunks: {sentence_chunks[sentence_id]})skip {vec_num} with score {score}"
                    )
                    break

        return label_scores_lists, label_entities_scores_lists, max_possible_score

    def get_scores_lists(
        self,
        opts,
        entities: List[EntityStatChunks],
        wdb_entities: List[WdbEntity],
        entity_scores: List[Tuple[str, float]],
        wdb_entities_dict: Dict[str, WdbEntity],
        doc_entities_cnt: int,
        entity_scores_d: Dict[str | int, float],
        *args,
        **kwargs,
    ) -> Tuple[
        DefaultDict[DatasetOneLabelType, List],
        DefaultDict[DatasetOneLabelType, Dict[str, List]],
        float,
    ]:
        sentence_id_to_chunks: Dict[str, set]
        sentence_chunks: OrderedDict[int, EntityChunksType]
        vectors: np.ndarray

        sentence_id_to_chunks = kwargs.get('sentence_id_to_chunks', None)
        sentence_chunks = kwargs.get('sentence_chunks', None)
        vectors = kwargs.get('vectors', None)

        if sentence_id_to_chunks is None or sentence_chunks is None or vectors is None:
            raise RuntimeError("There is no enough parameters!")

        vec_ids, similarities = self._get_ids_of_similar_vectors(vectors)

        assert len(sentence_chunks) == len(vec_ids) == vectors.shape[0] == similarities.shape[0]

        label_scores_lists: DefaultDict[DatasetOneLabelType, List]
        label_entities_scores_lists: DefaultDict[DatasetOneLabelType, Dict[str, List]]
        max_possible_score: float

        (
            label_scores_lists,
            label_entities_scores_lists,
            max_possible_score,
        ) = self._get_labels_scores(vec_ids, similarities, sentence_chunks, entity_scores_d)
        return label_scores_lists, label_entities_scores_lists, max_possible_score

    def get_entity_chunks_scores(
        self,
        opts,
        entities: List[EntityStatChunks],
        wdb_entities: List[WdbEntity],
        entity_scores: List[Tuple[str, float]],
        entity_scores_d: Dict[str, float],
        *args,
        **kwargs,
    ) -> Tuple[Dict[str, EntityChunksType] | None, List[Tuple[str, float]]]:
        sentence_chunks = kwargs.get('sentence_chunks', None)
        s_entity_scores_d = entity_scores_d
        if sentence_chunks is None or s_entity_scores_d is None:
            raise RuntimeError("There is no 'sentence_chunks' or 's_entity_scores_d'!")

        s_entity_id_to_chunks = {
            str(entity_id): [s_entities[0]]
            for entity_id, s_entities in sentence_chunks.items()
            if str(entity_id) in s_entity_scores_d
        }
        s_entity_scores: List[Tuple[str, float]]
        s_entity_scores = [
            (entity_id, s_entity_scores_d[entity_id]) for entity_id in s_entity_scores_d.keys()
        ]
        return s_entity_id_to_chunks, s_entity_scores

    def get_entity_scores_dict(
        self,
        opts,
        entities: List[EntityStatChunks],
        wdb_entities: List[WdbEntity],
        entity_scores: List[Tuple[str, float]],
        *args,
        **kwargs,
    ) -> Dict[str, float]:
        sentence_id_to_chunks = kwargs.get('sentence_id_to_chunks', None)
        if sentence_id_to_chunks is None:
            raise RuntimeError("There is no 'sentence_id_to_chunks'!")

        entity_scores_d = {ent_id: score for ent_id, score in entity_scores}
        sentence_scores_d = self._get_sentence_ir_weights(
            entity_scores_d, sentence_id_to_chunks, opts.sentence_min_weight
        )
        return sentence_scores_d

    def get_explanation(
        self,
        opts,
        entities: List[EntityStatChunks],
        wdb_entities: List[WdbEntity],
        entity_scores: List[Tuple[str, float]],
        entity_id_to_chunks: Dict[str, List[List[Chunk]]],
        label_to_entities: Dict[DatasetOneLabelType, List[Tuple[str, float]]],
        predicted_labels: List[PredictedLabel],
        wdb_entities_dict: Dict[str, WdbEntity],
    ) -> ExplanationChunksType | None:
        return get_chunks_explanation(
            len(entity_id_to_chunks),
            entity_id_to_chunks,
            label_to_entities,
            predicted_labels,
            entity_scores,
            None,
            opts.explanation_strategy,
            opts.auto_explanation_strategy_entities_threshold,
        )


def get_vector_classifier_instance(opts):
    return VectorClassifier(
        VectorClassifierOpts(
            endpoint=opts.endpoint,
            common_stat=opts.common_stat,
            top_l=opts.top_l,
            top_k=opts.top_k,
            label_score_threshold=opts.label_score_threshold,
            entity_total_docs_count_min_threshold=opts.entity_total_docs_count_min_threshold,
            entity_total_count_min_threshold=opts.entity_total_count_min_threshold,
            entity_label_max_count_threshold=opts.entity_label_max_count_threshold,
            entity_labels_topk=opts.entity_labels_topk,
            entity_score_min_threshold=opts.entity_score_min_threshold,
            entity_score_max_threshold=opts.entity_score_max_threshold,
            predict_score_type=opts.predict_score_type,
            predict_score_aggregator=opts.predict_score_aggregator,
            entity_weight_method=opts.entity_weight_method,
            explanation_strategy=opts.explanation_strategy,
            save_highlighted_html=opts.save_highlighted_html,
            index_path=opts.index_path,
            index_db_path=opts.index_db_path,
            top_v=opts.top_v,
            sentence_sim_score_up_threshold=opts.sentence_sim_score_up_threshold,
            sentence_sim_score_low_threshold=opts.sentence_sim_score_low_threshold,
            sentence_min_weight=opts.sentence_min_weight,
            use_weight_of_sentences=opts.use_weight_of_sentences,
        )
    )
