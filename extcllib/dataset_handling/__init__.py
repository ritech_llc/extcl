#!/usr/bin/env python3
# coding: utf-8

import argparse
import importlib
import logging

from .dataset_types import (
    DatasetItemType,
    DatasetOneLabelType,
    DatasetLabelType,
    DataClassType,
    DatasetPartType,
    UndefinedLabelName,
)
from .dataset import (
    CommonOpts,
    DatasetOpts,
    DataSet,
    DataSetReader,
    DataSetReaderOpts,
    DataSetTrainTest,
    DataSetTrainDevTest,
)
from .dataset_split_strategy import (
    SplitStrategyOpts,
    TrainTestSplitStrategyOpts,
    TrainDevTestSplitStrategyOpts,
    SplitStrategy,
    SplitStrategyOptsType,
    TrainTestSplitStrategy,
    TrainDevTestSplitStrategy,
)

from .dataset_handler import DatasetHandler, ExTclStep

from .default_items_handlers import extract_doc_id_from_item, read_parts_items

__all__ = [
    'DatasetItemType',
    'DatasetOneLabelType',
    'DatasetLabelType',
    'DataClassType',
    'DatasetPartType',
    'UndefinedLabelName',
    'CommonOpts',
    'DatasetOpts',
    'DataSet',
    'DataSetReader',
    'DataSetReaderOpts',
    'DataSetTrainTest',
    'DataSetTrainDevTest',
    'SplitStrategyOpts',
    'TrainTestSplitStrategyOpts',
    'TrainDevTestSplitStrategyOpts',
    'SplitStrategy',
    'SplitStrategyOptsType',
    'TrainTestSplitStrategy',
    'TrainDevTestSplitStrategy',
    'DatasetHandler',
    'ExTclStep',
    'extract_doc_id_from_item',
    'read_parts_items',
    'add_common_opts',
    'add_dataset_opts',
    'get_dataset_handler_instance',
]


def add_common_opts(parser):
    o = CommonOpts()
    parser_group = parser.add_argument_group('common')
    parser_group.add_argument(
        "--dataset_reader",
        '-dr',
        type=str,
        default=o.dataset_reader,
        help="dataset reader class name, defined in 'dataset_readers'",
    )
    parser_group.add_argument(
        "--input",
        '-i',
        type=str,
        default=o.input,
        help="input (csv-filename, input directory or text file)",
    )
    parser_group.add_argument(
        "--work_dir", '-w', type=str, default=o.work_dir, help="working directory"
    )
    parser_group.add_argument(
        "--proc_cnt", '-p', type=int, default=o.proc_cnt, help="parallel processes count"
    )
    parser_group.add_argument(
        "--classify_method",
        type=str,
        default=o.classify_method,
        help="classify method ('baseline' or 'vectorize')",
    )
    parser_group.add_argument(
        "--model_name",
        "-m",
        type=str,
        default=o.model_name,
        help="vectorize model name (SentenceBert/LASER/Word2Vec)",
    )
    parser_group.add_argument(
        "--use_first_symbol_as_label",
        type=bool,
        default=o.use_first_symbol_as_label,
        help="use_first_symbol_as_label",
    )


def add_dataset_opts(parser):
    o = DatasetOpts()
    parser_group = parser.add_argument_group('dataset')
    parser_group.add_argument(
        "--train_prop", type=float, default=o.train_prop, help="train proportion"
    )
    parser_group.add_argument("--dev_prop", type=float, default=o.dev_prop, help="dev proportion")
    parser_group.add_argument(
        "--test_prop", type=float, default=o.test_prop, help="test proportion"
    )
    parser_group.add_argument(
        "--random_state", type=int, default=o.random_state, help="random state"
    )
    parser_group.add_argument("--batch_size", type=int, default=o.batch_size, help="batch size")
    parser_group.add_argument("--train", type=str, default=o.train, help="train part")
    parser_group.add_argument("--dev", type=str, default=o.dev, help="dev part")
    parser_group.add_argument("--test", type=str, default=o.test, help="test part")


def get_dataset_handler_instance(opts: argparse.Namespace):
    if hasattr(opts, 'document_path') and opts.document_path:
        opts.input = opts.document_path
        opts.dataset_reader = 'OneFileDatasetReader'
        opts.parts = ['test']

    assert opts.dataset_reader
    try:
        dataset_readers = importlib.import_module('extcllib.dataset_readers')
        dataset_reader_cls = getattr(dataset_readers, opts.dataset_reader)
    except Exception as e:
        logging.exception(e)
        raise RuntimeError(e)

    d_h = DatasetHandler(
        opts=DatasetOpts(
            input=opts.input,
            work_dir=opts.work_dir,
            proc_cnt=opts.proc_cnt,
            train_prop=opts.train_prop,
            dev_prop=opts.dev_prop,
            test_prop=opts.test_prop,
            random_state=opts.random_state,
            batch_size=opts.batch_size,
            train=opts.train,
            dev=opts.dev,
            test=opts.test,
            classify_method=opts.classify_method if hasattr(opts, 'classify_method') else None,
            model_name=opts.model_name if hasattr(opts, 'model_name') else None,
            use_first_symbol_as_label=opts.use_first_symbol_as_label
            if hasattr(opts, 'use_first_symbol_as_label')
            else False,
        ),
        dataset_reader_cls=dataset_reader_cls,
    )
    return d_h
