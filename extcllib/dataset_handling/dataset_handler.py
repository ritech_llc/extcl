#!/usr/bin/env python3
# coding: utf-8

from enum import Enum, auto
import logging
import os
import os.path as fs
from pathlib import Path
from typing import Callable, Generator, Tuple, Union


from .dataset_types import DatasetPartType, DatasetItemType, DatasetLabelType
from .dataset import DatasetOpts
from .dataset_split_strategy import (
    SplitStrategyOptsType,
    TrainTestSplitStrategyOpts,
    TrainDevTestSplitStrategyOpts,
)


class ExTclStep(Enum):
    RAW_DOCS = auto()
    TEXT_EXTRACT = auto()
    LING_ANALYSIS = auto()
    VECTORIZE = auto()
    MAKE_INDEX = auto()
    CALC_STAT = auto()
    UPLOAD_STAT = auto()
    TUNE = auto()
    CLASSIFY = auto()
    HIGHLIGHT = auto()


class ItemReader(object):
    def read(self, item: DatasetItemType) -> str:
        raise NotImplementedError("This must be implemented!")


class DefaultFileReader(ItemReader):
    def read(self, path: str) -> str:
        with open(path, 'r') as f:
            content = f.read()
        return content


class DatasetHandler(object):
    def __init__(
        self,
        opts: DatasetOpts,
        dataset_reader_cls,
    ):
        self.opts = opts
        if not self.opts.input or not isinstance(self.opts.input, (str,)):
            raise RuntimeError("Input param is empty!")

        if not fs.exists(self.opts.input):
            raise RuntimeError(f"Input does not exits: {self.opts.input}")

        os.makedirs(self.opts.work_dir, exist_ok=True)

        # Store vectors, index files and hyperparams for different vectorize models
        suffix = ''
        if self.opts.classify_method == 'vectorize' and self.opts.model_name:
            suffix = '_' + self.opts.model_name.lower()

        self._step_to_subdirname = {
            ExTclStep.RAW_DOCS: 'raw',
            ExTclStep.TEXT_EXTRACT: 'txt',
            ExTclStep.LING_ANALYSIS: 'ling',
            ExTclStep.CALC_STAT: 'stat',
            ExTclStep.VECTORIZE: 'vector' + suffix,
            ExTclStep.MAKE_INDEX: 'index' + suffix,
            ExTclStep.TUNE: 'hyperparams' + suffix,
            ExTclStep.CLASSIFY: 'classify' + suffix,
            ExTclStep.HIGHLIGHT: 'highlighted' + suffix,
        }

        self._step_to_file_extension = {
            ExTclStep.TEXT_EXTRACT: '.txt',
            ExTclStep.LING_ANALYSIS: '.conll',
            ExTclStep.VECTORIZE: '.vec',
            ExTclStep.MAKE_INDEX: '.index',
            ExTclStep.CALC_STAT: '.stat',
            ExTclStep.CLASSIFY: '.extcl',
            ExTclStep.HIGHLIGHT: '.html',
        }

        self._step_to_input_output_list = {
            ExTclStep.TEXT_EXTRACT: (ExTclStep.RAW_DOCS, ExTclStep.TEXT_EXTRACT),
            ExTclStep.LING_ANALYSIS: (ExTclStep.TEXT_EXTRACT, ExTclStep.LING_ANALYSIS),
            ExTclStep.VECTORIZE: (
                ExTclStep.TEXT_EXTRACT,
                ExTclStep.LING_ANALYSIS,
                ExTclStep.VECTORIZE,
            ),
            ExTclStep.MAKE_INDEX: (ExTclStep.VECTORIZE,),
            ExTclStep.CALC_STAT: (
                ExTclStep.TEXT_EXTRACT,
                ExTclStep.LING_ANALYSIS,
                ExTclStep.CALC_STAT,
            ),
            ExTclStep.UPLOAD_STAT: (ExTclStep.CALC_STAT,),
            ExTclStep.CLASSIFY: (
                ExTclStep.RAW_DOCS,
                ExTclStep.TEXT_EXTRACT,
                ExTclStep.LING_ANALYSIS,
                ExTclStep.VECTORIZE,
                ExTclStep.CLASSIFY,
                ExTclStep.HIGHLIGHT,
            ),
        }

        self.dataset = None

        split: Union[
            Tuple[float, float], Tuple[float, float, float], Tuple[str, str], Tuple[str, str, str]
        ]
        if self.opts.train and self.opts.test and self.opts.dev:
            split = (self.opts.train, self.opts.dev, self.opts.test)
        elif self.opts.train and self.opts.test:
            split = (self.opts.train, self.opts.test)
        elif self.opts.train_prop and self.opts.test_prop and self.opts.dev_prop:
            split = (self.opts.train_prop, self.opts.dev_prop, self.opts.test_prop)
        elif self.opts.train_prop and self.opts.test_prop:
            split = (self.opts.train_prop, self.opts.test_prop)
        else:
            raise RuntimeError(
                "Opts requires train/dev/test OR train/test OR train_prop/dev_prop/test_prop OR train_prop/test_prop"
            )

        split_strategy_opts: SplitStrategyOptsType
        if len(split) == 2:
            split_strategy_opts = TrainTestSplitStrategyOpts(
                split=split,
                random_state=self.opts.random_state,
            )
        elif len(split) == 3:
            split_strategy_opts = TrainDevTestSplitStrategyOpts(
                split=split,
                random_state=self.opts.random_state,
            )
        else:
            raise RuntimeError(f"Incorrect size of split: {split}")

        self.dataset_reader_cls = dataset_reader_cls
        if not self.dataset_reader_cls:
            raise RuntimeError(f"dataset_reader_cls is not correct!")

        self.dataset = dataset_reader_cls(
            opts=self.opts,
            split_strategy_opts=split_strategy_opts,
        )

        if hasattr(self.dataset, 'extract_item_path'):
            self.extract_item_path = self.dataset.extract_item_path
        else:
            self.extract_item_path = lambda x: str(x)

    def step_workdir(self, step: ExTclStep) -> str:
        w = fs.join(self.opts.work_dir, self._step_to_subdirname[step])
        os.makedirs(w, exist_ok=True)
        return w

    def _items(
        self, step: ExTclStep = ExTclStep.RAW_DOCS, part: DatasetPartType = 'all'
    ) -> Generator[Tuple[DatasetItemType, DatasetLabelType], None, None]:
        if not self.dataset:
            raise RuntimeError("Dataset was not initialized!")

        if part == 'all':
            for x, y in self._items(step, 'train'):
                yield x, y
            for x, y in self._items(step, 'dev'):
                yield x, y
            for x, y in self._items(step, 'test'):
                yield x, y

        elif part == 'train':
            if hasattr(self.dataset, 'train_items'):
                for x, y in self.dataset.train_items():
                    yield x, y
            else:
                logging.error("There is no train_items in dataset!")
        elif part == 'dev':
            if hasattr(self.dataset, 'dev_items'):
                for x, y in self.dataset.dev_items():
                    yield x, y
            else:
                logging.error("There is no dev_items in dataset!")
        elif part == 'test':
            if hasattr(self.dataset, 'test_items'):
                for x, y in self.dataset.test_items():
                    yield x, y
            else:
                logging.error("There is no test_items in dataset!")
        else:
            raise RuntimeError(f"Unexpected part name: {part}!")

    def items(
        self, step: ExTclStep = ExTclStep.RAW_DOCS, part: DatasetPartType = "all"
    ) -> Generator[Tuple[DatasetItemType, DatasetLabelType], None, None]:
        for x, y in self._items(step, part):
            yield x, y

    def path_items(
        self,
        step: ExTclStep = ExTclStep.RAW_DOCS,
        part: DatasetPartType = "all",
        path_extractor: Callable[[DatasetItemType], str] | None = None,
    ) -> Generator[Tuple[str, DatasetLabelType], None, None]:
        if path_extractor is None:
            path_extractor = self.extract_item_path

        for x, y in self.items(step, part):
            item_orig_path = path_extractor(x)

            input_basedirs = list()
            if fs.isfile(self.opts.input):
                input_basedirs.append(fs.dirname(self.opts.input))
            elif fs.isdir(self.opts.input):
                input_basedirs.append(self.opts.input)

            if fs.isfile(item_orig_path):
                input_basedirs.append(fs.dirname(item_orig_path))

            input_basedir = ''
            for input_basedir_ in input_basedirs:
                item_orig_path_ = fs.join(input_basedir_, item_orig_path)
                if fs.isfile(item_orig_path_):
                    input_basedir = input_basedir_
                    item_orig_path = item_orig_path_
                    break

            if not fs.isfile(item_orig_path):
                raise RuntimeError(
                    f"File {item_orig_path} (or other variations) does not exists! x: {x}, y: {y}"
                )
            if step == ExTclStep.RAW_DOCS:
                p = item_orig_path
            else:
                if input_basedir:
                    item_orig_path = fs.relpath(item_orig_path, input_basedir)
                p = fs.join(self.step_workdir(step), item_orig_path)
                extension = self._step_to_file_extension[step]
                p = fs.join(fs.dirname(p), Path(p).stem + extension)

            yield p, y

    def read_items(
        self,
        step: ExTclStep = ExTclStep.RAW_DOCS,
        part: DatasetPartType = "all",
        item_reader=DefaultFileReader(),
        path_extractor: Callable[[DatasetItemType], str] | None = None,
    ) -> Generator[Tuple[DatasetItemType, DatasetLabelType], None, None]:
        for path, y in self.path_items(step, part, path_extractor):
            yield item_reader.read(path), y

    def steps_paths(
        self,
        step: ExTclStep,
        part: DatasetPartType = "all",
        path_extractor: Callable[[DatasetItemType], str] | None = None,
    ) -> Generator[Tuple[Tuple[DatasetItemType, ...], DatasetLabelType], None, None]:
        steps = self._step_to_input_output_list[step]

        assert len(steps) > 0

        step_generators = [
            self.path_items(step=step, part=part, path_extractor=path_extractor) for step in steps
        ]
        for step_pairs in zip(*step_generators):
            step_paths_ = list()
            step_path, step_label = step_pairs[0]
            label = step_label
            step_paths_.append(step_path)

            for step_path, step_label in step_pairs[1:]:
                assert label == step_label
                step_paths_.append(step_path)

            yield tuple(step_paths_), label

    def get_filepath_in_workdir(self, filename, step: ExTclStep | None = None):
        if step is None:
            directory = self.opts.work_dir
        else:
            directory = fs.join(self.opts.work_dir, self._step_to_subdirname[step])

        os.makedirs(directory, exist_ok=True)
        return fs.join(directory, filename)

    def get_best_params_config_filename(self):
        return self.get_filepath_in_workdir('best.json', ExTclStep.TUNE)
