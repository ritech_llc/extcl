#!/usr/bin/env python3
# coding: utf-8

from dataclasses import dataclass
from collections.abc import Callable
import os.path as fs
from sklearn.model_selection import train_test_split
from typing import (
    Dict,
    List,
    Tuple,
    Union,
)

from .dataset_types import DatasetItemType, DatasetLabelType
from .default_items_handlers import read_parts_items, extract_doc_id_from_item


@dataclass
class SplitStrategyOpts:
    split: Tuple[str, ...] | Tuple[float, ...]
    random_state: int = 38


@dataclass
class TrainTestSplitStrategyOpts(SplitStrategyOpts):
    def __init__(
        self, split: Tuple[str, ...] | Tuple[float, ...] = (0.9, 0.1), random_state: int = 38
    ):
        assert len(split) == 2
        super().__init__(split, random_state)


@dataclass
class TrainDevTestSplitStrategyOpts(SplitStrategyOpts):
    def __init__(
        self,
        split: Tuple[str, ...] | Tuple[float, ...] = (0.9, 0.05, 0.05),
        random_state: int = 38,
    ):
        assert len(split) == 3
        super().__init__(split, random_state)


SplitStrategyOptsType = Union[TrainTestSplitStrategyOpts, TrainDevTestSplitStrategyOpts]


class SplitStrategy(object):
    def __init__(
        self,
        opts: SplitStrategyOptsType = TrainTestSplitStrategyOpts(),
        parts_items_reader: Callable[[str, Dict, Dict], Tuple[List, List]] = read_parts_items,
        doc_id_extractor_from_item: Callable[
            [DatasetItemType, DatasetLabelType], str
        ] = extract_doc_id_from_item,
    ):
        self.opts = opts
        self.parts_items_reader = parts_items_reader
        self.doc_id_extractor_from_item = doc_id_extractor_from_item

    def split(self, x: Tuple, y: Tuple) -> Tuple[List[DatasetItemType | DatasetItemType], ...]:
        raise NotImplementedError("")


class TrainTestSplitStrategy(SplitStrategy):
    def __init__(
        self,
        opts: SplitStrategyOptsType = TrainTestSplitStrategyOpts(),
        parts_items_reader: Callable[[str, Dict, Dict], Tuple[List, List]] = read_parts_items,
        doc_id_extractor_from_item: Callable[
            [DatasetItemType, DatasetLabelType], str
        ] = extract_doc_id_from_item,
    ):
        super().__init__(opts, parts_items_reader, doc_id_extractor_from_item)

    def split(self, x: Tuple, y: Tuple) -> Tuple[List[DatasetItemType | DatasetItemType], ...]:
        assert len(self.opts.split) == 2
        train, test = self.opts.split

        if isinstance(train, (float,)) and isinstance(test, (float,)):
            assert train + test == 1.0
        elif isinstance(train, (str,)) and isinstance(test, (str,)):
            assert fs.isfile(train)
            assert fs.isfile(test)

            x_indices = dict()
            y_indices = dict()
            for i, (x_, y_) in enumerate(zip(x, y)):
                x_id = self.doc_id_extractor_from_item(x_, y_)
                x_indices[x_id] = i
                y_indices[x_id] = i

            x_train, y_train = self.parts_items_reader(train, x_indices, y_indices)
            x_test, y_test = self.parts_items_reader(test, x_indices, y_indices)

            return x_train, x_test, y_train, y_test
        else:
            raise RuntimeError(f"Unsupported train-test: {train} {test}")

        x_train, x_test, y_train, y_test = train_test_split(
            [i for i in range(len(x))],
            [i for i in range(len(y))],
            test_size=test,
            random_state=self.opts.random_state,
        )
        return x_train, x_test, y_train, y_test


class TrainDevTestSplitStrategy(SplitStrategy):
    def __init__(
        self,
        opts: SplitStrategyOptsType = TrainDevTestSplitStrategyOpts(),
        parts_items_reader: Callable[[str, Dict, Dict], Tuple[List, List]] = read_parts_items,
        doc_id_extractor_from_item: Callable[
            [DatasetItemType, DatasetLabelType], str
        ] = extract_doc_id_from_item,
    ):
        super().__init__(opts, parts_items_reader, doc_id_extractor_from_item)

    def split(self, x: Tuple, y: Tuple) -> Tuple[List[DatasetItemType | DatasetItemType], ...]:
        assert len(self.opts.split) == 3
        train, dev, test = self.opts.split

        if isinstance(train, (float,)) and isinstance(dev, (float,)) and isinstance(test, (float,)):
            assert train + dev + test == 1.0

            x_train, x_test, y_train, y_test = train_test_split(
                [i for i in range(len(x))],
                [i for i in range(len(y))],
                test_size=dev + test,
                random_state=self.opts.random_state,
            )
            x_dev, x_test, y_dev, y_test = train_test_split(
                x_test,
                y_test,
                test_size=test / (dev + test),
                random_state=self.opts.random_state,
            )
            return x_train, x_dev, x_test, y_train, y_dev, y_test

        elif isinstance(train, (str,)) and isinstance(dev, (str,)) and isinstance(test, (str,)):
            assert fs.isfile(train)
            assert fs.isfile(dev)
            assert fs.isfile(test)

            x_indices = dict()
            y_indices = dict()
            for i, (x_, y_) in enumerate(zip(x, y)):
                x_id = self.doc_id_extractor_from_item(x_, y_)
                x_indices[x_id] = i
                y_indices[x_id] = i

            x_train, y_train = self.parts_items_reader(train, x_indices, y_indices)
            x_dev, y_dev = self.parts_items_reader(dev, x_indices, y_indices)
            x_test, y_test = self.parts_items_reader(test, x_indices, y_indices)

            return x_train, x_dev, x_test, y_train, y_dev, y_test
        else:
            raise RuntimeError(f"Unsupported train-dev-test: {train} {dev} {test}")
