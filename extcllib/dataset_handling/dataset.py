#!/usr/bin/env python3
# coding: utf-8

from dataclasses import dataclass

import psutil

from typing import (
    Generator,
    List,
    Tuple,
)

from .dataset_types import DatasetItemType, DatasetLabelType
from .dataset_split_strategy import SplitStrategy


@dataclass
class DataSetReaderOpts:
    path: str = ''


class DataSetReader(object):
    def __init__(self, opts: DataSetReaderOpts = DataSetReaderOpts()):
        self.opts = opts

    def get_items_list(self) -> Tuple[Tuple[DatasetItemType, ...], Tuple[DatasetLabelType, ...]]:
        raise NotImplementedError("")


@dataclass
class CommonOpts:
    input: str = ''
    dataset_reader: str = 'OneFileDatasetReader'
    work_dir: str = "extcl_workdir"
    proc_cnt: int = psutil.cpu_count(logical=False)
    classify_method: str = 'baseline'
    model_name: str | None = None
    use_first_symbol_as_label: bool = False


@dataclass
class DatasetOpts(CommonOpts):
    train_prop: float = 0.8
    dev_prop: float = 0.1
    test_prop: float = 0.1
    random_state: int = 38
    batch_size: int = 1000
    train: str | None = None
    dev: str | None = None
    test: str | None = None


class DataSet(object):
    x: Tuple[DatasetItemType, ...]
    y: Tuple[DatasetLabelType, ...]
    x_train: List[int]
    x_test: List[int]
    y_train: List[int]
    y_test: List[int]

    def __init__(
        self,
        opts: DataSetReaderOpts = DataSetReaderOpts(),
        reader: DataSetReader = DataSetReader(),
        splitter: SplitStrategy = SplitStrategy(),
    ):
        self.opts = opts
        self.reader = reader
        self.splitter = splitter
        self.x, self.y = self.reader.get_items_list()
        self.init()

    def init(self):
        raise NotImplementedError("")

    def train_items(self) -> Generator[Tuple[DatasetItemType, DatasetLabelType], None, None]:
        for x_i, y_i in zip(self.x_train, self.y_train):
            yield self.x[x_i], self.y[y_i]

    def test_items(self) -> Generator[Tuple[DatasetItemType, DatasetLabelType], None, None]:
        for x_i, y_i in zip(self.x_test, self.y_test):
            yield self.x[x_i], self.y[y_i]


class DataSetTrainTest(DataSet):
    def __init__(
        self,
        opts: DataSetReaderOpts = DataSetReaderOpts(),
        reader: DataSetReader = DataSetReader(),
        splitter: SplitStrategy = SplitStrategy(),
    ):
        super().__init__(opts, reader, splitter)

    def init(self):
        self.x_train, self.x_test, self.y_train, self.y_test = self.splitter.split(self.x, self.y)


class DataSetTrainDevTest(DataSet):
    x_dev: List[int]
    y_dev: List[int]

    def __init__(
        self,
        opts: DataSetReaderOpts = DataSetReaderOpts(),
        reader: DataSetReader = DataSetReader(),
        splitter: SplitStrategy = SplitStrategy(),
    ):
        super().__init__(opts, reader, splitter)
        self.x, self.y = self.reader.get_items_list()

    def init(self):
        (
            self.x_train,
            self.x_dev,
            self.x_test,
            self.y_train,
            self.y_dev,
            self.y_test,
        ) = self.splitter.split(self.x, self.y)

    def dev_items(self) -> Generator[Tuple[DatasetItemType, DatasetLabelType], None, None]:
        for x_i, y_i in zip(self.x_dev, self.y_dev):
            yield self.x[x_i], self.y[y_i]
