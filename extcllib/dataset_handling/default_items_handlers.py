#!/usr/bin/env python3
# coding: utf-8

import csv
from pathlib import Path
from typing import (
    Dict,
    List,
    Tuple,
)

from .dataset_types import DatasetItemType, DatasetLabelType


def read_parts_items(filename: str, x_indices: Dict, y_indices: Dict) -> Tuple[List, List]:
    """
    reads dataset part from filename
    :param filename: file contains a subset of the dataset with document identifiers (doc_id-s)
    :param x_indices: dictionary with all dataset items (doc_id -> x_value)
    :param y_indices: dictionary with all dataset items (doc_id -> y_value)
    :return: two lists (x values and y values)
    """
    x_items = []
    y_items = []
    with open(filename, 'r', newline='') as f:
        csv_reader = csv.reader(f, delimiter=',')
        for row in csv_reader:
            try:
                x_i, _ = row
            except ValueError as v_err:
                x_i = row[0]

            x_i = x_i.strip()

            x_items.append(x_indices[x_i])
            y_items.append(y_indices[x_i])
    return x_items, y_items


def extract_doc_id_from_item(x: DatasetItemType, y: DatasetLabelType, idx: int = 1) -> str:
    """

    Function expects filepath in position {idx} of param `x`.
    Filepath (the name) is using as doc_id.

    :param x: dataset item (';'-separated csv-row or list/tuple/other (must support item assignment),
    :param y: dataset label (does not used in this function)
    :param idx: index of filepath
    :return: document identifier (doc_id)
    """
    try:
        if isinstance(x, (str,)):
            x_id = Path(x.split(';')[idx]).name
            return x_id
        else:
            raise AttributeError("")
    except AttributeError as atr_err:
        if isinstance(x, (list, tuple)) and len(x) >= 2:
            x_id = Path(x[idx]).name
            return x_id
        else:
            raise RuntimeError(f"Failed to extract doc_id from '{x}'")
