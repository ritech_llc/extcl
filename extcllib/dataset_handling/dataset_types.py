#!/usr/bin/env python3
# coding: utf-8

from typing import (
    Any,
    ClassVar,
    Dict,
    Literal,
    Protocol,
    Tuple,
    Union,
    TypeAlias,
)

DatasetItemType: TypeAlias = Any
DatasetOneLabelType: TypeAlias = Union[int, str]
DatasetLabelType: TypeAlias = Tuple[DatasetOneLabelType, ...]

DatasetPartType: TypeAlias = Literal['all', 'train', 'dev', 'test']


class DataClassType(Protocol):
    __dataclass_fields__: ClassVar[Dict]


UndefinedLabelName = 'UndefinedLabel'
