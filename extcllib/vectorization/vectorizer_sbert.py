#!/usr/bin/env python3
# coding: utf-8

import argparse
import collections
from dataclasses import dataclass, field
import logging
import multiprocessing as mp
import numpy as np
import os
import os.path as fs
from typing import OrderedDict, List, Tuple

from sentence_transformers import SentenceTransformer
from laser_encoders import LaserEncoderPipeline
import gensim
import gensim.downloader

import pylp.lp_doc
from pylp.converter_conll_ud_v1 import ConverterConllUDV1
from pylp.lp_doc import Doc as LingProcDoc
from pylp.common import STOP_WORD_POS_TAGS

from extcllib.multiprocessing import MultiProcessor
from extcllib.dataset_handling import DatasetLabelType
from extcllib.vector_indexing import IndexBaseOpts
from extcllib.statistics import Chunk, EntityChunk, EntityChunksType


@dataclass
class VectorMakerOpts(IndexBaseOpts):
    model_language: str = 'rus'
    sbert_model_name: str = field(default_factory=lambda: 'distiluse-base-multilingual-cased-v1')

    def is_model_lang_russian(self):
        return self.model_language.lower() in ['ru', 'rus', 'russian']

    def is_model_lang_english(self):
        return self.model_language.lower() in ['en', 'eng', 'english']

    def get_laser_lang_code(self):
        """
        see more https://github.com/facebookresearch/LASER/blob/main/laser_encoders/language_list.py
        :return:
        """
        if self.is_model_lang_russian():
            laser_lang_code = "rus_Cyrl"
        elif self.is_model_lang_english():
            laser_lang_code = "eng_Latn"
        else:
            raise RuntimeError(f"Unsupported lang yet: {self.model_language}")

        return laser_lang_code
        

    def get_w2v_model_name(self):
        if self.is_model_lang_russian():
            model_name = "word2vec-ruscorpora-300"
        elif self.is_model_lang_english():
            model_name = "glove-wiki-gigaword-300"
        else:
            raise RuntimeError(f"Unsupported lang yet: {self.model_language}")
        return model_name


class VectorMaker(object):
    def __init__(self, opts: VectorMakerOpts = VectorMakerOpts()):
        self.opts = opts
        self.model = None
        self.load_model()

    def load_model(self):
        if self.model is None:
            if self.opts.model_name == "SentenceBert":
                self.model = SentenceTransformer(self.opts.sbert_model_name)
            elif self.opts.model_name == "LASER":
                self.model = LaserEncoderPipeline(lang=self.opts.get_laser_lang_code())
            elif self.opts.model_name == "Word2Vec":
                self.model = gensim.downloader.load(self.opts.get_w2v_model_name())
            else:
                raise RuntimeError(f"Unsupported model name: {self.opts.model_name}")

            logging.info(f"Loaded model: {self.opts.model_name}")

        if self.model is None:
            raise RuntimeError(f"Failed to load model: {self.opts.model_name}")

    def get_vectors(
        self, doc_id: str | int, text: str, conll_raw_text: str, label: DatasetLabelType
    ) -> Tuple[OrderedDict[int, EntityChunksType], List, np.ndarray]:
        self.load_model()
        if self.opts.model_name == "Word2Vec":
            return get_w2v_sentence_vectors(
                self.model, doc_id, text, conll_raw_text, label, self.opts
            )
        else:
            return get_sentence_vectors(self.model, doc_id, text, conll_raw_text, label, self.opts)


def add_vec_opts(parser):
    o = VectorMakerOpts()
    parser_group = parser.add_argument_group('vector')
    parser_group.add_argument(
        "--model_language",
        "-l",
        type=str,
        default=o.model_language,
        help="model language (for LASER)",
    )
    parser_group.add_argument(
        "--sbert_model_name", "-sm", type=str, default=o.sbert_model_name, help="sbert model name"
    )


def get_sentence_chunks_from_doc(
    doc: pylp.lp_doc.Doc, text: str
) -> OrderedDict[int, EntityChunksType]:
    res: OrderedDict[int, EntityChunksType] = collections.OrderedDict()
    for s_id, s in enumerate(doc.sents()):
        s_words_list = s.to_dict()['words']
        s_len = len(s_words_list)
        sent_start_pos = s_words_list[0]['O']
        sent_end_pos = s_words_list[s_len - 1]['O'] + s_words_list[s_len - 1]['S']
        sent_length = sent_end_pos - sent_start_pos

        words_cnt = 0
        for w in s.words():
            if w.pos_tag not in STOP_WORD_POS_TAGS:
                words_cnt += 1

        if words_cnt < 1:
            sent_text = text[sent_start_pos : sent_start_pos + sent_length]
            logging.debug(f"Skip this sentence: {sent_text}")
            continue

        if s_id not in res:
            res[s_id] = list()

        res[s_id].append(EntityChunk([Chunk(sent_start_pos, sent_length)]))

    return res


def get_sentence_chunks(
    doc_id: str | int, text: str, conll_raw_text: str
) -> OrderedDict[int, EntityChunksType]:
    doc = LingProcDoc(str(doc_id))
    converter = ConverterConllUDV1()
    doc = converter(text=text, conll_raw_text=conll_raw_text, doc=doc)
    return get_sentence_chunks_from_doc(doc, text)


def get_w2v_sentence_vectors(
    model,
    doc_id: str | int,
    text: str,
    conll_raw_text: str,
    label: DatasetLabelType,
    opts: VectorMakerOpts = VectorMakerOpts(),
) -> Tuple[OrderedDict[int, EntityChunksType], List, np.ndarray]:
    doc = LingProcDoc(str(doc_id))
    converter = ConverterConllUDV1()
    doc = converter(text=text, conll_raw_text=conll_raw_text, doc=doc)
    sentence_chunks = get_sentence_chunks(doc_id, text, conll_raw_text)
    sentence_chunks_ids = list(sentence_chunks.keys())
    embeddings, sentences = [], []
    for s_id, s in enumerate(doc.sents()):
        if s_id not in sentence_chunks_ids:
            continue
        s_words_list = s.to_dict()['words']
        words_vectors = []
        for w in s_words_list:
            if w['p'] not in STOP_WORD_POS_TAGS:
                res_lemma = (
                    w['lemma'] + "_" + w['p'].name if opts.is_model_lang_russian() else w['lemma']
                )
                try:
                    words_vectors.append(model[res_lemma])
                except KeyError as e:
                    continue
        if len(words_vectors) > 1:
            embeddings.append(np.mean(words_vectors, axis=0))
        else:
            embeddings.append(np.zeros(300, dtype="float32"))
        sent_chunks = sentence_chunks[s_id]
        sent_chunk = sent_chunks[0].chunks[0]
        offset, length = sent_chunk.offset, sent_chunk.length
        sentences.append(text[offset : offset + length])

    embeddings = np.array(embeddings)
    return sentence_chunks, sentences, embeddings


def get_sentence_vectors(
    model,
    doc_id: str | int,
    text: str,
    conll_raw_text: str,
    label: DatasetLabelType,
    opts: VectorMakerOpts = VectorMakerOpts(),
) -> Tuple[OrderedDict[int, EntityChunksType], List, np.ndarray]:
    sentence_chunks: OrderedDict[int, EntityChunksType]
    sentence_chunks = get_sentence_chunks(doc_id, text, conll_raw_text)
    sentences = []
    for sent_id, sent_chunks in sentence_chunks.items():
        assert len(sent_chunks) == 1
        assert len(sent_chunks[0].chunks) == 1
        sent_chunk = sent_chunks[0].chunks[0]
        offset, length = sent_chunk.offset, sent_chunk.length
        sentences.append(text[offset : offset + length])

    if opts.model_name == "SentenceBert":
        embeddings = model.encode(sentences)
    elif opts.model_name == "LASER":
        embeddings = model.encode_sentences(sentences)
    else:
        raise RuntimeError(f"Unsupported model name: {opts.model_name}")

    assert len(sentence_chunks) == len(sentences) == embeddings.shape[0]

    return sentence_chunks, sentences, embeddings


def vectorize(
    vector_maker: VectorMaker,
    txt_path: str,
    ling_path: str,
    vec_path: str,
    label: DatasetLabelType,
    counters=None,
) -> Tuple[OrderedDict[int, EntityChunksType], List, np.ndarray] | None:
    if counters is None:
        counters = collections.Counter()

    if fs.exists(vec_path):
        logging.info(f"Skip. {vec_path} already exists.")
        counters["skip"] += 1
        return

    assert fs.exists(txt_path)
    assert fs.exists(ling_path)

    with open(txt_path, 'r') as f:
        text = f.read()

    with open(ling_path, 'r') as f:
        conll_raw_text = f.read()

    try:
        sentence_chunks, sentences, vec_result = vector_maker.get_vectors(
            doc_id=vec_path, text=text, conll_raw_text=conll_raw_text, label=label
        )
        counters["built_vec"] += 1
    except RuntimeError as r_err:
        logging.error(f"Failed to get vector: {ling_path}: {str(r_err)}")
        counters["failed_to_get_vector"] += 1
        return

    os.makedirs(fs.dirname(vec_path), exist_ok=True)
    np.array(vec_result).dump(vec_path)

    counters["vec_success"] += 1
    logging.info(f"Vector built {ling_path} -> {vec_path}")
    return sentence_chunks, sentences, vec_result


def vec_func(
    opts: argparse.Namespace, proc_name: str, input_queue: mp.Queue, output_queue: mp.Queue
):
    vec_maker = VectorMaker(
        VectorMakerOpts(
            model_language=opts.model_language,
            model_name=opts.model_name,
            sbert_model_name=opts.sbert_model_name,
        )
    )

    counters = collections.Counter()

    for item in MultiProcessor.iter_queue(input_queue):
        counters['docs_cnt'] += 1
        try:
            label: DatasetLabelType
            (txt_path, ling_path, vec_path), label = item
            vectorize(
                vec_maker,
                txt_path=txt_path,
                ling_path=ling_path,
                vec_path=vec_path,
                label=label,
                counters=counters,
            )
        except Exception as e:
            counters["fail_to_build_vector_for_item"] += 1
            logging.exception(f"Failed to build vector for item {item}: {e}")

    res = {
        'proc_name': str(proc_name),
    }
    res.update(counters)

    MultiProcessor.put_to_output_queue(output_queue, res)
