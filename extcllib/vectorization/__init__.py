#!/usr/bin/env python3
# coding: utf-8

from .vectorizer import (
    VectorMakerOpts,
    VectorMaker,
    get_sentence_chunks_from_doc,
    get_sentence_chunks,
    get_sentence_vectors,
    vectorize,
    vec_func,
    get_vector_maker_instance,
)

__all__ = [
    'VectorMakerOpts',
    'VectorMaker',
    'add_vec_opts',
    'get_sentence_chunks_from_doc',
    'get_sentence_chunks',
    'get_sentence_vectors',
    'vectorize',
    'vec_func',
    'get_vector_maker_instance',
]


def add_vec_opts(parser):
    o = VectorMakerOpts()
    parser_group = parser.add_argument_group('vector')
    parser_group.add_argument(
        "--model_language",
        "-l",
        type=str,
        default=o.model_language,
        help="model language (for LASER)",
    )
    parser_group.add_argument(
        "--sbert_model_name", "-sm", type=str, default=o.sbert_model_name, help="sbert model name"
    )
