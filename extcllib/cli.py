#!/usr/bin/env python3
# coding: utf-8

import argparse
import copy
import logging

import yaml
import os.path as fs
import json

from extcllib.common_utils import ArgsFormatter, TimeProfiler
from extcllib.multiprocessing import MultiProcessor
from extcllib.dataset_handling import (
    add_common_opts,
    add_dataset_opts,
    get_dataset_handler_instance,
    ExTclStep,
)
from extcllib.text_extraction import add_extract_text_opts, extract_text_func
from extcllib.linguistics import add_ling_opts, ling_func
from extcllib.statistics import (
    add_stat_builder_opts,
    calc_stat_func,
    add_stat_uploader_opts,
    upload_stat_func,
    add_get_stat_opts,
    get_stat_cli,
)
from extcllib.classification import (
    add_classificator_opts,
    add_baseline_classificator_opts,
    classificator_part_func,
    add_vec_classificator_opts,
    add_tuner_opts,
    tuner_cli,
)
from extcllib.vectorization import add_vec_opts, vec_func
from extcllib.vector_indexing import add_index_opts, make_index_func


def extract_text_cli(opts):
    d_h = get_dataset_handler_instance(opts)
    input_queue_items_generator = d_h.steps_paths(
        step=ExTclStep.TEXT_EXTRACT,
        part=opts.parts[0] if hasattr(opts, 'parts') and len(opts.parts) == 1 else 'all',
    )
    MultiProcessor.process_step(
        opts=opts,
        func=extract_text_func,
        input_queue_items_generator=input_queue_items_generator,
        step_name='extract text',
    )


def ling_cli(opts):
    d_h = get_dataset_handler_instance(opts)
    input_queue_items_generator = d_h.steps_paths(
        step=ExTclStep.LING_ANALYSIS,
        part=opts.parts[0] if hasattr(opts, 'parts') and len(opts.parts) == 1 else 'all',
    )
    MultiProcessor.process_step(
        opts=opts,
        func=ling_func,
        input_queue_items_generator=input_queue_items_generator,
        step_name='ling',
    )


def calc_stat_cli(opts):
    d_h = get_dataset_handler_instance(opts)
    input_queue_items_generator = d_h.steps_paths(
        step=ExTclStep.CALC_STAT,
        part=opts.parts[0] if hasattr(opts, 'parts') and len(opts.parts) == 1 else 'all',
    )
    MultiProcessor.process_step(
        opts=opts,
        func=calc_stat_func,
        input_queue_items_generator=input_queue_items_generator,
        step_name='calc_stat',
    )


def upload_stat_cli(opts):
    d_h = get_dataset_handler_instance(opts)
    input_queue_items_generator = d_h.steps_paths(
        step=ExTclStep.UPLOAD_STAT,
        part='train',
    )
    MultiProcessor.process_step(
        opts=opts,
        func=upload_stat_func,
        input_queue_items_generator=input_queue_items_generator,
        step_name='upload_stat',
    )


def classificator_cli(opts):
    d_h = get_dataset_handler_instance(opts)

    if hasattr(opts, 'index_db_path'):
        opts.index_db_path = d_h.get_filepath_in_workdir(opts.index_db_path, ExTclStep.MAKE_INDEX)
    if hasattr(opts, 'index_path'):
        opts.index_path = d_h.get_filepath_in_workdir(opts.index_path, ExTclStep.MAKE_INDEX)

    opts_ = copy.deepcopy(opts)

    best_params_config_filename = d_h.get_best_params_config_filename()
    if fs.exists(best_params_config_filename):
        with open(best_params_config_filename, 'r') as f:
            best_params = json.load(f)
            opts_.__dict__.update(best_params)
            logging.info(f"Found tuned params {best_params_config_filename}. Try to use them")

    results = [
        (part, classificator_part_func(opts_, d_h, part))
        for part in {'train', 'dev', 'test'} & set(opts_.parts)
    ]

    if opts.document_path is None or not opts.document_path:
        logging.info("RESULT METRICS:")
        for part, (correct_cnt, docs_cnt, metrics, _) in results:
            logging.info(f"[{part}] Correct {correct_cnt} / {docs_cnt}. Metrics: {metrics}")
        return
    else:
        assert len(results) == 1
        assert len(results[0]) == 2
        predicted_labels = results[0][1][3]
        return predicted_labels


def vectorize_cli(opts):
    d_h = get_dataset_handler_instance(opts)
    input_queue_items_generator = d_h.steps_paths(
        step=ExTclStep.VECTORIZE,
        part='all',
    )
    MultiProcessor.process_step(
        opts=opts,
        func=vec_func,
        input_queue_items_generator=input_queue_items_generator,
        step_name='vector',
    )


def indexer_cli(opts):
    opts.proc_cnt = 1
    d_h = get_dataset_handler_instance(opts)
    input_queue_items_generator = d_h.steps_paths(
        step=ExTclStep.MAKE_INDEX,
        part='train',
    )
    opts.index_db_path = d_h.get_filepath_in_workdir(opts.index_db_path, ExTclStep.MAKE_INDEX)
    opts.index_path = d_h.get_filepath_in_workdir(opts.index_path, ExTclStep.MAKE_INDEX)
    MultiProcessor.process_step(
        opts=opts,
        func=make_index_func,
        input_queue_items_generator=input_queue_items_generator,
        step_name='index',
    )


def add_cli_opts(parser):
    add_common_opts(parser)
    add_dataset_opts(parser)
    subparsers = parser.add_subparsers(help="sub-command help")

    text_extractor_parser = subparsers.add_parser(
        'extract_text',
        help="extract text",
        formatter_class=ArgsFormatter,
    )
    text_extractor_parser.add_argument(
        "--document_path",
        "-d",
        type=str,
        required=False,
        help="path to document which need to be processed",
    )
    add_extract_text_opts(text_extractor_parser)
    text_extractor_parser.set_defaults(func=extract_text_cli)

    ling_parser = subparsers.add_parser(
        'ling',
        help="linguistic analysis",
        formatter_class=ArgsFormatter,
    )
    ling_parser.add_argument(
        "--document_path",
        "-d",
        type=str,
        required=False,
        help="path to document which need to be processed",
    )
    add_ling_opts(ling_parser)
    ling_parser.set_defaults(func=ling_cli)

    vector_parser = subparsers.add_parser(
        'vector',
        help="vectorization",
        formatter_class=ArgsFormatter,
    )
    vector_parser.add_argument(
        "--document_path",
        "-d",
        type=str,
        required=False,
        help="path to document which need to be processed",
    )
    add_index_opts(vector_parser)
    add_vec_opts(vector_parser)
    vector_parser.set_defaults(func=vectorize_cli)

    index_parser = subparsers.add_parser(
        'index',
        help="make_index",
        formatter_class=ArgsFormatter,
    )
    index_parser.add_argument(
        "--document_path",
        "-d",
        type=str,
        required=False,
        help="path to document which need to be processed",
    )
    add_index_opts(index_parser)
    add_vec_opts(index_parser)
    index_parser.set_defaults(func=indexer_cli)

    stat_builder_parser = subparsers.add_parser(
        'calc_stat',
        help="calculate stat",
        formatter_class=ArgsFormatter,
    )
    stat_builder_parser.add_argument(
        "--document_path",
        "-d",
        type=str,
        required=False,
        help="path to document which need to be processed",
    )
    add_stat_builder_opts(stat_builder_parser)
    stat_builder_parser.set_defaults(func=calc_stat_cli)

    stat_uploader_parser = subparsers.add_parser(
        'upload_stat',
        help="upload stat",
        formatter_class=ArgsFormatter,
    )
    stat_uploader_parser.add_argument(
        "--document_path",
        "-d",
        type=str,
        required=False,
        help="path to document which need to be processed",
    )
    add_stat_uploader_opts(stat_uploader_parser)
    stat_uploader_parser.set_defaults(func=upload_stat_cli)

    stat_get_parser = subparsers.add_parser(
        'get_stat',
        help="get stat (for debugging, use -v to print)",
        formatter_class=ArgsFormatter,
    )
    add_get_stat_opts(stat_get_parser)
    stat_get_parser.set_defaults(func=get_stat_cli)

    tuning_parser = subparsers.add_parser(
        'tune',
        help="tune params",
        formatter_class=ArgsFormatter,
    )
    add_extract_text_opts(tuning_parser)
    add_ling_opts(tuning_parser)
    add_stat_builder_opts(tuning_parser)
    add_stat_uploader_opts(tuning_parser)
    add_index_opts(tuning_parser)
    add_vec_opts(tuning_parser)
    add_tuner_opts(tuning_parser)
    add_classificator_opts(tuning_parser)
    add_baseline_classificator_opts(tuning_parser)
    add_vec_classificator_opts(tuning_parser)
    tuning_parser.set_defaults(func=tuner_cli)

    classificator_parser = subparsers.add_parser(
        'classify',
        help="assign class (label)",
        formatter_class=ArgsFormatter,
    )
    classificator_parser.add_argument(
        "--document_path",
        "-d",
        type=str,
        required=False,
        help="path to txt document which need to be processed",
    )
    add_extract_text_opts(classificator_parser)
    add_ling_opts(classificator_parser)
    add_stat_builder_opts(classificator_parser)
    add_stat_uploader_opts(classificator_parser)
    add_index_opts(classificator_parser)
    add_vec_opts(classificator_parser)
    add_classificator_opts(classificator_parser)
    add_baseline_classificator_opts(classificator_parser)
    add_vec_classificator_opts(classificator_parser)
    classificator_parser.set_defaults(func=classificator_cli)


def load_args_from_config(config: str, opts):
    with open(config) as f:
        cfg = yaml.load(f, Loader=yaml.FullLoader)

    for part in cfg.keys():
        part_opts = cfg.get(part)
        opts.__dict__.update(part_opts)


def main():
    parser = argparse.ArgumentParser(
        prog="ExTCL",
        description="""Explainable Text Classification Library.""",
        formatter_class=ArgsFormatter,
    )
    parser.add_argument("--config", "-c", type=str, required=False, help="config file (yaml)")

    add_cli_opts(parser)
    args = parser.parse_args()

    logging.debug(f"cli args: {args}")

    if args.config is not None:
        logging.info(
            f"Loading args from config {args.config}. Arguments will be replaced from file."
        )
        load_args_from_config(args.config, args)
        logging.debug(f"final args: {args}")

    if not hasattr(args, 'func'):
        parser.print_help()
        return

    try:
        with TimeProfiler("whole process") as p:
            args.func(args)
    except Exception as e:
        logging.exception("failed to run: %s ", e)


if __name__ == "__main__":
    main()
