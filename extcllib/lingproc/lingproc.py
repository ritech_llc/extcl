import spacy
from .spacy_model import SpacyModelSelector
from nltk import Tree


class LingProc:
    def __init__(self, lang='en'):
        model = SpacyModelSelector.get(lang,SpacyModelSelector.get("en",""))
        assert(len(model)>0)
        self.nlp = spacy.load(model)
        self.nlp.add_pipe('sentencizer')

    def buildTree(self, node):
        if node.n_lefts + node.n_rights > 0:
            return Tree(node.orth_, [self.buildTree(child) for child in node.children])
        else:
            return node.orth_
    def getWords(self, doc):
        ngrams = []
        for token in doc:
            if token.pos_ == 'NOUN':
                ngrams.append(token.lemma_)
            for c in token.children:
                if c.pos_ == 'ADJ':
                    ngrams.append(c.lemma_ + " " + token.lemma_)
            if token.pos_ == 'VERB':
                ngrams.append(token.lemma_)
        return ngrams
    def process(self, text):
        doc = self.nlp(text)

        return {
                'words': self.getWords(doc), 
                'trees': [self.buildTree(sent.root) for sent in doc.sents]
        }