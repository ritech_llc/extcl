#!/usr/bin/env python3
# coding: utf-8

import csv
from dataclasses import dataclass
import logging
from typing import Tuple

from extcllib.dataset_handling import (
    DatasetOpts,
    DatasetItemType,
    DatasetLabelType,
    DataSetReaderOpts,
    DataSetReader,
    DataSetTrainDevTest,
    SplitStrategyOptsType,
    TrainDevTestSplitStrategyOpts,
    TrainDevTestSplitStrategy,
    read_parts_items,
    extract_doc_id_from_item,
)


@dataclass
class FromCsvDataSetOpts(DataSetReaderOpts):
    path: str = "dataset.csv"
    delimiter: str = ";"
    labels_delimiter: str = ','
    use_first_symbol_as_label: bool = False


ItemsFromCsvReaderOpts = FromCsvDataSetOpts


class ItemsFromCsvReader(DataSetReader):
    opts: ItemsFromCsvReaderOpts

    def __init__(self, opts: ItemsFromCsvReaderOpts):
        super().__init__(opts)
        self.opts = opts

    def get_items_list(self) -> Tuple[Tuple[DatasetItemType, ...], Tuple[DatasetLabelType, ...]]:
        x_items = list()
        y_items = list()
        path = self.opts.path
        with open(path, 'r', newline='') as f:
            csv_reader = csv.reader(f, delimiter=self.opts.delimiter)
            for row in csv_reader:
                try:
                    doc_id, txt_filename, meta_filename, labels = row
                except ValueError as v_err:
                    try:
                        doc_id, txt_filename, meta_filename, *labels = row
                        if not labels:
                            raise ValueError("")
                    except ValueError as v_err:
                        logging.error(f"Failed to parse row: {row}. Skip!")
                        continue

                if isinstance(labels, (str,)):
                    labels = labels.strip().split(self.opts.labels_delimiter)
                elif isinstance(labels, (list,)):
                    labels = [l.strip() for l in labels]
                else:
                    logging.error(f"Failed to parse row: {row}. Incorrect labels: {labels}. Skip!")
                    continue

                if self.opts.use_first_symbol_as_label:
                    labels = sorted(set([l[0] for l in labels]))

                x_item: DatasetItemType
                x_item = (doc_id, txt_filename, meta_filename)
                x_items.append(x_item)

                y_item: DatasetLabelType
                y_item = tuple(labels)
                y_items.append(y_item)

        return tuple(x_items), tuple(y_items)


class FromCsvDataSetReader(DataSetTrainDevTest):
    """
    Expected like csv-file with ";" separator and following columns (two variants):
        `id; txt_path; meta_path; label1, label2`
        OR
        `id; txt_path; meta_path; label1; label2`

    This class uses train-dev-test split strategy.
    """

    def __init__(self, opts: DatasetOpts, split_strategy_opts: SplitStrategyOptsType):
        if hasattr(opts, 'path'):
            input_path = opts.path
        elif hasattr(opts, 'input'):
            input_path = opts.input
        else:
            raise RuntimeError("expecting path or input param in opts")

        opts_ = FromCsvDataSetOpts(
            path=input_path, use_first_symbol_as_label=opts.use_first_symbol_as_label
        )
        items_reader_opts_ = ItemsFromCsvReaderOpts(
            path=input_path, use_first_symbol_as_label=opts.use_first_symbol_as_label
        )

        assert isinstance(split_strategy_opts, (TrainDevTestSplitStrategyOpts,))

        super().__init__(
            opts=opts_,
            reader=ItemsFromCsvReader(items_reader_opts_),
            splitter=TrainDevTestSplitStrategy(
                split_strategy_opts,
                parts_items_reader=read_parts_items,
                doc_id_extractor_from_item=extract_doc_id_from_item,
            ),
        )

    def extract_item_path(self, x: DatasetItemType) -> str:
        return x[1]
