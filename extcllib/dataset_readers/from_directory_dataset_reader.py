#!/usr/bin/env python3
# coding: utf-8

import os.path as fs
from typing import List, Tuple

from extcllib.dataset_handling import (
    DatasetOpts,
    DatasetItemType,
    DatasetLabelType,
    DataSetReaderOpts,
    DataSetReader,
    DataSetTrainDevTest,
    SplitStrategyOptsType,
    TrainDevTestSplitStrategyOpts,
    TrainDevTestSplitStrategy,
    read_parts_items,
    extract_doc_id_from_item,
)


class FromDirectoryDataSetOpts(DataSetReaderOpts):
    path: str = "dataset.d"


ItemsFromDirectoryReaderOpts = FromDirectoryDataSetOpts


class ItemsFromDirectoryReader(DataSetReader):
    def __init__(self, opts: FromDirectoryDataSetOpts = FromDirectoryDataSetOpts()):
        super().__init__(opts)

    def get_items_list(self) -> Tuple[Tuple[DatasetItemType], Tuple[DatasetLabelType]]:
        raise RuntimeError("This method does not implemented yet!")


class FromDirectoryDataSetReader(DataSetTrainDevTest):
    def __init__(self, opts: DatasetOpts, split_strategy_opts: SplitStrategyOptsType):
        if hasattr(opts, 'path'):
            input_path = opts.path
        elif hasattr(opts, 'input'):
            input_path = opts.input
        else:
            raise RuntimeError("expecting path or input param in opts")

        opts_ = FromDirectoryDataSetOpts(path=input_path)
        items_reader_opts_ = ItemsFromDirectoryReaderOpts(path=input_path)

        assert isinstance(split_strategy_opts, (TrainDevTestSplitStrategyOpts,))

        super().__init__(
            opts=opts_,
            reader=ItemsFromDirectoryReader(items_reader_opts_),
            splitter=TrainDevTestSplitStrategy(
                split_strategy_opts,
                parts_items_reader=read_parts_items,
                doc_id_extractor_from_item=extract_doc_id_from_item,
            ),
        )
