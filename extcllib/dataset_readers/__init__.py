#!/usr/bin/env python3
# coding: utf-8

from .textapp_dump_dataset_reader import FromCsvDataSetReader as TextAppDumpFromCsvDataSetReader
from .from_directory_dataset_reader import FromDirectoryDataSetReader
from .one_file_dataset_reader import OneFileDatasetReader

from typing import TypeAlias, Type, Union

DataSetReaderClasses: TypeAlias = Union[
    Type[TextAppDumpFromCsvDataSetReader],
    Type[FromDirectoryDataSetReader],
    Type[OneFileDatasetReader],
]

__all__ = [
    'DataSetReaderClasses',
    'TextAppDumpFromCsvDataSetReader',
    'FromDirectoryDataSetReader',
    'OneFileDatasetReader',
]
