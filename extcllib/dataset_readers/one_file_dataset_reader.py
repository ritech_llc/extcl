#!/usr/bin/env python3
# coding: utf-8

from dataclasses import dataclass
import os.path as fs
from typing import List, Tuple

from extcllib.dataset_handling import (
    DatasetOpts,
    DatasetItemType,
    DatasetLabelType,
    DataSetReaderOpts,
    DataSetReader,
    DataSetTrainTest,
    SplitStrategyOptsType,
    SplitStrategy,
    TrainTestSplitStrategyOpts,
    UndefinedLabelName,
)


@dataclass
class OneFileDatasetReaderOpts(DataSetReaderOpts):
    path: str = ''


class FakeDatasetReader(DataSetReader):
    def __init__(self, opts):
        super().__init__(opts)
        self.opts = opts

    def get_items_list(self) -> Tuple[Tuple[DatasetItemType, ...], Tuple[DatasetLabelType, ...]]:
        filename = self.opts.path
        assert fs.isfile(fs.abspath(filename))
        x_items = tuple([filename])
        y_items = tuple([(UndefinedLabelName,)])

        return x_items, y_items


class OneFileDatasetFakeSplitStrategy(SplitStrategy):
    def __init__(
        self,
        opts: SplitStrategyOptsType = TrainTestSplitStrategyOpts(),
    ):
        super().__init__(opts)

    def split(self, x: Tuple, y: Tuple) -> Tuple[List[DatasetItemType | DatasetItemType], ...]:
        return [0], [0], [0], [0]


class OneFileDatasetReader(DataSetTrainTest):
    """
    Expected one file as input
    """

    def __init__(self, opts: DatasetOpts, split_strategy_opts: SplitStrategyOptsType):
        if hasattr(opts, 'path'):
            input_path = opts.path
        elif hasattr(opts, 'input'):
            input_path = opts.input
        else:
            raise RuntimeError("expecting path or input param in opts")

        opts_ = OneFileDatasetReaderOpts(input_path)

        super().__init__(
            opts=opts_,
            reader=FakeDatasetReader(opts_),
            splitter=OneFileDatasetFakeSplitStrategy(
                split_strategy_opts,
            ),
        )
