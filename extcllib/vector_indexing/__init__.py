#!/usr/bin/env python3
# coding: utf-8

from .indexer import (
    IndexBaseOpts,
    IndexMakerOpts,
    IndexMaker,
    make_index_func,
)

__all__ = [
    'IndexBaseOpts',
    'IndexMakerOpts',
    'IndexMaker',
    'add_index_opts',
    'make_index_func',
]


def add_index_opts(parser):
    o = IndexMakerOpts()
    parser_group = parser.add_argument_group('index')
    parser_group.add_argument(
        "--index_db_path",
        "-idp",
        type=str,
        default=o.index_db_path,
        help="index database path",
    )
    parser_group.add_argument(
        "--index_path",
        "-ip",
        type=str,
        default=o.index_path,
        help="index path",
    )
