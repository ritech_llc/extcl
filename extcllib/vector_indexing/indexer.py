#!/usr/bin/env python3
# coding: utf-8

import argparse
import collections
from dataclasses import dataclass
import faiss
import logging
import multiprocessing as mp
import numpy as np
import os
import os.path as fs
import sqlite3
from typing import Counter

from extcllib.dataset_handling import DatasetLabelType
from extcllib.multiprocessing import MultiProcessor


@dataclass
class IndexBaseOpts:
    index_db_path: str = 'sqlite_vec.db'
    index_path: str = 'main.index'
    index_dim: int = 512
    model_name: str = "SentenceBert"


@dataclass
class IndexMakerOpts(IndexBaseOpts):
    pass


class IndexMaker:
    def __init__(self, opts: IndexMakerOpts):
        self.opts = opts
        if self.opts.model_name == "SentenceBert":
            self.opts.index_dim = 512
        elif self.opts.model_name == "LASER":
            self.opts.index_dim = 1024
        elif self.opts.model_name == "Word2Vec":
            self.opts.index_dim = 300

        os.makedirs(fs.dirname(self.opts.index_db_path), exist_ok=True)
        os.makedirs(fs.dirname(self.opts.index_path), exist_ok=True)
        self._init_db(self.opts.index_db_path)

    def _init_db(self, db_file):
        conn = sqlite3.connect(db_file)
        conn.execute(
            """CREATE TABLE IF NOT EXISTS vectors (
                        vec_id INTEGER PRIMARY KEY AUTOINCREMENT,
                        labels varchar(255)
                    )"""
        )
        conn.commit()
        conn.close()

    def _make_db_query(self, labels):
        return "INSERT INTO vectors (labels) values %s RETURNING vec_id" % ','.join(
            ["('%s')" % l for l in labels]
        )

    def _add_vectors_info_to_db(self, conn, labels):
        query = self._make_db_query(labels)
        vec_ids = [q[0] for q in conn.execute(query)]
        conn.commit()
        return vec_ids

    def _load_doc_vectors(self, vec_path, label: DatasetLabelType):
        vectors = np.load(vec_path, allow_pickle=True)
        labels = [label for i in range(len(vectors))]
        return vectors, labels

    def get_vectors_ids(self, vec_path: str, label: DatasetLabelType):
        vectors, labels = self._load_doc_vectors(vec_path, label)
        conn = sqlite3.connect(self.opts.index_db_path)
        vec_ids = self._add_vectors_info_to_db(conn, labels)
        conn.close()
        return vectors, vec_ids

    def make_index(self, vectors, vec_ids):
        faiss.normalize_L2(vectors)
        if fs.exists(self.opts.index_path):
            index = faiss.read_index(self.opts.index_path)
        else:
            index = faiss.index_factory(
                self.opts.index_dim, "IDMap,HNSW32", faiss.METRIC_INNER_PRODUCT
            )

        index.add_with_ids(vectors, vec_ids)
        faiss.write_index(index, self.opts.index_path)
        logging.info(f"Done with build index: {self.opts.index_path}")
        logging.info(f"db path: {self.opts.index_db_path}")


def get_vectors_ids(index_maker: IndexMaker, vec_path: str, label: DatasetLabelType, counters=None):
    if counters is None:
        counters = collections.Counter()

    assert fs.exists(vec_path)

    try:
        vectors, vec_ids = index_maker.get_vectors_ids(vec_path, label)
        counters["Indexed"] += 1
    except RuntimeError as r_err:
        logging.error(f"Failed to get vectors: {vec_path}: {str(r_err)}")
        counters["fail_get_vectors"] += 1
        return

    counters["success"] += 1
    logging.info(f"Get vec ids for {vec_path}")
    return vectors, vec_ids


def make_index_func(
    opts: argparse.Namespace,
    proc_name: str,
    input_queue: mp.Queue,
    output_queue: mp.Queue,
):
    counters: Counter
    counters = collections.Counter()
    vec_array, vec_ids_array = [], []
    index_maker = IndexMaker(
        IndexMakerOpts(
            index_db_path=opts.index_db_path, index_path=opts.index_path, model_name=opts.model_name
        )
    )
    for item in MultiProcessor.iter_queue(input_queue):
        counters['docs_cnt'] += 1
        try:
            label: DatasetLabelType
            (vec_path,), label = item
            vectors, vec_ids = get_vectors_ids(index_maker, vec_path, label, counters)
            vec_array.append(vectors)
            vec_ids_array += vec_ids
        except Exception as e:
            counters["fail_get_vectors_ids"] += 1
            logging.exception(f"Failed to get vec ids for item {item}: {e}")

    vec_array = np.concatenate(vec_array)
    index_maker.make_index(vec_array, vec_ids_array)

    res = {
        'proc_name': str(proc_name),
    }
    res.update(counters)
    MultiProcessor.put_to_output_queue(output_queue, res)
