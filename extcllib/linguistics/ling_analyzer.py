#!/usr/bin/env python3
# coding: utf-8

import argparse
import collections
from dataclasses import dataclass
import logging
import multiprocessing as mp
import os
import os.path as fs
from typing import Counter

from extcllib.multiprocessing import MultiProcessor

from ufal.udpipe import Model, Pipeline, ProcessingError


@dataclass
class UDPipeLingAnalyzerOpts:
    model_file: str
    input_format: str = "tokenize"  # conllu, horizontal, vertical
    output_format: str = "conllu"


class UDPipeLingAnalyzer(object):
    def __init__(self, opts: UDPipeLingAnalyzerOpts):
        self.opts = opts
        self.model = Model.load(self.opts.model_file)
        if not self.model:
            raise RuntimeError(f"Cannot load model from file {self.opts.model_file}.")

        self.pipeline = Pipeline(
            self.model,
            self.opts.input_format,
            Pipeline.DEFAULT,
            Pipeline.DEFAULT,
            self.opts.output_format,
        )

    def analyze(self, text: str):
        error = ProcessingError()
        processed = self.pipeline.process(text, error)
        if error.occurred():
            raise RuntimeError(f"An error occurred when running udpipe: {error.message}")
        return processed


def ling_analyze(ling_analyzer: UDPipeLingAnalyzer, txt_path: str, ling_path: str, counters=None):
    if counters is None:
        counters = collections.Counter()

    if fs.exists(ling_path):
        logging.info(f"Skip. {ling_path} already exists.")
        counters["skip"] += 1
        return

    assert fs.exists(txt_path)
    try:
        with open(txt_path, 'r') as f:
            text = f.read()

        l_a_result = ling_analyzer.analyze(text)
    except RuntimeError as r_err:
        logging.error(f"Failed to analyze: {txt_path}: {str(r_err)}")
        counters["fail_to_analyze"] += 1
        return

    os.makedirs(fs.dirname(ling_path), exist_ok=True)
    with open(ling_path, 'w') as f:
        f.write(l_a_result)

    counters["ling_success"] += 1
    logging.info(f"Analyzed {txt_path} -> {ling_path}")
    return l_a_result


def ling_func(
    opts: argparse.Namespace, proc_name: str, input_queue: mp.Queue, output_queue: mp.Queue
):
    ling_analyzer = UDPipeLingAnalyzer(
        UDPipeLingAnalyzerOpts(
            model_file=opts.model_file,
            input_format=opts.input_format,
            output_format=opts.output_format,
        )
    )
    if ling_analyzer:
        logging.info(f"Loaded model: {opts.model_file}")
    else:
        raise RuntimeError(f"Failed to load model: {opts.model_file}")

    counters: Counter
    counters = collections.Counter()

    for item in MultiProcessor.iter_queue(input_queue):
        counters['docs_cnt'] += 1
        try:
            (txt_path, ling_path), _ = item
            assert fs.exists(txt_path)
            ling_analyze(ling_analyzer, txt_path, ling_path, counters)
        except Exception as e:
            counters["fail_to_ling_for_item"] += 1
            logging.exception(f"Failed to ling for item {item}: {e}")

    res = {
        'proc_name': str(proc_name),
    }
    res.update(counters)

    MultiProcessor.put_to_output_queue(output_queue, res)
