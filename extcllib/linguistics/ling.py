#!/usr/bin/env python3
# coding: utf-8

import collections
from dataclasses import dataclass
import logging
import json
from typing import Dict, List, Counter, Tuple, TypeAlias, Set

import pylp.lp_doc
import pylp.lemmas.lemmatizer as lplemma
from pylp.common import STOP_WORD_POS_TAGS
from pylp.common import POS_TAG_DICT
from pylp.phrases.util import add_phrases_to_doc

from pylp.converter_conll_ud_v1 import ConverterConllUDV1
from pylp.lp_doc import Doc as LingProcDoc


from extcllib.dataset_handling import DatasetLabelType


@dataclass
class Chunk:
    offset: int
    length: int


@dataclass
class EntityChunk:
    chunks: List[Chunk]


EntityChunksType: TypeAlias = List[List[Chunk]]


@dataclass
class EntityStat:
    word_id: str
    lemma: str
    label: DatasetLabelType
    count: int
    pos: str

    def __init__(
        self,
        word_id: str | int,
        lemma: str,
        label: DatasetLabelType,
        count: int | str,
        pos: str,
    ):
        self.word_id = str(word_id)
        self.lemma = lemma
        self.label = label
        self.count = int(count)
        self.pos = pos

    def __hash__(self):
        return int(self.get_id())

    def __eq__(self, other):
        return self.get_id() == other.get_id()

    def get_id(self) -> str:
        return self.word_id

    def to_dict(self) -> Dict:
        return {
            "id": self.word_id,
            "lemma": self.lemma,
            "label": list(self.label),
            "count": self.count,
        }

    def to_row(self) -> List:
        return [
            self.word_id,
            self.lemma,
            self.count,
            json.dumps(list(self.label)),
        ]

    @staticmethod
    def from_row(row: Tuple | List):
        word_id, lemma, count, label_s = row
        try:
            label: Tuple[str, ...]
            label = tuple(str(l) for l in json.loads(label_s))
        except Exception as err:
            raise RuntimeError(f"Failed to load label from row: {row}")
        return EntityStat(word_id, lemma, label, count, '')


@dataclass
class EntityStatChunks(EntityStat):
    chunks: EntityChunksType

    def __init__(
        self,
        word_id: str | int,
        lemma: str,
        label: DatasetLabelType,
        count: int | str,
        pos: str,
        chunks: EntityChunksType,
    ):
        super().__init__(word_id, lemma, label, count, pos)
        self.chunks = chunks

    def __hash__(self):
        return int(self.get_id())

    def __eq__(self, other):
        return self.get_id() == other.get_id()


def init_doc(doc_id: str | int, text: str, conll_raw_text: str) -> pylp.lp_doc.Doc:
    doc = LingProcDoc(str(doc_id))
    converter = ConverterConllUDV1()
    return converter(text=text, conll_raw_text=conll_raw_text, doc=doc)


try:
    lp_lemmatizer = lplemma.Lemmatizer()
except Exception as e:
    lp_lemmatizer = None


class NoLemmatizationException(Exception):
    pass


def is_lemma_ok(w):
    return not (w.lemma is None or w.lemma == '')


def calculate_stat_from_doc(
    opts, doc: pylp.lp_doc.Doc, label: DatasetLabelType
) -> Tuple[List[EntityStatChunks], Dict[int, Set[str]]]:
    if opts.allowed_word_pos_tags:
        allowed_word_pos_tags = {
            POS_TAG_DICT[pos_tag.upper()] for pos_tag in opts.allowed_word_pos_tags
        }
    else:
        allowed_word_pos_tags = None

    if opts.phrases_max_len >= 2:
        add_phrases_to_doc(doc, opts.phrases_max_len)
        logging.debug(f"Added phrases for {doc.doc_id}")

    def _get_first_word():
        first_w = None
        for s in doc.sents():
            for w in s.words():
                first_w = w
                break
            break
        return first_w

    use_pylp_lemmatization = False
    if opts.use_pylp_lemmatization is None:
        first_w = _get_first_word()
        if first_w and not is_lemma_ok(first_w):
            logging.warning(
                f"Seems like need lemmatization for {doc.doc_id}. First word lemma: {first_w.lemma}, form: {first_w.form}"
            )
            use_pylp_lemmatization = True

    elif opts.use_pylp_lemmatization:
        use_pylp_lemmatization = True

    if use_pylp_lemmatization:
        if lp_lemmatizer:
            try:
                lp_lemmatizer(doc)
                logging.info(f"Applied lp lemmatizer for {doc.doc_id}")
            except Exception as e:
                raise NoLemmatizationException(str(e))
        else:
            raise NoLemmatizationException("No way to use lp_lemmatizer")

    if not is_lemma_ok(_get_first_word()):
        raise NoLemmatizationException("Faced word without lemma!")

    counter: Counter = collections.Counter()
    entity_words = dict()
    entity_chunks_spans = collections.defaultdict(list)
    sentence_id_to_chunks = collections.defaultdict(set)
    for s_i, s in enumerate(doc.sents()):
        for w_i, w in enumerate(s.words()):
            if w.pos_tag in STOP_WORD_POS_TAGS:
                continue

            if allowed_word_pos_tags and w.pos_tag.name not in allowed_word_pos_tags:
                continue

            entity_id = str(w.word_id)
            counter[entity_id] += 1
            entity_words[entity_id] = (str(w.lemma).lower(), w.pos_tag.name)
            entity_chunks_spans[entity_id].append([Chunk(w.offset, w.len)])
            sentence_id_to_chunks[s_i].add(entity_id)

        for p_i, p in enumerate(s.phrases(with_mwe=opts.phrases_with_mwe)):
            entity_id = str(p.get_id())
            counter[entity_id] += 1
            p_lemma = p.get_str_repr()
            entity_words[entity_id] = (str(p_lemma).lower(), '')
            entity_chunks_spans[entity_id].append(
                [Chunk(s[w_i].offset, s[w_i].len) for w_i in p.get_sent_pos_list()]
            )
            sentence_id_to_chunks[s_i].add(entity_id)

    stat_result = [
        EntityStatChunks(
            ent_id,
            entity_words[ent_id][0],
            label,
            count,
            pos=entity_words[ent_id][1],
            chunks=entity_chunks_spans[ent_id],
        )
        for ent_id, count in counter.most_common()
    ]

    return stat_result, sentence_id_to_chunks
