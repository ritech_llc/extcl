#!/usr/bin/env python3
# coding: utf-8

from .ling_analyzer import (
    UDPipeLingAnalyzerOpts,
    UDPipeLingAnalyzer,
    ling_func,
    ling_analyze,
)
from .ling import (
    Chunk,
    EntityChunk,
    EntityChunksType,
    EntityStatChunks,
    EntityStat,
    init_doc,
    calculate_stat_from_doc,
)

__all__ = [
    'add_ling_opts',
    'ling_func',
    'ling_analyze',
    'UDPipeLingAnalyzerOpts',
    'UDPipeLingAnalyzer',
    'Chunk',
    'EntityChunk',
    'EntityStatChunks',
    'EntityChunksType',
    'EntityStat',
    'init_doc',
    'calculate_stat_from_doc',
]


def add_ling_opts(parser):
    o = UDPipeLingAnalyzerOpts(model_file='udpipe.model')
    parser_group = parser.add_argument_group('ling')
    parser_group.add_argument(
        "--input_format", "-if", type=str, default=o.input_format, help="udpipe input format"
    )
    parser_group.add_argument(
        "--output_format", "-of", type=str, default=o.output_format, help="udpipe output format"
    )
    parser_group.add_argument(
        "--model_file", "-lm", type=str, default=o.model_file, help="udpipe model file"
    )
