#!/usr/bin/env python3
# coding: utf-8

from .text_extractor import (
    TextExtractorOpts,
    TextExtractor,
    extract_text_func,
    extract_text,
)

__all__ = [
    'TextExtractorOpts',
    'TextExtractor',
    'add_extract_text_opts',
    'extract_text_func',
    'extract_text',
]


def add_extract_text_opts(parser):
    o = TextExtractorOpts()
    parser_group = parser.add_argument_group('text_extractor')
    parser_group.add_argument(
        "--raw_already_plain_text",
        type=str,
        default=o.raw_already_plain_text,
        help="consider raw as already plaintext",
    )
