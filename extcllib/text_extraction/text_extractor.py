#!/usr/bin/env python3
# coding: utf-8

import argparse
import collections
from dataclasses import dataclass

import docxpy
import pypdfium2 as pdfium
from bs4 import BeautifulSoup

import logging
import multiprocessing as mp
import os
import os.path as fs
from typing import Counter, Dict

from extcllib.multiprocessing import MultiProcessor


@dataclass
class TextExtractorOpts:
    raw_already_plain_text: bool = False


class TextExtractor:
    def __init__(self, opts: TextExtractorOpts = TextExtractorOpts()):
        self.opts = opts

    def _get_extension(self, file_path: str) -> str:
        if file_path.lower().endswith('.pdf'):
            return 'pdf'
        elif file_path.lower().endswith('.docx'):
            return 'docx'
        elif file_path.lower().endswith('.txt'):
            return 'txt'
        elif file_path.lower().endswith('.html'):
            return 'html'
        else:
            raise Exception(f'Wrong format of file {file_path}')

    def _get_pdf_text(self, file_path: str) -> str:
        pdf = pdfium.PdfDocument(file_path)
        plain_text = ''
        for page in pdf:
            textpage = page.get_textpage()
            text_all = textpage.get_text_range()
            plain_text += text_all + '\n'
        return plain_text

    def _get_docx_text(self, file_path: str) -> str:
        plain_text = docxpy.process(file_path)
        return plain_text

    def _get_txt_text(self, file_path: str) -> str:
        with open(file_path, 'rb') as f:
            plain_text = f.read().decode(errors='replace')
        return plain_text

    def _get_html_text(self, file_path: str) -> str:
        with open(file_path, 'r') as f:
            html = f.read()
        soup = BeautifulSoup(html, features="html.parser")
        plain_text = soup.get_text()
        return plain_text

    def get_text(self, document: Dict | str) -> str:
        if isinstance(document, dict):
            return document['plaintext']

        if os.path.exists(document):
            file_path = document
        else:
            raise RuntimeError("file does not exist!")

        if self.opts.raw_already_plain_text:
            return self._get_txt_text(file_path)

        ext = self._get_extension(file_path)
        if ext == "pdf":
            return self._get_pdf_text(file_path)
        elif ext == "docx":
            return self._get_docx_text(file_path)
        elif ext == "txt":
            return self._get_txt_text(file_path)
        elif ext == "html":
            return self._get_html_text(file_path)
        else:
            raise RuntimeError(f"this files extension does not supported: {file_path}")


def extract_text(
    text_extractor: TextExtractor, raw_path: str, txt_path: str, counters=None
) -> None:
    if counters is None:
        counters = collections.Counter()

    if fs.exists(txt_path):
        logging.info(f"Skip. {txt_path} already exists.")
        counters["skip"] += 1
        return

    assert fs.exists(raw_path)

    try:
        text = text_extractor.get_text(raw_path)
        counters["extracted"] += 1
    except RuntimeError as r_err:
        logging.error(f"Failed to extract text: {raw_path}: {str(r_err)}")
        counters["fail_extract_text"] += 1
        return

    os.makedirs(fs.dirname(txt_path), exist_ok=True)
    with open(txt_path, 'w', encoding='utf-8') as f:
        f.write(text)

    counters["success"] += 1
    logging.info(f"Extracted {raw_path} -> {txt_path}")


def extract_text_func(
    opts: argparse.Namespace,
    proc_name: str,
    input_queue: mp.Queue,
    output_queue: mp.Queue,
):
    counters: Counter
    counters = collections.Counter()

    text_extractor = TextExtractor(
        TextExtractorOpts(raw_already_plain_text=opts.raw_already_plain_text)
    )
    for item in MultiProcessor.iter_queue(input_queue):
        counters['docs_cnt'] += 1
        try:
            (raw_path, txt_path), _ = item
            extract_text(text_extractor, raw_path, txt_path, counters)
        except Exception as e:
            counters["fail_extract_text_for_item"] += 1
            logging.exception(f"Failed to extract text for item {item}: {e}")

    res = {
        'proc_name': str(proc_name),
    }
    res.update(counters)
    MultiProcessor.put_to_output_queue(output_queue, res)
