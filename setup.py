#!/usr/bin/env python3
# coding: utf-8

import os
from setuptools import setup, find_packages

setup(
    name='extcl',
    version=os.environ.get('version', '0.1'),
    description='Exactus Text Classification Library',
    author='LLC RITECH',
    author_email='info@ritech.ru',
    license='GNU General Public License v3.0 ',
    packages=find_packages(),
    install_requires=[
        'beautifulsoup4',
        'black',
        'docxpy',
        'gensim',
        'faiss-cpu',
        'jsonrpcclient',
        'numpy',
        'pandas',
        'psutil',
        'pyfakefs',
        'pypdfium2',
        'pytest',
        'PyYAML',
        'requests',
        'scikit-learn',
        'ufal.udpipe',
        'pybind11',
        'pydantic',
        'pymorphy2',
        'seaborn',
        'sentence-transformers',
        'laser_encoders',
        'tensorboardX',
        # 'fairseq @ git+https://github.com/One-sixth/fairseq.git',
        # 'pylp @ git+https://github.com/dvzubarev/pylp.git',
        # 'pyexbase @ git+https://github.com/dvzubarev/pyexbase.git',
    ],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'extcl = extcllib.cli:main',
        ],
    },
)
