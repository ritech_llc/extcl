#!/usr/bin/env bash

for d in extcllib tests documentation;
do
  black -t py310 -l 100 -S  $(find $d -name '*.py' -type f)
done ;

black -t py310 -l 100 -S setup.py

