# Configuration

## YML-конфигурация

### Для способа классификации на основе сущностей (слова и словосочетания + ХТЗ)

Далее в документации будет использоваться следующее краткое название данного способа классификации: `baseline`.  

Примеры конфигураций:
* [baseline для русского языка](../etc/config/extcl_config_baseline.yml)
* [baseline для английского языка](../etc/config/extcl_config_en_baseline.yml)

### Для способа классификации на основе поиска похожих предложений
Для формирования векторов предложений можно использовать одну из моделей: 
* `Word2Vec` 
* `Laser` (далее `laser`)
* `SentenceBert` (далее `sbert`)
 
Примеры конфигураций:
* [laser для английского языка](../etc/config/extcl_config_en_laser.yml)
* [laser для английского языка](../etc/config/extcl_config_laser.yml)
* [sbert для английского языка](../etc/config/extcl_config_en_sbert.yml)
* [sbert для английского языка](../etc/config/extcl_config_sbert.yml)

## Аргументы командной строки

Параметры можно передавать из командной строки. 
Далее для примера  приведены два равнозначных вызова команды classify:
1. Путём использования yml-конфигурации extcl_config.yml (содержимое приведено ниже)
```bash
extcl -c etc/config/extcl_config.yml classify
```
2. Путём использования агрументов командной строки
```bash
extcl -i data/ru_patents_dataset_shuf10K_test100.csv -w data/extcl_workdir_test100 -dr TextAppDumpFromCsvDataSetReader -p 4 --train_prop 0.8 --dev_prop 0.1 --test_prop 0.1 --random_state 38 classify --model_file ../data/russian-syntagrus-ud-2.5-191206.udpipe --classify_method baseline --parts dev test --top_l 1 --label_score_threshold 0.0005 --predict_score_aggregator sum --explanation_strategy auto --save_highlighted_html True --top_k 50 --entity_total_docs_count_min_threshold 3 --entity_total_count_min_threshold 5 --entity_label_max_count_threshold 4 --entity_labels_topk 4 --entity_score_min_threshold 0.001 --entity_score_max_threshold 1.0 --predict_score_type "x-tfidf"
```

Содержимое файла `etc/config/extcl_config.yml` :
```yaml
extcl_common:
  dataset_reader: TextAppDumpFromCsvDataSetReader
  input: data/ru_patents_dataset_shuf10K_test100.csv
  work_dir: data/extcl_workdir_test100
  proc_cnt: 4
  train_prop: 0.8
  dev_prop: 0.1
  test_prop: 0.1
  random_state: 38

extcl_wordsdb:
  endpoint: http://localhost:8550/jsonrpc

extcl_ling:
  input_format: tokenize
  output_format: conllu
  model_file: ../data/russian-syntagrus-ud-2.5-191206.udpipe

extcl_classifier:
  classify_method: baseline
  parts: ['dev', 'test']
  top_l: 1
  label_score_threshold: 0.0005
  predict_score_aggregator: sum
  explanation_strategy: auto
  save_highlighted_html: true

extcl_classifier_baseline:
  top_k: 50
  entity_total_docs_count_min_threshold: 3
  entity_total_count_min_threshold: 5
  entity_label_max_count_threshold: 4
  entity_labels_topk: 4 # 2
  entity_score_min_threshold: 0.001
  entity_score_max_threshold: 1.0 # 0.55
  predict_score_type: x-tfidf

extcl_classifier_tune: 
  tune_part: dev
  target_metrics: ["micro_f1", "accuracy"]
  max_evals: 10000
  top_l_vars: [1]
  top_k_vars: [50] # [10,50,100]
  label_score_threshold_vars: [0.0005] # [0.0005, 0.001]
  entity_score_min_threshold_vars: [0.001] # [0.005, 0.001, 0.0005]
  entity_total_docs_count_min_threshold_vars: [3] # [3, 5, 10]
  entity_total_count_min_threshold_vars: [5]
  entity_label_max_count_threshold_vars: [4] # [4, 5]
```
