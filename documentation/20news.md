# Conduct experiments on 20 news groups dataset

## Download and prepare data

```bash
mkdir -p data/20news 
cd data/20news
bash ../../utils/download_and_prepare_20news.csv
cd ../../
```

## Run steps

```bash
extcl -c etc/config/extcl_config_20news.yml extract_text
```
```bash
extcl -c etc/config/extcl_config_20news.yml ling
```
```bash
extcl -c etc/config/extcl_config_20news.yml calc_stat
```
```bash
extcl -c etc/config/extcl_config_20news.yml upload_stat
```
```bash
extcl -c etc/config/extcl_config_20news.yml tune
```

```bash
extcl -c etc/config/extcl_config_20news.yml classify
```

See results. Find following lines in stdout:
```
[dev] Correct \d+ / 100 
[test] Correct \d+ / 7431
```
where `\d+` is the number of correct predicted documents in `dev` and `test` parts.

Depending on the dev part of the dataset, you should get something like the following result:

* DEV: `[dev] Correct 100 / 100. Metrics: {'micro_accuracy': 1.0, 'micro_precision': 1.0, 'micro_recall': 1.0, 'micro_f1': 1.0 ...`
* TEST:  `[test] Correct 6062 / 7431. Metrics: {'micro_accuracy': 0.981, 'micro_precision': 0.816, 'micro_recall': 0.805, 'micro_f1': 0.81 ...`

