# Подключение библиотеки
import argparse
from extcllib.cli import ArgsFormatter
from extcllib.cli import add_common_opts, add_dataset_opts
from extcllib.cli import add_extract_text_opts, extract_text_cli
from extcllib.cli import add_ling_opts, ling_cli
from extcllib.cli import add_stat_builder_opts, calc_stat_cli
from extcllib.cli import add_stat_uploader_opts, upload_stat_cli
from extcllib.cli import add_get_stat_opts, get_stat_cli

from extcllib.cli import (
    add_index_opts,
    add_vec_opts,
    add_tuner_opts,
    add_classificator_opts,
    add_baseline_classificator_opts,
    add_vec_classificator_opts,
    tuner_cli,
    classificator_cli,
)

# Настройки (задание параметов) и подготовка параметров при помощи argparse
dataset_reader = 'TextAppDumpFromCsvDataSetReader'
input = 'etc/sample/TextAppDumpFromCsvDataSetReader/sample.csv'
work_dir = '/tmp/sample_workdir'
model_file = '../data/russian-syntagrus-ud-2.5-191206.udpipe'


parser = argparse.ArgumentParser(
    prog="ExTCL",
    description="""Explainable Text Classification Library.""",
    formatter_class=ArgsFormatter,
)
parser.add_argument("--config", "-c", type=str, required=False, help="config file (yaml)")
add_common_opts(parser)
add_dataset_opts(parser)
add_extract_text_opts(parser)
add_ling_opts(parser)
add_stat_builder_opts(parser)
add_stat_uploader_opts(parser)
# add_get_stat_opts(parser)

add_index_opts(parser)
add_vec_opts(parser)
add_tuner_opts(parser)
add_classificator_opts(parser)
add_baseline_classificator_opts(parser)
add_vec_classificator_opts(parser)

args = parser.parse_args()
args.__dict__.update(
    {
        'dataset_reader': dataset_reader,
        'input': input,
        'work_dir': work_dir,
        'model_file': model_file,
        'get_documents_stat': False,
        'top_l_vars': [1],
        'label_score_threshold_vars': [0.001, 0.0005, 0.0001],
        'entity_total_docs_count_min_threshold_vars': [0, 1],
        'entity_total_count_min_threshold_vars': [0, 1],
        'save_highlighted_html': True,
        'document_path': None,
    }
)

# Run steps below:

# Extract text
extract_text_cli(args)
## Extract text output:
"""
...
... extract text whole result(ints): {'docs_cnt': 10, 'extracted': 10, 'success': 10}
"""

# Ling analysis
ling_cli(args)
## Ling analysis output
"""
...
... ling whole result(ints): {'docs_cnt': 10, 'ling_success': 10}
"""

# Calc stat
calc_stat_cli(args)
## Calc stat output
"""
...
...  calc_stat whole result(ints): {'built_stat': 10, 'stat_success': 10}
"""

# Upload stat  (run once!)
## Требуется запущенный инстанс [WordsDb](https://gitlab.com/ritech_llc/wordsdb).
upload_stat_cli(args)
## Upload stat output
"""
...
...  upload_stat whole result(ints): {'uploaded': 8}
"""

# Get stat (check)
get_stat_cli(args)
## Get stat output
"""
...
...  WordsDbCommonStat(last_added_time=..., total_entities_count=14929, total_docs_count=8, vocabulary_size=3551, labels_size=5, labels_stat=OrderedDict([('A', LabelStat(total_entities_count=1867, total_docs_count=2)), ('B', LabelStat(total_entities_count=2872, total_docs_count=2)), ('C', LabelStat(total_entities_count=953, total_docs_count=1)), ('E', LabelStat(total_entities_count=1728, total_docs_count=1)), ('H', LabelStat(total_entities_count=7509, total_docs_count=2))]))
"""

# Tune
tuner_cli(args)
## Tune output
"""
...
Best score: {'micro_accuracy': 1.0, 'micro_precision': 1.0, 'micro_recall': 1.0, 'micro_f1': 1.0, 'labels': {'B': {'accuracy': 1.0, 'precision': 1.0, 'recall': 1.0, 'f1': 1.0}}, 'macro_precision': 1.0, 'macro_recall': 1.0, 'macro_f1': 1.0}
Best params: 
{'entity_label_max_count_threshold': 2,
 'entity_labels_topk': 2,
 'entity_score_max_threshold': 1.0,
 'entity_score_min_threshold': 0.01,
 'entity_total_count_min_threshold': 1,
 'entity_total_docs_count_min_threshold': 0,
 'label_score_threshold': 0.0001,
 'sentence_min_weight': -0.01,
 'sentence_sim_score_low_threshold': 0.4,
 'sentence_sim_score_up_threshold': 1.01,
 'top_k': 50,
 'top_l': 1,
 'top_v': 10,
 'use_weight_of_sentences': False}
"""

# Classification
classificator_cli(args)
## Classification output
"""
... [test] Correct 0 / 1. Metrics: {'micro_accuracy': 0.0, 'micro_precision': 0.0, 'micro_recall': 0.0, 'micro_f1': 0.0, 'labels': {'C': {'accuracy': 0.0, 'precision': 0.0, 'recall': 0.0, 'f1': 0.0}}, 'macro_precision': 0.0, 'macro_recall': 0.0, 'macro_f1': 0.0, 'accuracy': 0.0}
... [dev] Correct 1 / 1. Metrics: {'micro_accuracy': 1.0, 'micro_precision': 1.0, 'micro_recall': 1.0, 'micro_f1': 1.0, 'labels': {'B': {'accuracy': 1.0, 'precision': 1.0, 'recall': 1.0, 'f1': 1.0}}, 'macro_precision': 1.0, 'macro_recall': 1.0, 'macro_f1': 1.0, 'accuracy': 1.0}
"""

# Classification one document
args.__dict__.update({"document_path": '/tmp/sample_workdir/txt/sample/8/8.txt'})
predicted_labels = classificator_cli(args)
## Classification one document output
"""
... Classified /tmp/sample_workdir/txt/sample/8/8.txt -> [PredictedLabel(label='H', score=0.1134)].
... Saved /tmp/sample_workdir/classify/8.extcl
... Saved /tmp/sample_workdir/highlighted/8.html
"""

# See predicted labels
print("Predicted for doc:", predicted_labels)
"""
Predicted for doc: [('H', 0.1134)]
"""
