# Installation 

### Установка системных зависимостей для pyexbase (libpyexbase)

```bash
sudo apt install build-essential cmake libboost-locale-dev pybind11-dev python3-dev
```
    
### Установка python-овских (pip) и сторонних (thirdparty) зависимостей 
```bash
pip install -r requirements.txt
```

### Установка пакета (локальная установка)
```bash
pip install .
```

### WordsDb 

Для работы ExTcl (для шагов `upload_stat`, `get_stat`, `tune` и `classify`) требуется запущенный инстанс `WordsDB`.  
Для этого нужно его установить.  
Ссылка на репозиторий: [WordsDb](https://gitlab.com/ritech_llc/wordsdb)  
Нужно выкачать репозиторий и собрать проект из исходников.  
```bash
git clone https://gitlab.com/ritech_llc/wordsdb.git
cd wordsdb
# ... Выполнить необходимые шаги из README.md проекта WordsDb ... #
```

### Проверка 

Команда
```bash
extcl --help
```

должна выдать следующий по структуре результат (детали могут отличаться): 

```
usage: ExTCL [-h] [--config CONFIG] [--dataset_reader DATASET_READER] [--input INPUT] [--work_dir WORK_DIR] [--proc_cnt PROC_CNT] [--train_prop TRAIN_PROP] [--dev_prop DEV_PROP] [--test_prop TEST_PROP]
             [--random_state RANDOM_STATE] [--batch_size BATCH_SIZE] [--train TRAIN] [--dev DEV] [--test TEST]
             {extract_text,ling,vector,index,calc_stat,upload_stat,get_stat,tune,classify} ...

Explainable Text Classification Library.

positional arguments:
  {extract_text,ling,vector,index,calc_stat,upload_stat,get_stat,tune,classify}
                        sub-command help
    extract_text        extract text
    ling                linguistic analysis
    vector              vectorization
    index               make_index
    calc_stat           calculate stat
    upload_stat         upload stat
    get_stat            get stat (for debugging, use -v to print)
    tune                tune params
    classify            assign class (label)

options:
  -h, --help            show this help message and exit
  --config CONFIG, -c CONFIG
                        config file (yaml) (default: None)

common:
  --dataset_reader DATASET_READER, -dr DATASET_READER
                        dataset reader class name, defined in 'dataset_readers' (default: OneFileDatasetReader)
  --input INPUT, -i INPUT
                        input (csv-filename, input directory or text file) (default: )
  --work_dir WORK_DIR, -w WORK_DIR
                        working directory (default: extcl_workdir)
  --proc_cnt PROC_CNT, -p PROC_CNT
                        parallel processes count (default: 4)

dataset:
  --train_prop TRAIN_PROP
                        train proportion (default: 0.8)
  --dev_prop DEV_PROP   dev proportion (default: 0.1)
  --test_prop TEST_PROP
                        test proportion (default: 0.1)
  --random_state RANDOM_STATE
                        random state (default: 38)
  --batch_size BATCH_SIZE
                        batch size (default: 1000)
  --train TRAIN         train part (default: None)
  --dev DEV             dev part (default: None)
  --test TEST           test part (default: None)
```
