# Sample

Здесь приведены примеры использования команд из командной строки для демонстрационного набора данных `etc/sample/TextAppDumpFromCsvDataSetReader`.    
В данном документе параметры передаются как аргументы командной строки во время запуска команд.  
Эквивалентные примеры с использованием yml-файла приводятся рядом с основными примерами (они закомментированы).

## Основные шаги + baseline

Здесь показаны основные шаги и метод классификации на основе ХТЗ ([характеристика тематической значимости](https://www.elibrary.ru/item.asp?id=41216283)).

Yml-файл для демонстрации данного подхода приводится [здесь](extcl_sample.yml).

#### Из Python-а
> Аналогичные вызовы при помощи python-овского интерфейса показаны здесь: [sample_example.py](sample_example.py)

### extract_text
```bash
extcl --dataset_reader TextAppDumpFromCsvDataSetReader -i etc/sample/TextAppDumpFromCsvDataSetReader/sample.csv -w /tmp/sample_workdir extract_text
# extcl -c documentation/extcl_sample.yml extract_text
```

Данная команда преобразует исходный документ в текст и расположит "plain"-тексты в рабочей директории.  
Например, файл `etc/sample/TextAppDumpFromCsvDataSetReader/sample/8/8.txt` будет положен в `/tmp/sample_workdir/txt/sample/8/8.txt`,
`etc/sample/TextAppDumpFromCsvDataSetReader/sample/9/9.txt` в `/tmp/sample_workdir/txt/sample/9/9.txt` и т.д.  

Ожидаемый вывод:
```
... INFO: faiss.loader: Loading faiss.
... INFO: faiss.loader: Successfully loaded faiss.
... INFO: root: Extracted etc/sample/TextAppDumpFromCsvDataSetReader/sample/8/8.txt -> /tmp/sample_workdir/txt/sample/8/8.txt
... INFO: root: Extracted etc/sample/TextAppDumpFromCsvDataSetReader/sample/9/9.txt -> /tmp/sample_workdir/txt/sample/9/9.txt
... INFO: root: Extracted etc/sample/TextAppDumpFromCsvDataSetReader/sample/1/1.txt -> /tmp/sample_workdir/txt/sample/1/1.txt
... INFO: root: Extracted etc/sample/TextAppDumpFromCsvDataSetReader/sample/7/7.txt -> /tmp/sample_workdir/txt/sample/7/7.txt
... INFO: root: Extracted etc/sample/TextAppDumpFromCsvDataSetReader/sample/5/5.txt -> /tmp/sample_workdir/txt/sample/5/5.txt
... INFO: root: Extracted etc/sample/TextAppDumpFromCsvDataSetReader/sample/3/3.txt -> /tmp/sample_workdir/txt/sample/3/3.txt
... INFO: root: Extracted etc/sample/TextAppDumpFromCsvDataSetReader/sample/6/6.txt -> /tmp/sample_workdir/txt/sample/6/6.txt
... INFO: root: Extracted etc/sample/TextAppDumpFromCsvDataSetReader/sample/2/2.txt -> /tmp/sample_workdir/txt/sample/2/2.txt
... INFO: root: Extracted etc/sample/TextAppDumpFromCsvDataSetReader/sample/4/4.txt -> /tmp/sample_workdir/txt/sample/4/4.txt
... INFO: root: Extracted etc/sample/TextAppDumpFromCsvDataSetReader/sample/10/10.txt -> /tmp/sample_workdir/txt/sample/10/10.txt
... INFO: root: extract text whole result(ints): {'docs_cnt': 10, 'extracted': 10, 'success': 10}
```

> Формат логов следующий: `%(asctime)s [%(process)s] %(levelname)s: %(name)s: %(message)s`, 
> где `asctime` - время, `process` - номер процесса, `levelname` - уровень логирования, `name` - название логгера, `message` - сообщение

### ling

Модель для лингвистического анализа можно скачать из [официального репозитория](https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-3131).    
Некоторые модели (`english-ewt-ud-2.5-191206.udpipe`, `russian-syntagrus-ud-2.5-191206.udpipe` и `german-gsd-ud-2.5-191206.udpipe`) также доступны в [облаке mail.ru](https://cloud.mail.ru/public/Ng68/HxS71f9rU).

Начиная с этой команды и далее может требоваться указание `--model_file` для лингв. анализа.  
Поэтому нужно скачать нужную модель и положить в определённое место, затем указать правильный путь к файлу при вызове команд.  
Далее в примерах предполагается, что файл модели `russian-syntagrus-ud-2.5-191206.udpipe` скачан и положен здесь: `../data/russian-syntagrus-ud-2.5-191206.udpipe`    

```bash
extcl --dataset_reader TextAppDumpFromCsvDataSetReader -i etc/sample/TextAppDumpFromCsvDataSetReader/sample.csv -w /tmp/sample_workdir ling --model_file ../data/russian-syntagrus-ud-2.5-191206.udpipe
# extcl -c documentation/extcl_sample.yml ling
```

Данная команда выполнит лингв. анализ для "plain"-текстов документов и расположит результат в формате CoNLL-U (расширение .conll) в рабочей директории.  
Например, результат анализа файла `/tmp/sample_workdir/txt/sample/9/9.txt` будет положен в `/tmp/sample_workdir/ling/sample/9/9.conll`,
`/tmp/sample_workdir/txt/sample/1/1.txt` в `/tmp/sample_workdir/ling/sample/1/1.conll` и т.д. 
Каждый отдельный рабочий процесс (worker) загружает копию модели лингв. анализа. Например, в приведённом ниже ожидаемом виде можно наблюдать, что 4 процесса-workera участвовали в настоящем этапе.    

Ожидаемый вывод:
```
... INFO: faiss.loader: Loading faiss.
... INFO: faiss.loader: Successfully loaded faiss.
... INFO: root: Loaded model: ../data/russian-syntagrus-ud-2.5-191206.udpipe
... INFO: root: Loaded model: ../data/russian-syntagrus-ud-2.5-191206.udpipe
... INFO: root: Loaded model: ../data/russian-syntagrus-ud-2.5-191206.udpipe
... INFO: root: Loaded model: ../data/russian-syntagrus-ud-2.5-191206.udpipe
... INFO: root: Analyzed /tmp/sample_workdir/txt/sample/9/9.txt -> /tmp/sample_workdir/ling/sample/9/9.conll
... INFO: root: Analyzed /tmp/sample_workdir/txt/sample/1/1.txt -> /tmp/sample_workdir/ling/sample/1/1.conll
... INFO: root: Analyzed /tmp/sample_workdir/txt/sample/7/7.txt -> /tmp/sample_workdir/ling/sample/7/7.conll
... INFO: root: Analyzed /tmp/sample_workdir/txt/sample/3/3.txt -> /tmp/sample_workdir/ling/sample/3/3.conll
... INFO: root: Analyzed /tmp/sample_workdir/txt/sample/2/2.txt -> /tmp/sample_workdir/ling/sample/2/2.conll
... INFO: root: Analyzed /tmp/sample_workdir/txt/sample/5/5.txt -> /tmp/sample_workdir/ling/sample/5/5.conll
... INFO: root: Analyzed /tmp/sample_workdir/txt/sample/6/6.txt -> /tmp/sample_workdir/ling/sample/6/6.conll
... INFO: root: Analyzed /tmp/sample_workdir/txt/sample/4/4.txt -> /tmp/sample_workdir/ling/sample/4/4.conll
... INFO: root: Analyzed /tmp/sample_workdir/txt/sample/8/8.txt -> /tmp/sample_workdir/ling/sample/8/8.conll
... INFO: root: Analyzed /tmp/sample_workdir/txt/sample/10/10.txt -> /tmp/sample_workdir/ling/sample/10/10.conll
... INFO: root: ling whole result(ints): {'docs_cnt': 10, 'ling_success': 10}
... INFO: root: TimeProfiler. whole process: ...
```

Возможные ошибки
* `RuntimeError: Cannot load model from file udpipe.model.`
Не задан или неправильно задан параметр `model_file`. 

### calc_stat
```bash
extcl --dataset_reader TextAppDumpFromCsvDataSetReader -i etc/sample/TextAppDumpFromCsvDataSetReader/sample.csv -w /tmp/sample_workdir calc_stat
# extcl -c documentation/extcl_sample.yml calc_stat
```

Данная команда выполнит выделение слов и словосочетаний из результата лингв. анализа документов и положит результат в нижеописанном формате (расширение `.stat`) в рабочей директории.

Например, результат анализа файла `/tmp/sample_workdir/ling/sample/9/9.conll` будет положен в `/tmp/sample_workdir/stat/sample/9/9.stat`,
`/tmp/sample_workdir/ling/sample/1/1.conll` в `/tmp/sample_workdir/stat/sample/1/1.stat` и т.д.

Ожидаемый вывод:
```
... INFO: faiss.loader: Loading faiss.
... INFO: faiss.loader: Successfully loaded faiss.
... INFO: root: Stat built /tmp/sample_workdir/ling/sample/9/9.conll -> /tmp/sample_workdir/stat/sample/9/9.stat
... INFO: root: Stat built /tmp/sample_workdir/ling/sample/1/1.conll -> /tmp/sample_workdir/stat/sample/1/1.stat
... INFO: root: Stat built /tmp/sample_workdir/ling/sample/3/3.conll -> /tmp/sample_workdir/stat/sample/3/3.stat
... INFO: root: Stat built /tmp/sample_workdir/ling/sample/7/7.conll -> /tmp/sample_workdir/stat/sample/7/7.stat
... INFO: root: Stat built /tmp/sample_workdir/ling/sample/5/5.conll -> /tmp/sample_workdir/stat/sample/5/5.stat
... INFO: root: Stat built /tmp/sample_workdir/ling/sample/2/2.conll -> /tmp/sample_workdir/stat/sample/2/2.stat
... INFO: root: Stat built /tmp/sample_workdir/ling/sample/4/4.conll -> /tmp/sample_workdir/stat/sample/4/4.stat
... INFO: root: Stat built /tmp/sample_workdir/ling/sample/6/6.conll -> /tmp/sample_workdir/stat/sample/6/6.stat
... INFO: root: Stat built /tmp/sample_workdir/ling/sample/8/8.conll -> /tmp/sample_workdir/stat/sample/8/8.stat
... INFO: root: Stat built /tmp/sample_workdir/ling/sample/10/10.conll -> /tmp/sample_workdir/stat/sample/10/10.stat
... INFO: root: calc_stat whole result(ints): {'built_stat': 10, 'stat_success': 10}
```

Файл `.stat` содержит отсортированный в порядке убывания встречаемости список слов и словосочетаний (сущностей). Де-факто это csv-файл, содержащий следующие поля:
* уникальный идентификатор сущности; 
* нормальная форма сущности; 
* число вхождений (сколько раз встретилась данная сущность в данном документе); 
* список меток классов.   

Пример (топ 10 сущностей документа `/tmp/sample_workdir/stat/sample/9/9.stat` -- первые 10 строк):
```csv
4156822362375226048,жир,22,"[""A""]"
9378588268864305357,получение,17,"[""A""]"
11341418727752418006,компонент,12,"[""A""]"
17952135422336639193,капуста,12,"[""A""]"
8230547555896881897,пшеничный,10,"[""A""]"
10799896727387325385,соль,10,"[""A""]"
14399799768923458354,топленый,10,"[""A""]"
17776448534253595825,гарнир,10,"[""A""]"
2354010141768830895,топленый жир,10,"[""A""]"
3109088788412448990,часть,9,"[""A""]"
```

### upload_stat (начиная с этой команды требуется запущенный инстанс WordsDB)  
Ссылка на репозиторий WordsDB: [ссылка](https://gitlab.com/ritech_llc/wordsdb).  
Начиная с этой команды и далее предполагается, что инстанс WordsDB слушает порт по умолчанию -- `8550`.

```bash
extcl --dataset_reader TextAppDumpFromCsvDataSetReader -i etc/sample/TextAppDumpFromCsvDataSetReader/sample.csv -w /tmp/sample_workdir upload_stat
# extcl -c documentation/extcl_sample.yml upload_stat
```

Данная команда загрузит статистику документов из train-части датасета в WordsDB.

Например, результат анализа файлов `/tmp/sample_workdir/stat/sample/9/9.stat`, `/tmp/sample_workdir/stat/sample/5/5.stat` и т.д. будут загружен в WordsDB (`http://localhost:8550/jsonrpc`). 

Ожидаемый вывод:
```
... INFO: faiss.loader: Loading faiss.
... INFO: faiss.loader: Successfully loaded faiss.
... INFO: root: Stat uploaded /tmp/sample_workdir/stat/sample/9/9.stat -> http://localhost:8550/jsonrpc
... INFO: root: Stat uploaded /tmp/sample_workdir/stat/sample/5/5.stat -> http://localhost:8550/jsonrpc
... INFO: root: Stat uploaded /tmp/sample_workdir/stat/sample/8/8.stat -> http://localhost:8550/jsonrpc
... INFO: root: Stat uploaded /tmp/sample_workdir/stat/sample/1/1.stat -> http://localhost:8550/jsonrpc
... INFO: root: Stat uploaded /tmp/sample_workdir/stat/sample/7/7.stat -> http://localhost:8550/jsonrpc
... INFO: root: Stat uploaded /tmp/sample_workdir/stat/sample/3/3.stat -> http://localhost:8550/jsonrpc
... INFO: root: Stat uploaded /tmp/sample_workdir/stat/sample/2/2.stat -> http://localhost:8550/jsonrpc
... INFO: root: Stat uploaded /tmp/sample_workdir/stat/sample/6/6.stat -> http://localhost:8550/jsonrpc
... INFO: root: upload_stat whole result(ints): {'uploaded': 8}
```

### get_stat 
```bash
extcl --dataset_reader TextAppDumpFromCsvDataSetReader -i etc/sample/TextAppDumpFromCsvDataSetReader/sample.csv -w /tmp/sample_workdir get_stat
# extcl -c documentation/extcl_sample.yml get_stat
```

Данная команда получит общую статистику WordsDB. 
Из нижеприведённого ожидаемого вывода видно, что 
* всего сущностей встретилось (`total_entities_count`) -- 14929,
* всего загружено документов (`total_docs_count`) -- 8
* размер словаря(кол-во уникальных сущностей, `vocabulary_size`) -- 3551, 
* кол-во меток классов (`labels_size`) -- 5, 
* статистика по отдельным меткам классов:
  * 'A':
    * `total_entities_count` - 1867
    * `total_docs_count` - 2
  * 'B':
    * `total_entities_count` - 2872
    * `total_docs_count` - 2
  * и т.д. 


Ожидаемый вывод:
```
... INFO: faiss.loader: Loading faiss.
... INFO: faiss.loader: Successfully loaded faiss.
... INFO: root: WordsDbCommonStat(last_added_time=1706096927, total_entities_count=14929, total_docs_count=8, vocabulary_size=3551, labels_size=5, labels_stat=OrderedDict([('A', LabelStat(total_entities_count=1867, total_docs_count=2)), ('B', LabelStat(total_entities_count=2872, total_docs_count=2)), ('C', LabelStat(total_entities_count=953, total_docs_count=1)), ('E', LabelStat(total_entities_count=1728, total_docs_count=1)), ('H', LabelStat(total_entities_count=7509, total_docs_count=2))]))
... INFO: root: Parameter 'get_documents_stat' is not set.
```

### tune 
```bash
extcl --dataset_reader TextAppDumpFromCsvDataSetReader -i etc/sample/TextAppDumpFromCsvDataSetReader/sample.csv -w /tmp/sample_workdir tune --top_l_vars 1  --label_score_threshold_vars 0.001 0.0005 0.0001 --entity_total_docs_count_min_threshold_vars 0 1 --entity_total_count_min_threshold_vars 0 1
# extcl -c documentation/extcl_sample.yml tune
```

Данная команда выполнит подбор лучших гиперпараметров из заданной сетки параметров и для заданных метрик классификации (по умолчанию учитываются следующие метрики в таком порядке: ["micro_f1", "macro_f1", "accuracy"]) 
Будут перебраны следующие параметры (перебираемые значения также приводятся):
* `top_l`: [1]  
* `label_score_threshold`: [0.001, 0.0005, 0.0001] 
* `entity_total_docs_count_min_threshold`: [0, 1] 
* `entity_total_count_min_threshold`: [0, 1]

Подбор гиперпараметров проводится на dev-части датасета.   
Промежуточные варианты для наборов параметров сохраняются в `/tmp/sample_workdir/hyperparams/`.  
Создаётся символьная ссылка на лучший набор параметров `/tmp/sample_workdir/hyperparams/best.json`

Ожидаемый вывод:
```
INFO: faiss.loader: Loading faiss.
INFO: faiss.loader: Successfully loaded faiss.
WARNING: root: found ONLY 156 of 529 (29%) entities in WordsDB
INFO: root: Classified etc/sample/TextAppDumpFromCsvDataSetReader/sample/4/4.txt -> [PredictedLabel(label='B', score=0.0625)] (gold: {'B'}). 
WARNING: root: found ONLY 156 of 529 (29%) entities in WordsDB
................................
Best params: 
{'entity_label_max_count_threshold': 2,
 'entity_labels_topk': 2,
 'entity_score_max_threshold': 1.0,
 'entity_score_min_threshold': 0.01,
 'entity_total_count_min_threshold': 1,
 'entity_total_docs_count_min_threshold': 0,
 'label_score_threshold': 0.0001,
 'sentence_min_weight': -0.01,
 'sentence_sim_score_low_threshold': 0.4,
 'sentence_sim_score_up_threshold': 1.01,
 'top_k': 50,
 'top_l': 1,
 'top_v': 10,
 'use_weight_of_sentences': False}
```


### classify (dev и test части)
```bash
extcl --dataset_reader TextAppDumpFromCsvDataSetReader -i etc/sample/TextAppDumpFromCsvDataSetReader/sample.csv -w /tmp/sample_workdir classify --save_highlighted_html True
# extcl -c documentation/extcl_sample.yml classify
```

Данная команда выполнит классификацию для dev и test частей датасета.  
Классификатор использует набор параметров `/tmp/sample_workdir/hyperparams/best.json`.  
Результаты классификации сохранятся в `/tmp/sample_workdir/classify` и `/tmp/sample_workdir/highlighted`.  
Будут сохранены файлы:
* `/tmp/sample_workdir/classify/sample/4/4.extcl`
* `/tmp/sample_workdir/highlighted/sample/4/4.html`
* `/tmp/sample_workdir/classify/sample/10/10.extcl`
* `/tmp/sample_workdir/highlighted/sample/10/10.html`

Ожидаемый вывод:
```
... INFO: faiss.loader: Loading faiss.
... INFO: faiss.loader: Successfully loaded faiss.
... INFO: root: Found tuned params /tmp/sample_workdir/hyperparams/best.json. Try to use them
... WARNING: root: found ONLY 156 of 529 (29%) entities in WordsDB
... INFO: root: Classified etc/sample/TextAppDumpFromCsvDataSetReader/sample/4/4.txt -> [PredictedLabel(label='B', score=0.0625)] (gold: {'B'}). 
... INFO: root: Saved /tmp/sample_workdir/classify/sample/4/4.extcl
... INFO: root: Saved /tmp/sample_workdir/highlighted/sample/4/4.html
... INFO: root: classify whole result(ints): {'items_cnt': 1, 'docs_cnt': 1, 'correct_cnt': 1, 'classified': 1, 'explanation_added': 1, 'saved_extcl': 1, 'saved_highlighted': 1}
... INFO: root: [dev] Correct 1 / 1. Metrics {'micro_accuracy': 1.0, 'micro_precision': 1.0, 'micro_recall': 1.0, 'micro_f1': 1.0, 'labels': {'B': {'accuracy': 1.0, 'precision': 1.0, 'recall': 1.0, 'f1': 1.0}}, 'macro_precision': 1.0, 'macro_recall': 1.0, 'macro_f1': 1.0, 'accuracy': 1.0}
... WARNING: root: found ONLY 200 of 859 (23%) entities in WordsDB
... INFO: root: Classified etc/sample/TextAppDumpFromCsvDataSetReader/sample/10/10.txt -> [PredictedLabel(label='H', score=0.0857)] (gold: {'C'}). 
... INFO: root: Saved /tmp/sample_workdir/classify/sample/10/10.extcl
... INFO: root: Saved /tmp/sample_workdir/highlighted/sample/10/10.html
/home/exuser/develop/ex_tcl/.venv3.11/lib/python3.11/site-packages/sklearn/preprocessing/_label.py:900: UserWarning: unknown class(es) ['H'] will be ignored
  warnings.warn(
... INFO: root: classify whole result(ints): {'items_cnt': 1, 'docs_cnt': 1, 'classified': 1, 'explanation_added': 1, 'saved_extcl': 1, 'saved_highlighted': 1}
... INFO: root: [test] Correct 0 / 1. Metrics {'micro_accuracy': 0.0, 'micro_precision': 0.0, 'micro_recall': 0.0, 'micro_f1': 0.0, 'labels': {'C': {'accuracy': 0.0, 'precision': 0.0, 'recall': 0.0, 'f1': 0.0}}, 'macro_precision': 0.0, 'macro_recall': 0.0, 'macro_f1': 0.0, 'accuracy': 0.0}
... INFO: root: RESULT METRICS:
... INFO: root: [dev] Correct 1 / 1. Metrics: {'micro_accuracy': 1.0, 'micro_precision': 1.0, 'micro_recall': 1.0, 'micro_f1': 1.0, 'labels': {'B': {'accuracy': 1.0, 'precision': 1.0, 'recall': 1.0, 'f1': 1.0}}, 'macro_precision': 1.0, 'macro_recall': 1.0, 'macro_f1': 1.0, 'accuracy': 1.0}
... INFO: root: [test] Correct 0 / 1. Metrics: {'micro_accuracy': 0.0, 'micro_precision': 0.0, 'micro_recall': 0.0, 'micro_f1': 0.0, 'labels': {'C': {'accuracy': 0.0, 'precision': 0.0, 'recall': 0.0, 'f1': 0.0}}, 'macro_precision': 0.0, 'macro_recall': 0.0, 'macro_f1': 0.0, 'accuracy': 0.0}
```

### Классифицировать один документ
```bash
extcl --dataset_reader TextAppDumpFromCsvDataSetReader -i etc/sample/TextAppDumpFromCsvDataSetReader/sample.csv -w /tmp/sample_workdir classify --model_file ../data/russian-syntagrus-ud-2.5-191206.udpipe -d /tmp/sample_workdir/txt/sample/8/8.txt --save_highlighted_html True
# extcl -c documentation/extcl_sample.yml classify -d /tmp/sample_workdir/txt/sample/8/8.txt
```

Документ рассматривается как новый, поэтому перед непосредственно самой классификацией автоматически будут выполнены все необходимые шаги:
- извлечение текста  
- лингв. анализ  
- выделение слов и словосочетаний  
 
Будут сохранены файлы:
* `/tmp/sample_workdir/txt/8.txt`
* `/tmp/sample_workdir/ling/8.conll`
* `/tmp/sample_workdir/classify/8.extcl`
* `/tmp/sample_workdir/highlighted/8.html`

Ожидаемый вывод:
```
... INFO: faiss.loader: Loading faiss.
... INFO: faiss.loader: Successfully loaded faiss.
... INFO: root: Found tuned params /tmp/sample_workdir/hyperparams/best.json. Try to use them
... INFO: root: Extracted /tmp/sample_workdir/txt/sample/8/8.txt -> /tmp/sample_workdir/txt/8.txt
... INFO: root: Loaded model: ../data/russian-syntagrus-ud-2.5-191206.udpipe
... INFO: root: Analyzed /tmp/sample_workdir/txt/8.txt -> /tmp/sample_workdir/ling/8.conll
... INFO: root: Classified /tmp/sample_workdir/txt/sample/8/8.txt -> [PredictedLabel(label='H', score=0.1134)]. 
... INFO: root: Saved /tmp/sample_workdir/classify/8.extcl
... INFO: root: Saved /tmp/sample_workdir/highlighted/8.html
/home/exuser/develop/ex_tcl/.venv3.11/lib/python3.11/site-packages/sklearn/preprocessing/_label.py:900: UserWarning: unknown class(es) ['H'] will be ignored
  warnings.warn(
... INFO: root: TimeProfiler. whole process: 7.64 sec
```

При повторном выполнении данной команды будут пропущены шаги:
- извлечение текста
- лингв. анализ


### Просмотр результата классификации 

Для test/train частей:
* `.extcl` файлы доступны в папке `/tmp/sample_workdir/classify/`
* `html` файлы доступны в папке `/tmp/sample_workdir/highlighted/`

Для файла `/tmp/sample_workdir/txt/sample/8/8.txt` результат доступен в файлах:
* `/tmp/sample_workdir/classify/8.extcl` (json-файл)
* `/tmp/sample_workdir/highlighted/8.html` (этот файл лучше открыть в браузере)

Файл с расширением `.extcl` представляет собой json-файл, содержащий следующую информацию:

* predicted_labels -- список предсказанных меток с указанием их финальной(агрегированной) оценки (score);
* explanation -- объяснение результата классификации для каждой метки класса:

    - для каждой предсказанной метки приводятся веса сущностей (из которых вычисляется относительный вклад сущности в предсказание текущей метки класса);
    - список офсетов «кусков» текста для каждой сущности. «Куском» текста, например, является одна словоформа из словосочетания или целое предложение.

Файл с расширением `.html` представляет собой размеченный текст, в котором выделены сущности, повлиявшие на выбор «предсказанных» меток классов.


## При помощи векторов Word2Vec

Предположим, что уже выполнены общие шаги:

* `extract_text`
* `ling`
* `calc_stat`
* `upload_stat`

Далее приводятся примеры шагов `vector`, `index` и `tune`, используя конфигурацию [extcl_sample_word2vec.yml](extcl_sample_word2vec.yml)

### vector
```bash
extcl -c documentation/extcl_sample_word2vec.yml vector
```

В результате выполнения команды будут сформированы вектора предложений:
для `/tmp/sample_workdir/ling/sample/9/9.conll` -> `/tmp/sample_workdir/vector_word2vec/sample/9/9.vec`, `/tmp/sample_workdir/ling/sample/5/5.conll` -> `/tmp/sample_workdir/vector_word2vec/sample/5/5.vec` и т.д.

Ожидаемый вывод (при условии, что нужные модели уже были загружены во время предыдущих запусков при помощи встроенных инструментов в gensim):
```
... INFO: faiss.loader: Loading faiss.
... INFO: faiss.loader: Successfully loaded faiss.
... INFO: root: Loading args from config documentation/extcl_sample_word2vec.yml. Arguments will be replaced from file.
... INFO: gensim.models.keyedvectors: loading projection weights from /home/exuser/gensim-data/word2vec-ruscorpora-300/word2vec-ruscorpora-300.gz
... INFO: gensim.models.keyedvectors: loading projection weights from /home/exuser/gensim-data/word2vec-ruscorpora-300/word2vec-ruscorpora-300.gz
... INFO: gensim.models.keyedvectors: loading projection weights from /home/exuser/gensim-data/word2vec-ruscorpora-300/word2vec-ruscorpora-300.gz
... ... ... ... ... ... 
... INFO: root: Loaded model: Word2Vec
... INFO: root: Vector built /tmp/sample_workdir/ling/sample/9/9.conll -> /tmp/sample_workdir/vector_word2vec/sample/9/9.vec
... INFO: root: Vector built /tmp/sample_workdir/ling/sample/5/5.conll -> /tmp/sample_workdir/vector_word2vec/sample/5/5.vec
... ... ... ... ... ... 
... INFO: root: vector whole result(ints): {'docs_cnt': 10, 'built_vec': 10, 'vec_success': 10}
```

Возможные ошибки:
1. При загрузке модели `gensim`
```bash
  File "/usr/lib/python3.11/json/decoder.py", line 355, in raw_decode
    raise JSONDecodeError("Expecting value", s, err.value) from None
json.decoder.JSONDecodeError: Expecting value: line 1 column 1 (char 0)
```
  Возникает из-за сетевой ошибки. Может помочь повторная попытка выполнения команды. 

### index
```bash
extcl -c documentation/extcl_sample_word2vec.yml index
```

Данная команда выполнит индексацию векторов (train-части датасета) в `/tmp/sample_workdir/index_word2vec/main.index` и создаст вспомогательную БД для хранения связи "номер вектора предложения" -> "метки классов".
Сохранит файлы:
* `/tmp/sample_workdir/index_word2vec/main.index`
* `/tmp/sample_workdir/index_word2vec/sqlite_vec.db`

```
... INFO: faiss.loader: Loading faiss.
... INFO: faiss.loader: Successfully loaded faiss.
... INFO: root: Loading args from config documentation/extcl_sample_word2vec.yml. Arguments will be replaced from file.
... INFO: root: Get vec ids for /tmp/sample_workdir/vector_word2vec/sample/8/8.vec
... INFO: root: Get vec ids for /tmp/sample_workdir/vector_word2vec/sample/9/9.vec
... INFO: root: Get vec ids for /tmp/sample_workdir/vector_word2vec/sample/1/1.vec
... INFO: root: Get vec ids for /tmp/sample_workdir/vector_word2vec/sample/7/7.vec
... INFO: root: Get vec ids for /tmp/sample_workdir/vector_word2vec/sample/5/5.vec
... INFO: root: Get vec ids for /tmp/sample_workdir/vector_word2vec/sample/3/3.vec
... INFO: root: Get vec ids for /tmp/sample_workdir/vector_word2vec/sample/6/6.vec
... INFO: root: Get vec ids for /tmp/sample_workdir/vector_word2vec/sample/2/2.vec
... INFO: root: Done with build index: /tmp/sample_workdir/index_word2vec/main.index
... INFO: root: db path: /tmp/sample_workdir/index_word2vec/sqlite_vec.db
... INFO: root: index whole result(ints): {'docs_cnt': 8, 'Indexed': 8, 'success': 8}
```

### tune
```bash
extcl -c documentation/extcl_sample_word2vec.yml tune
```

> Более подробное описание данного шага приведено выше в разделе "Основные шаги + baseline", за исключением списка актуальных настраиваемых параметров. 

### Классифицировать один документ
```bash
extcl -c documentation/extcl_sample_word2vec.yml classify -d /tmp/sample_workdir/txt/sample/8/8.txt
```

> Более подробное описание данного шага приведено выше в разделе "Основные шаги + baseline". 

### Просмотр результата классификации 

Для test/train частей:
* `.extcl` файлы доступны в папке `/tmp/sample_workdir/classify_word2vec/`
* `html` файлы доступны в папке `/tmp/sample_workdir/highlighted_word2vec/`

Для файла `/tmp/sample_workdir/txt/sample/8/8.txt` результат доступен в файлах:
* `/tmp/sample_workdir/classify_word2vec/8.extcl` (json-файл)
* `/tmp/sample_workdir/highlighted_word2vec/8.html` (этот файл лучше открыть в браузере)

Файл с расширением `.extcl` представляет собой json-файл, содержащий следующую информацию:

* predicted_labels -- список предсказанных меток с указанием их финальной(агрегированной) оценки (score);
* explanation -- объяснение результата классификации для каждой метки класса:

    - для каждой предсказанной метки приводятся веса предложений (из которых вычисляется относительный вклад предложения в предсказание текущей метки класса);
    - список офсетов «кусков» текста для каждого предложения.

Файл с расширением `.html` представляет собой размеченный текст, в котором выделены предложения, повлиявшие на выбор «предсказанных» меток классов.

## При помощи векторов SentenceBert

Предположим, что уже выполнены общие шаги:

* `extract_text`
* `ling`
* `calc_stat`
* `upload_stat`

Далее приводятся примеры шагов `vector`, `index` и `tune`, используя конфигурацию [extcl_sample_sbert.yml](extcl_sample_sbert.yml)

### vector
```bash
extcl -c documentation/extcl_sample_sbert.yml vector
```

> Выходные данные данного шага аналогичны такому же из раздела "При помощи векторов Word2Vec", за исключением подпапки в рабочей директории. Здесь используется: `vector_sentencebert` вместо `vector_word2vec`.  

### index
```bash
extcl -c documentation/extcl_sample_sbert.yml index
```

> Выходные данные данного шага аналогичны такому же из раздела "При помощи векторов Word2Vec", за исключением подпапки в рабочей директории. Здесь используется: `index_sentencebert` вместо `index_word2vec`. 

### tune
```bash
extcl -c documentation/extcl_sample_sbert.yml tune
```

> Данный шаг аналогичен такому же из раздела "При помощи векторов Word2Vec".  

### Классифицировать один документ
```bash
extcl -c documentation/extcl_sample_sbert.yml classify -d /tmp/sample_workdir/txt/sample/8/8.txt
```

> Данный шаг аналогичен такому же из раздела "При помощи векторов Word2Vec"

### Классифицировать один документ (use_weight_of_sentences)
Демонстрация включенного параметра `use_weight_of_sentences` -- учитывание веса предложений при поиске похожих векторов предложений.  
Вес предложения -- это сумма весов сущностей в предложении. Для вычисления веса сущности используется `tf-idf`.  
При `use_weight_of_sentences == True` оценка похожести предложения умножается на вес исходного предложения.  
Таким образом, больший вклад в предсказание класса сделают более весомые предложения согласно теории информационного поиска.  
```bash
rm -r /tmp/sample_workdir/hyperparams_sentencebert /tmp/sample_workdir/classify_sentencebert /tmp/sample_workdir/highlighted_sentencebert
extcl -c documentation/extcl_sample_sbert_entity_based.yml tune 
extcl -c documentation/extcl_sample_sbert_entity_based.yml classify -d /tmp/sample_workdir/txt/sample/8/8.txt
```

> Отметим, что `use_weight_of_sentences` также можно использовать и для других векторных подходов (Word2Vec, Laser, Sbert).   

### Просмотр результата классификации 

Для test/train частей:

* `.extcl` файлы доступны в папке `/tmp/sample_workdir/classify_sentencebert/`
* `html` файлы доступны в папке `/tmp/sample_workdir/highlighted_sentencebert/`

Для файла `/tmp/sample_workdir/txt/sample/8/8.txt` результат доступен в файлах:

* `/tmp/sample_workdir/classify_sentencebert/8.extcl` (json-файл)
* `/tmp/sample_workdir/highlighted_sentencebert/8.html` (этот файл лучше открыть в браузере)

Структура файлов идентична файлам, получаемым при классификации с помощью метода Word2Vec.  

## При помощи векторов Laser

Предположим, что уже выполнены общие шаги:

* `extract_text`
* `ling`
* `calc_stat`
* `upload_stat`

Далее приводятся примеры шагов `vector`, `index` и `tune`, используя конфигурацию [extcl_sample_laser.yml](extcl_sample_laser.yml)

### vector
```bash
extcl -c documentation/extcl_sample_laser.yml vector
```

> Выходные данные данного шага аналогичны такому же из раздела "При помощи векторов Word2Vec", за исключением подпапки в рабочей директории. Здесь используется: `vector_laser` вместо `vector_word2vec`.

### index
```bash
extcl -c documentation/extcl_sample_laser.yml index
```

> Выходные данные данного шага аналогичны такому же из раздела "При помощи векторов Word2Vec", за исключением подпапки в рабочей директории. Здесь используется: `index_laser` вместо `index_word2vec`. 

### tune
```bash
extcl -c documentation/extcl_sample_laser.yml tune
```

> Данный шаг аналогичен такому же из раздела "При помощи векторов Word2Vec".  

### Классифицировать один документ
```bash
extcl -c documentation/extcl_sample_laser.yml classify -d /tmp/sample_workdir/txt/sample/8/8.txt
```

> Данный шаг аналогичен такому же из раздела "При помощи векторов Word2Vec".  

### Просмотр результата классификации 

Для test/train частей:

* `.extcl` файлы доступны в папке `/tmp/sample_workdir/classify_laser/`
* `html` файлы доступны в папке `/tmp/sample_workdir/highlighted_laser/`

Для файла `/tmp/sample_workdir/txt/sample/8/8.txt` результат доступен в файлах:

* `/tmp/sample_workdir/classify_laser/8.extcl` (json-файл)
* `/tmp/sample_workdir/highlighted_laser/8.html` (этот файл лучше открыть в браузере)

Структура файлов идентична файлам, получаемым при классификации с помощью метода Word2Vec и SentenceBert


## Другие возможности библиотеки

### Интеграция с анализаторами других языков

Например, следующая команда выделит сущности на немецком языке, используя модель `german-gsd-ud-2.5-191206.udpipe`.

Языковую модель для лингвистического анализа можно скачать из [официального репозитория](https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-3131).  
Некоторые модели (`english-ewt-ud-2.5-191206.udpipe`, `russian-syntagrus-ud-2.5-191206.udpipe` и `german-gsd-ud-2.5-191206.udpipe`) также доступны в [облаке mail.ru](https://cloud.mail.ru/public/Ng68/HxS71f9rU).

```bash
extcl -c documentation/extcl_sample_german.yml extract_text -d etc/sample/TextAppDumpFromCsvDataSetReader/sample/german.txt
extcl -c documentation/extcl_sample_german.yml ling -d etc/sample/TextAppDumpFromCsvDataSetReader/sample/german.txt
extcl -c documentation/extcl_sample_german.yml calc_stat -d etc/sample/TextAppDumpFromCsvDataSetReader/sample/german.txt
```

В результате выполнение вышеприведённых команд появятся файлы:
* `/tmp/sample_workdir/txt/etc/sample/TextAppDumpFromCsvDataSetReader/sample/german.txt`
* `/tmp/sample_workdir/ling/etc/sample/TextAppDumpFromCsvDataSetReader/sample/german.conll`
* `/tmp/sample_workdir/stat/etc/sample/TextAppDumpFromCsvDataSetReader/sample/german.stat`

Извлеченные слова и словосочетания можно посмотреть в файле `/tmp/sample_workdir/stat/etc/sample/TextAppDumpFromCsvDataSetReader/sample/german.stat`.
> Формат файла `.stat` приведён выше в разделе "Основные шаги + baseline". Отметим, что в последнем столбце (метки классов) для данного примера присутствует единственная метка класса: `UndefinedLabel` - неопределённая метка.      

В нашем файле присутствуют, в частности, следующие слова и словосочетания:
- `weltlandfläche`
- `östlich länge`
- `westlich länge`
- и т.д.

> Стоит отметить, что на возможность использования других языков напрямую влияет сторонняя библиотека `pylp`, так как шаги по извлечению слов и словосочетаний выполняются при помощи `pylp`.  
> Известно, что `pylp` достаточно стабильно работает для английского и русского языков. При этом нет гарантии, что и для других языков также хорошо всё будет работать. 

### Работа со словосочетаниями

Библиотека поддерживает возможность работы со словосочетаниями (синтаксически связными сочетаниями – лексико-фразеологическими элементами - ЛФЭ), выделяемым по заданным шаблонам на этапе лингвистического анализа. При формировании сущностей (этапы лингв. анализа и извлечения слов и словосочетаний (команды `ling` и `calc_stat`)) применяется следующее:
* Фильтрация по стоп-pos-tag-ам (список `STOP_WORD_POS_TAGS` берётся из библиотеки `pylp`). Данный шаг применяется всегда. 
* Фильтрация по «белому» списку названий частей речи. Есть возможность указать список POS_TAG-ов, которые разрешается использовать для формирования сущности – для этого применяется параметр `allowed_word_pos_tags`. Если `allowed_word_pos_tags` не указан, тогда данный шаг не применяется и, соответственно, используются все части речи. По умолчанию параметр `allowed_word_pos_tags` не задан. 
* Формирование словосочетания с заданной максимальной длиной (кол-вом слов). Данный шаг применяется при задании параметра `phrases_max_len`, равному 2 или более. По умолчанию задано число 2.

Для работы со словосочетаниями имеются следующие параметры:
* `allowed_word_pos_tags` -- белый список pos-тегов (по умолчанию не указывается, поэтому рассматриваются все слова и словосочетания)
* `phrases_max_len` -- максимальное кол-во слов в словосочетании (по умолчанию -- 2) 

[Пример конфигурации с указанными параметрами для словосочетаний](extcl_sample_synonym.yml), в которой добавлены следующие параметры:
* `allowed_word_pos_tags`: `['NOUN', 'VERB']`
* `phrases_max_len`: `3`

Для демонстрации этой возможности можно выполнить следующие команды:
```
extcl -c documentation/extcl_sample_phrases.yml extract_text -d etc/sample/TextAppDumpFromCsvDataSetReader/sample/8/8.txt
extcl -c documentation/extcl_sample_phrases.yml ling -d etc/sample/TextAppDumpFromCsvDataSetReader/sample/8/8.txt
rm /tmp/sample_workdir_phrases/stat/etc/sample/TextAppDumpFromCsvDataSetReader/sample/8/8.stat
extcl -c documentation/extcl_sample_phrases.yml calc_stat -d etc/sample/TextAppDumpFromCsvDataSetReader/sample/8/8.txt
```
В результате в файле `/tmp/sample_workdir_phrases/stat/etc/sample/TextAppDumpFromCsvDataSetReader/sample/8/8.stat` будут присутствовать следующие слова и словосочетания:
- `таблица автоматический управление` (таблица автоматического управления)
- `операционный проблема` (операционная проблема)
- и т.д. 

### Подключение языковой модели

Библиотека поддерживает возможность подключения языковых моделей (эмбеддингов) для повышения полноты классификации за счёт «переоценки» значимости связанных (в смысле некоторой метрики в векторном пространстве) и синонимичных слов и словосочетаний с оценкой их значимости в контексте рубрик. 
Это реализовано на этапе извлечения слов и словосочетаний в виде добавления синонимов к исходному «мешку слов» путём подключения моделей `w2vec` с отсечением синонимов по порогу. Для подключения данной возможности используются параметры `synonym_model` (путь к модели `w2vec`) и `min_sim_theshold` (порог, ниже которой «синонимы» не добавляются в «мешок» слов). 
По умолчанию эта возможность отключена. 

Повышения полноты классификации за счёт "переоценки" значимости связанных (в смысле некоторой метрики в векторном пространстве) и синонимичных слов и словосочетаний с оценкой их значимости в контексте рубрик.
Для этого имеются следующие параметры:
* `synonym_model` -- название предобученной языковой модели (список доступных моделей можно посмотреть [здесь](https://radimrehurek.com/gensim/models/word2vec.html#pretrained-models))  
* `min_sim_threshold` -- порог, ниже которой "синонимы" не добавляются в "мешок" слов

[Пример конфигурации с подключением языковой модели](extcl_sample_synonym.yml) -- в конфигурацию добавлены следующие параметры:
* `synonym_model`: `word2vec-ruscorpora-300`
* `min_sim_threshold`: `0.75`

Для демонстрации этой возможности можно выполнить следующие команды:
```
extcl -c documentation/extcl_sample_synonym.yml extract_text -d etc/sample/TextAppDumpFromCsvDataSetReader/sample/8/8.txt
extcl -c documentation/extcl_sample_synonym.yml ling -d etc/sample/TextAppDumpFromCsvDataSetReader/sample/8/8.txt
rm /tmp/sample_workdir/stat/etc/sample/TextAppDumpFromCsvDataSetReader/sample/8/8.stat
extcl -c documentation/extcl_sample_synonym.yml calc_stat -d etc/sample/TextAppDumpFromCsvDataSetReader/sample/8/8.txt
```
В результате в файле `/tmp/sample_workdir/stat/etc/sample/TextAppDumpFromCsvDataSetReader/sample/8/8.stat` будут присутствовать следующие слова и словосочетания - "синонимы" (они располагаются в конце файла):
- `подтвердить` 
- `вывести`
- и т.д. 


### Интерпретация результата отнесения объекта к той или иной рубрике
Интерпретация результата отнесения объекта к той или иной рубрике (рубрикам) достигается путём указания на ключевые признаки (слова и словосочетания) с указанием их значимости в контексте классифицированной рубрики.  

Для демонстрации этой возможности можно выполнить следующие команды:
```
rm /tmp/sample_workdir/hyperparams/best.json
rm /tmp/sample_workdir/classify/10.extcl
rm /tmp/sample_workdir/highlighted/10.html
extcl --dataset_reader TextAppDumpFromCsvDataSetReader -i etc/sample/TextAppDumpFromCsvDataSetReader/sample.csv -w /tmp/sample_workdir classify --model_file ../data/russian-syntagrus-ud-2.5-191206.udpipe -d etc/sample/TextAppDumpFromCsvDataSetReader/sample/10/10.txt --save_highlighted_html True --top_l 3 --entity_total_docs_count_min_threshold 1 --entity_total_count_min_threshold 1 --entity_label_max_count_threshold 4 --entity_labels_topk 3 --label_score_threshold 0.001
```
Затем открыть в браузере файл `/tmp/sample_workdir/highlighted/10.html`. 
При снятии/выборе чекбокса определённой рубрики(метки класса) визуально можно заметить, что в результате классификации указана значимость в контексте классифицированной рубрики.
Для просмотра значимых слов определённой рубрики(метки класса) рекомендуется оставить выбранным только нужный чекбокс, сняв галочку с остальных. 
Например, для метки класса:
* `H` наиболее значимыми сущностями являются "сопротивление", "таблица", "интервал" и т.д. 
* `B` -- "мм", "металлов", "размер" и т.д. 
* `A` -- "содержание", "менее", "увеличение" и т.д. 


### Дообучение классификатора 
Библиотека поддерживает возможность дообучения (расширения / коррекции обучающей выборки) классификатора «на лету» (для отдельных решающих правил).   
Это достигается путём специального повторного выполнения шагов `upload_stat` и `index` (и соответствующих необходимых предыдущих шагов) для нового списка `train` для дообучения.   
В результате выполнения вышеупомянутых шагов модель классификации будет дообучена для отдельных решающих правил в соответствии с новым списком `train`.   
Например, на небольшом тестовом наборе данных (`train 100 / dev 10 / test 10`) для «плохого» класса G было выполнено «дообучение» на 12 документах (с классом G) – общий результат классификации был повышен 6% (f1-мера).  
