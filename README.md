# ExTCL

Открытая библиотека тематической классификации текстов на основе гибридных методов объяснимого искусственного интеллекта.

## Требования

* GNU Debian Bookworm или более поздняя  
* Python 3.10 и выше
* GCC 11 и выше

## Системные требования для запуска и использования открытой библиотеки

* **Процессор** 4-ядерный, с тактовой частотой 2,5 ГГц, не менее;
* **Оперативная память (RAM):** минимально от 4 Гб, рекомендуется 16 Гб и более.
* **Дисковое пространство:** свободно 20 Гб и более.
* **Разрядность ОС:** 32-разрядная ОС (х86) или 64-разрядная ОС (х64).

## Установка 

ExTCL является пакетом python.  
Для использования также требуется [WordsDb](https://gitlab.com/ritech_llc/wordsdb).  
Порядок установки приведён в [INSTALL](documentation/INSTALL.md).


### Настройка конфигурации etc/config/extcl_config.yml
```bash
nano etc/config/extcl_config.yml
# также см. примеры конфигураций, лежащих рядом с extcl_config.yml 
```
Более подробно настройка конфигурации и используемые параметры приведены в [CONFIG](documentation/CONFIG.md).

## Подготовка датасета
Библотеку предпочтительнее использовать из командной строки, поэтому удобнее, когда датасет расположен в файловой системе, и каждому документу соответствует один файл.  
Более подробно подготовка датасета описана в [DATASET](documentation/DATASET.md).  
Пример работы с демонстрационным набором данных приведён в [SAMPLE](documentation/SAMPLE.md).  

## Использование библиотеки из командной строки (CLI)

### Извлечение текста (extract text)
```bash
extcl -c etc/config/extcl_config.yml extract_text 
```

### Лингвистический анализ (ling analysis)
```bash
extcl -c etc/config/extcl_config.yml ling 
```

### Векторизация (vectorization)
```bash
extcl -c etc/config/extcl_config.yml vector
```

### Индексация векторов (Index) 
```bash
extcl -c etc/config/extcl_config.yml index
```

### Формирование статистики 
```bash
extcl -c etc/config/extcl_config.yml calc_stat
```

### Загрузка/добавление статистики в WordsDB

1. В отдельной вкладке запустить `wordsdb` (если ещё не сделано)  
В папке `build` проекта `wordsdb` выполнить:
```bash
extcl -c ../etc/config/wordsdb.yml
```

2. Загрузка/Добавление статистики в WordsDB 
```bash
extcl -c etc/config/extcl_config.yml upload_stat
```

Данные `WordsDB` сохраняются лишь при выключении (Graceful Stop, например, при помощи комбинации клавиш `CTRL+C`). 
Если по какой-то причине `WordsDB` перестал работать, тогда нужно его перезапустить. 
Затем при помощи метода получения статистики можно понять, какое количество документов добавлено в WordsDB. 
Если кол-во не сходится с требуемым кол-вом, тогда лучше рекомендуется полностью заново выполнить добавление документов.

При возникновении ошибки на данном этапе требуется лишь повторное выполнение `upload_stat` без выполнения предыдущих команд. 

3. Получение статистики из WordsDB (для контроля)

```bash
extcl -c etc/config/extcl_config.yml get_stat
```

### Обучение (тюнинг гиперпараметров, требуется запущенный инстанс WordsDb)
```bash
extcl -c etc/config/extcl_config.yml tune
```

### Классификация (dev/test части датасета, требуется запущенный инстанс WordsDb)
```bash
extcl -c etc/config/extcl_config.yml classify
```

### Классификация одного документа (текстовый документ, требуется запущенный инстанс WordsDb)
```bash
extcl -c etc/config/extcl_config.yml classify -d <path_to_txt>
```

## Шаги для разных способов классификации (последовательность выполнения важна)

### Общие шаги 
1. `extract_text`
2. `ling`
3. `calc_stat`
4. `upload_stat`

### (А) Шаги для классификации на основе сущностей (слова и словосочетания + ХТЗ)
5. (А) `tune`
6. (А) `classify`

### (Б) Шаги для классификации на основе поиска похожих предложений
5. (Б) `vector`
6. (Б) `index`
7. (Б) `tune`
8. (Б) `classify`

## Прочее
#### Подробный вывод логов
Для подробного вывода логов требуется установить переменную окружения `EXTCL_VERBOSE`:
```bash
export EXTCL_VERBOSE=1 # подробный
export EXTCL_VERBOSE=0 # стандартный
```

### Часто встречающиеся ошибки и проблемы

#### NoLemmatizationException 
Может возникнуть по ряду причин:
  * Нет лемматизации в используемой модели лингв. анализа; 
  * При использовании модели pylp, не установлена переменная окружения PYLP_RESOURCES_DIR ;
  * Не скопированы нужные файлы для pylp в PYLP_RESOURCES_DIR 

Может помочь:
* Выбор модель лингв. анализа со втроенной лемматизацей ;
* При использовании pylp модели: положить требуемые файлы для модели pylp в папку <PATH TO PYLP DATA DIR> и установить переменную окружения: 
  `export PYLP_RESOURCES_DIR=<PATH TO PYLP DATA DIR>`


## Перечень направлений прикладного использования
Результаты проекта, в частности разрабатываемая библиотека с открытым исходным кодом ExTCL может быть применена в следующих сферах:
* Системы поддержки принятия решений для стратегического планирования, системы патентной и научно-технической аналитики
* Системы анализа больших массивов текстов, решающие задачи:
  * Маршрутизации документов, сообщений и обращений;
  * Анализа нормативно-правовой информации;
  * Анализа патентной и научно-технической информации;
  * Контентной фильтрации;
  * DLP (предотвращение утечек данных).

## Общие характеристики библиотеки
* `Multiprocessing/multithreading` 
  * `ExTCL` поддерживает `multiprocessing` на уровне документов. 
  * `WordsDB` поддерживает `multithreading` (многопоточное параллельное исполнение); 
  * Сторонняя библиотека индексации векторов поддерживает `multithreading` (многопоточное параллельное исполнение);
* `Multilabel`: системе поддерживает документы, которому соответствует больше одной метки класса. 
* Удобная работа из консоли: возможен безопасный повторный запуск для большинства подкоманд `ExTcl`. Документ не будет обработан повторно: это реализовано на уровне предварительной проверки наличия выходного файла в рабочей директории. 

Однако не стоит повторно запускать команды `upload_stat`, `index`, т.к. в результате повторного выполнения появятся искажённая статистика в WordsDB и вектора в индексе.   
Сущностью является слово и словосочетание. Кол-во слов в словосочетании (длина словосочетания) является параметром: `phrases_max_len` (целое число, >=2).   

WordsDB разработана с использованием языка `С++` и представляет собой отдельный модуль, работающий независимо от библиотеки `ExTCL`. 
Коммуникация с основным клиентом производится посредством `JSON-RPC` (`Remote Protocol Call`) запросов. 
Также `WordsDB` обладает такими возможностями, как многопоточная обработка поступающих запросов и поддержка нескольких меток для одного документа (`multilabel`).

Библиотека `ExTCL` предоставляет удобный `CLI` (Command Line Interface) для работы, организованный по следующим принципам:  
* Общие параметры доступны;
* Для отдельных подкоманд – отдельные параметры;
* араметры группируются по компонентам;

Параметры для работы библиотеки классификации можно передавать через командную строку или указать в конфигурационном файле.

    
## Технические характеристики

1. Наличие API (командная строка для основных утилит, а также [Python](documentation/sample_example.py)).
2. Используемые языки / инструменты разработки: POSIX-совместимые ОС, Python 3, СИ++-17.
3. Поддержка обработки текстов на русском и английском языках.
4. Возможность интеграции с анализаторами [других языков](documentation/SAMPLE.md#Интеграция с анализаторами других языков).
5. Возможность работы со словосочетаниями (синтаксически связными сочетаниями – лексико-фразеологическими элементами - ЛФЭ), выделяемым по
заданным [шаблонам на этапе лингвистического анализа](documentation/SAMPLE.md#Работа со словосочетаниями).
6. Реализация алгоритмов классификации на основе характеристики тематической значимости (ХТЗ), алгоритмов на основе эмбеддинговых моделей
(Word2Vec, Sentence BERT, LASER).
7. Возможность [подключения языковых моделей (эмбеддингов)](documentation/SAMPLE.md#Подключение языковой модели) для повышения полноты классификации за счёт «переоценки» значимости связанных (в смысле
некоторой метрики в векторном пространстве) и синонимичных слов и словосочетаний с оценкой их значимости в контексте рубрик.
8. Возможность [интерпретации результата отнесения объекта к той или иной рубрике](documentation/SAMPLE.md#Интерпретация результата отнесения объекта к той или иной рубрике) (рубрикам) путём указания на ключевые признаки (слова и
словосочетания - лексико- фразеологические элементы - ЛФЭ) с указанием их значимости в контексте классифицированной рубрики.
9. Возможность [интерпретации результата отнесения объекта к той или иной рубрике](documentation/SAMPLE.md#Интерпретация результата отнесения объекта к той или иной рубрике) (рубрикам) путём отсылки к фрагментам исходного текста (предложениям,
абзацам, содержащим ключевые признаки - ЛФЭ) с указанием их значимости в контексте классифицированной рубрики.
10. Возможность [дообучения (расширения / коррекции обучающей выборки) классификатора «на лету»](documentation/SAMPLE.md#Дообучение классификатора) (для отдельных решающих правил).
11. Возможность параллельности процесса классификации (одновременная обработка нескольких документов параллельно в нескольких процессах/потоках).
12. Возможность параллельности процесса обучения (одновременная обработка нескольких документов параллельно в нескольких процессах/потоках с последующим построением обученной модели)
13. Качество (полнота и точность) и скорость работы на уровне передовых методов классификации:
+F1-мера классификации на уровне 96% (+/- 4%)
+Время классификации методом ХТЗ с объяснением среднего документа (10 000 символов текста) на стандартной рабочей станции (4 ядра CPU 2,5 ГГц, 16 Гб
RAM, без использования GPU) – не более 1,2 с.
    
## Пример новых вариантов использования кода открытой библиотеки

В [sample_example.py](documentation/sample_example.py) приведен пример вызовов ниже описанных функций:

```python
# Подключение библиотеки
import argparse
from extcllib.cli import ArgsFormatter
from extcllib.cli import add_common_opts, add_dataset_opts
from extcllib.cli import add_extract_text_opts, extract_text_cli
from extcllib.cli import add_ling_opts, ling_cli
from extcllib.cli import add_stat_builder_opts, calc_stat_cli
from extcllib.cli import add_stat_uploader_opts, upload_stat_cli
from extcllib.cli import add_get_stat_opts, get_stat_cli

from extcllib.cli import (
    add_index_opts,
    add_vec_opts,
    add_tuner_opts,
    add_classificator_opts,
    add_baseline_classificator_opts,
    add_vec_classificator_opts,
    tuner_cli,
    classificator_cli,
)

# Настройки (задание параметов) и подготовка параметров при помощи argparse
dataset_reader = 'TextAppDumpFromCsvDataSetReader'
input = 'etc/sample/TextAppDumpFromCsvDataSetReader/sample.csv'
work_dir = '/tmp/sample_workdir'
model_file = '../data/russian-syntagrus-ud-2.5-191206.udpipe'


parser = argparse.ArgumentParser(
    prog="ExTCL",
    description="""Explainable Text Classification Library.""",
    formatter_class=ArgsFormatter,
)
parser.add_argument("--config", "-c", type=str, required=False, help="config file (yaml)")
add_common_opts(parser)
add_dataset_opts(parser)
add_extract_text_opts(parser)
add_ling_opts(parser)
add_stat_builder_opts(parser)
add_stat_uploader_opts(parser)
# add_get_stat_opts(parser)

add_index_opts(parser)
add_vec_opts(parser)
add_tuner_opts(parser)
add_classificator_opts(parser)
add_baseline_classificator_opts(parser)
add_vec_classificator_opts(parser)

args = parser.parse_args()
args.__dict__.update(
    {
        'dataset_reader': dataset_reader,
        'input': input,
        'work_dir': work_dir,
        'model_file': model_file,
        'get_documents_stat': False,
        'top_l_vars': [1],
        'label_score_threshold_vars': [0.001, 0.0005, 0.0001],
        'entity_total_docs_count_min_threshold_vars': [0, 1],
        'entity_total_count_min_threshold_vars': [0, 1],
        'save_highlighted_html': True,
        'document_path': None,
    }
)

# Run steps below:

# Extract text
extract_text_cli(args)
## Extract text output:
"""
...
... extract text whole result(ints): {'docs_cnt': 10, 'extracted': 10, 'success': 10}
"""

# Ling analysis
ling_cli(args)
## Ling analysis output
"""
...
... ling whole result(ints): {'docs_cnt': 10, 'ling_success': 10}
"""

# Calc stat
calc_stat_cli(args)
## Calc stat output
"""
...
...  calc_stat whole result(ints): {'built_stat': 10, 'stat_success': 10}
"""

# Upload stat  (run once!)
## Требуется запущенный инстанс [WordsDb](https://gitlab.com/ritech_llc/wordsdb).
upload_stat_cli(args)
## Upload stat output
"""
...
...  upload_stat whole result(ints): {'uploaded': 8}
"""

# Get stat (check)
get_stat_cli(args)
## Get stat output
"""
...
...  WordsDbCommonStat(last_added_time=..., total_entities_count=14929, total_docs_count=8, vocabulary_size=3551, labels_size=5, labels_stat=OrderedDict([('A', LabelStat(total_entities_count=1867, total_docs_count=2)), ('B', LabelStat(total_entities_count=2872, total_docs_count=2)), ('C', LabelStat(total_entities_count=953, total_docs_count=1)), ('E', LabelStat(total_entities_count=1728, total_docs_count=1)), ('H', LabelStat(total_entities_count=7509, total_docs_count=2))]))
"""

# Tune
tuner_cli(args)
## Tune output
"""
...
Best score: {'micro_accuracy': 1.0, 'micro_precision': 1.0, 'micro_recall': 1.0, 'micro_f1': 1.0, 'labels': {'B': {'accuracy': 1.0, 'precision': 1.0, 'recall': 1.0, 'f1': 1.0}}, 'macro_precision': 1.0, 'macro_recall': 1.0, 'macro_f1': 1.0}
Best params: 
{'entity_label_max_count_threshold': 2,
 'entity_labels_topk': 2,
 'entity_score_max_threshold': 1.0,
 'entity_score_min_threshold': 0.01,
 'entity_total_count_min_threshold': 1,
 'entity_total_docs_count_min_threshold': 0,
 'label_score_threshold': 0.0001,
 'sentence_min_weight': -0.01,
 'sentence_sim_score_low_threshold': 0.4,
 'sentence_sim_score_up_threshold': 1.01,
 'top_k': 50,
 'top_l': 1,
 'top_v': 10,
 'use_weight_of_sentences': False}
"""

# Classification
classificator_cli(args)
## Classification output
"""
... [test] Correct 0 / 1. Metrics: {'micro_accuracy': 0.0, 'micro_precision': 0.0, 'micro_recall': 0.0, 'micro_f1': 0.0, 'labels': {'C': {'accuracy': 0.0, 'precision': 0.0, 'recall': 0.0, 'f1': 0.0}}, 'macro_precision': 0.0, 'macro_recall': 0.0, 'macro_f1': 0.0, 'accuracy': 0.0}
... [dev] Correct 1 / 1. Metrics: {'micro_accuracy': 1.0, 'micro_precision': 1.0, 'micro_recall': 1.0, 'micro_f1': 1.0, 'labels': {'B': {'accuracy': 1.0, 'precision': 1.0, 'recall': 1.0, 'f1': 1.0}}, 'macro_precision': 1.0, 'macro_recall': 1.0, 'macro_f1': 1.0, 'accuracy': 1.0}
"""

# Classification one document
args.__dict__.update({"document_path": '/tmp/sample_workdir/txt/sample/8/8.txt'})
predicted_labels = classificator_cli(args)
## Classification one document output
"""
... Classified /tmp/sample_workdir/txt/sample/8/8.txt -> [PredictedLabel(label='H', score=0.1134)].
... Saved /tmp/sample_workdir/classify/8.extcl
... Saved /tmp/sample_workdir/highlighted/8.html
"""

# See predicted labels
print("Predicted for doc:", predicted_labels)
"""
Predicted for doc: [('H', 0.1134)]
"""
```

Далее приведено более подробное описание шагов с приведением реализации вызванных функций. 
### Общие шаги

#### Извлечение текста
```python
def extract_text_cli(opts):
    d_h = get_dataset_handler_instance(opts)
    input_queue_items_generator = d_h.steps_paths(
        step=ExTclStep.TEXT_EXTRACT,
        part='all',
    )
    MultiProcessor.process_step(
        opts=opts,
        func=extract_text_func,
        input_queue_items_generator=input_queue_items_generator,
        step_name='extract text',
    )
```
#### Лингвистический анализ
```python
def ling_cli(opts):
    d_h = get_dataset_handler_instance(opts)
    input_queue_items_generator = d_h.steps_paths(
        step=ExTclStep.LING_ANALYSIS,
        part='all',
    )
    MultiProcessor.process_step(
        opts=opts,
        func=ling_func,
        input_queue_items_generator=input_queue_items_generator,
        step_name='ling',
    )
```
#### Формирование статистики
```python
def calc_stat_cli(opts):
    d_h = get_dataset_handler_instance(opts)
    input_queue_items_generator = d_h.steps_paths(
        step=ExTclStep.CALC_STAT,
        part='all',
    )
    MultiProcessor.process_step(
        opts=opts,
        func=calc_stat_func,
        input_queue_items_generator=input_queue_items_generator,
        step_name='calc_stat',
    )
```
#### Загрузка(добавление) статистики в WordsDB
Требуется запущенный инстанс [WordsDb](https://gitlab.com/ritech_llc/wordsdb).
```python
def upload_stat_cli(opts):
    d_h = get_dataset_handler_instance(opts)
    input_queue_items_generator = d_h.steps_paths(
        step=ExTclStep.UPLOAD_STAT,
        part='train',
    )
    MultiProcessor.process_step(
        opts=opts,
        func=upload_stat_func,
        input_queue_items_generator=input_queue_items_generator,
        step_name='upload_stat',
    )
```
#### Векторизация
```python
def vectorize_cli(opts):
    d_h = get_dataset_handler_instance(opts)
    input_queue_items_generator = d_h.steps_paths(
        step=ExTclStep.VECTORIZE,
        part='all',
    )
    MultiProcessor.process_step(
        opts=opts,
        func=vec_func,
        input_queue_items_generator=input_queue_items_generator,
        step_name='vector',
    )
```

#### Индексация векторов
```python
def indexer_cli(opts):
    opts.proc_cnt = 1
    d_h = get_dataset_handler_instance(opts)
    input_queue_items_generator = d_h.steps_paths(
        step=ExTclStep.MAKE_INDEX,
        part='train',
    )
    opts.index_db_path = d_h.get_filepath_in_workdir(opts.index_db_path, ExTclStep.MAKE_INDEX)
    opts.index_path = d_h.get_filepath_in_workdir(opts.index_path, ExTclStep.MAKE_INDEX)
    MultiProcessor.process_step(
        opts=opts,
        func=make_index_func,
        input_queue_items_generator=input_queue_items_generator,
        step_name='index',
    )
```
### Обучение
```python
def tuner_cli(opts):
    d_h = get_dataset_handler_instance(opts)

    if hasattr(opts, 'index_db_path'):
        opts.index_db_path = d_h.get_filepath_in_workdir(opts.index_db_path, ExTclStep.MAKE_INDEX)
    if hasattr(opts, 'index_path'):
        opts.index_path = d_h.get_filepath_in_workdir(opts.index_path, ExTclStep.MAKE_INDEX)

    best_params_config_filename = d_h.get_best_params_config_filename()

    hyperparams_generator = param_grid_generator(opts)

    res = MultiProcessor.process_step(
        opts=opts,
        func=tuner_func,
        input_queue_items_generator=hyperparams_generator,
        step_name='tune',
    )

    results = list()
    hyperparameters_work_file = d_h.get_filepath_in_workdir('best.json', ExTclStep.TUNE)
    for hyperparameters_work_file in os.listdir(fs.dirname(hyperparameters_work_file)):
        if hyperparameters_work_file == 'best.json':
            continue

        hyperparameters_work_file = d_h.get_filepath_in_workdir(
            hyperparameters_work_file, ExTclStep.TUNE
        )
        with open(hyperparameters_work_file, 'r') as f:
            hyperparameters_res = json.load(f)
            metrics = hyperparameters_res.pop('metrics')
            results.append((metrics, hyperparameters_res, hyperparameters_work_file))

    if isinstance(opts.target_metrics, (list,)):
        target_metrics = opts.target_metrics
    elif isinstance(opts.target_metrics, (str,)):
        target_metrics = [opts.target_metrics]

    def _sort_by_metrics(results1, results2):
        metrics1 = results1[0]
        metrics2 = results2[0]
        if metrics1 is None and metrics2 is None:
            return 1
        elif metrics1 is None and metrics2 is not None:
            return -1
        elif metrics2 is None and metrics1 is not None:
            return 1

        res = None
        for metric in target_metrics:
            if res:
                break
            if metric in metrics1 and metric in metrics2:
                if metrics1[metric] > metrics2[metric]:
                    res = 1
                    break
                if metrics1[metric] < metrics2[metric]:
                    res = -1
                    break
                else:
                    continue
            else:
                if metric in metrics1:
                    res = 1
                if metric in metrics2:
                    res = -1

        if res:
            return res

        return 1

    if results:
        results.sort(reverse=True, key=cmp_to_key(_sort_by_metrics))
    else:
        logging.error("There is empty results!")
        return

    if fs.exists(best_params_config_filename):
        os.remove(best_params_config_filename)

    os.symlink(os.path.basename(results[0][2]), best_params_config_filename)

    for i in range(min(6, len(results))):
        print("Top {} score: {}".format(i, results[i][0]))
        print("Top {} score: ".format(i))
        pprint.pprint(results[i][1])

    print("Best score: {}".format(results[0][0]))
    print("Best params: ")
    pprint.pprint(results[0][1])
```


#### Классификация
```python
def classificator_cli(opts):
    d_h = get_dataset_handler_instance(opts)

    if hasattr(opts, 'index_db_path'):
        opts.index_db_path = d_h.get_filepath_in_workdir(opts.index_db_path, ExTclStep.MAKE_INDEX)
    if hasattr(opts, 'index_path'):
        opts.index_path = d_h.get_filepath_in_workdir(opts.index_path, ExTclStep.MAKE_INDEX)

    opts_ = copy.deepcopy(opts)

    best_params_config_filename = d_h.get_best_params_config_filename()
    if fs.exists(best_params_config_filename):
        with open(best_params_config_filename, 'r') as f:
            best_params = json.load(f)
            opts_.__dict__.update(best_params)
            logging.info(f"Found tuned params {best_params_config_filename}. Try to use them")

    results = [
        (part, classificator_part_func(opts_, d_h, part))
        for part in {'train', 'dev', 'test'} & set(opts_.parts)
    ]

    if opts.document_path is None or not opts.document_path:
        logging.info("RESULT METRICS:")
        for part, (correct_cnt, docs_cnt, metrics, _) in results:
            logging.info(f"[{part}] Correct {correct_cnt} / {docs_cnt}. Metrics: {metrics}")
        return
    else:
        assert len(results) == 1
        assert len(results[0]) == 2
        predicted_labels = results[0][1][3]
        return predicted_labels
```

#### Объяснение результата классификации
В результате вызова `classificator_cli` далее вызывается следующая цепочка функций:
* `classificator_part_func`
* `classificator_func`
* `make_classification`

В функции `make_classification` доступно объяснение результата классификации:
```python
    predicted_labels, explanation = classifier.predict(
        txt_path, ling_path, vect_path, label, text, opts
    )
```

В результате в файловую систему сохраняются результаты (объяснение и html для просмотра) в файлы с расширением .extcl и .html соответственно.  



## License
[GPLv3](LICENSE)


## Datasets
[Datasets desctiption](documentation/extcl_datasets.md)
