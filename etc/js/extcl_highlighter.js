var MaxScore = null;
var CheckedLabels = null;
// var ChunkIdToChunkNodes = null;
var ChunkNodesSet = null;

function get_max_score()
{
    var max_score = Number.MIN_VALUE;
    for (let label in LabelToMaxTokenScore)
    {
        if (CheckedLabels.has(label))
        {
            max_score = Math.max(max_score, LabelToMaxTokenScore[label]);
        }
    }
    return max_score;
}

function get_chunk_ids_of_chunk_node(chunk_node)
{
    return chunk_node.getAttribute('chunk_ids').split(ChunkIdsSep);
}

function collect_chunk_id_to_chunk_nodes()
{
    var chunk_id_to_chunk_nodes = {};
    var all_chunk_nodes_set = new Set();

    var chunk_nodes = document.evaluate(
        "//*[@chunk_ids]",
        document,
        null,
        XPathResult.ANY_TYPE,
        null,
    );
    var chunk_node = chunk_nodes.iterateNext();
    while (chunk_node)
    {
        all_chunk_nodes_set.add(chunk_node);

        var chunk_ids = get_chunk_ids_of_chunk_node(chunk_node);
        chunk_ids.forEach(function (chunk_id)
        {
            if (!chunk_id_to_chunk_nodes.hasOwnProperty(chunk_id))
            {
                chunk_id_to_chunk_nodes[chunk_id] = new Set();
            }
            chunk_id_to_chunk_nodes[chunk_id].add(chunk_node);
        }
        );
        chunk_node = chunk_nodes.iterateNext();
    }
    return {'dict': chunk_id_to_chunk_nodes, 'set': all_chunk_nodes_set};
}

function remove_chunk_highlight(node)
{
    node.style.backgroundColor = null;
}

function remove_all_highlights()
{
    for (let chunk_node of ChunkNodesSet)
    {
        remove_chunk_highlight(chunk_node);
    }
}

function highlight_chunk_node(node, label, score)
{
    // var max_score = LabelToMaxTokenScore[label];
    var max_score = MaxScore;
    var alpha = score/max_score;
    var label_base_color = LabelToBaseColor[label];
    alpha = Math.round(alpha * 100) / 100;
    node.style.backgroundColor = 'rgba(' + label_base_color + ',' + alpha.toString() + ')';
}

function process_chunk_node(chunk_node)
{
    var chunk_ids = get_chunk_ids_of_chunk_node(chunk_node);

    var best_score = Number.MIN_VALUE;
    var best_label = null;

    for (let c_id in chunk_ids)
    {
        var chunk_id_ = chunk_ids[c_id];
        for (let cs_id in ChunksScores[chunk_id_])
        {
            var chunk_score = ChunksScores[chunk_id_][cs_id];
            var label = chunk_score[0];
            var score = chunk_score[1];
            if (CheckedLabels.has(label) && score > best_score)
            {
                best_score = score;
                best_label = label;
                break; // Here we expect, that chunk scores are always ordered
            }
        }
    }
    if (best_label && best_label !== 'null' && best_label !== 'undefined')
    {
        highlight_chunk_node(chunk_node, best_label, best_score);
    }
    else
    {
        remove_chunk_highlight(chunk_node);
    }
}

function get_label_checkboxes()
{
    return document.getElementsByClassName('ExtclLabelsCheckBox');
}

function get_checked_labels()
{
    var checked_labels = new Set();
    var inputElements = get_label_checkboxes();
    for(var i=0; inputElements[i]; ++i){
        if(inputElements[i].checked){
            label_id = inputElements[i].value;
            checked_labels.add(label_id);
        }
    }
    return checked_labels;
}

function extcl_checkbox_handler(form)
{
    if (!ChunkNodesSet || ChunkNodesSet == 'null' || ChunkNodesSet == 'undefined')
    {
        result = collect_chunk_id_to_chunk_nodes();
        // ChunkIdToChunkNodes = result['dict'];
        ChunkNodesSet = result['set'];
    }

    CheckedLabels = get_checked_labels();
    MaxScore = get_max_score(CheckedLabels);

    for (let chunk_node of ChunkNodesSet)
    {
        process_chunk_node(chunk_node);
    }
}

function extcl_checkbox_handler_all(source)
{
    var checkboxes = get_label_checkboxes();
    for(var i=0; checkboxes[i]; ++i)
    {
        checkboxes[i].checked = source.checked;
    }

    CheckedLabels = get_checked_labels();

    for (let chunk_node of ChunkNodesSet)
    {
        process_chunk_node(chunk_node);
    }
}

window.onload = extcl_checkbox_handler;
